//
//  Personal.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Resume;

@interface Personal : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * dob;
@property (nonatomic, retain) NSString * emailId;
@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSString * phNo;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Resume *ownby;

@end

//
//  CompanyInfo.h
//  MyARSample
//
//  Created by vairat on 22/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompanyInfo : NSObject{
    
    
    NSString *businessName;
    NSString *contactName;
    NSString *telphone;
    NSString *mobile;
    NSString *email;
    NSString *website;
    NSString *companyDesc;
}
@property (nonatomic, strong) NSString *businessName;
@property (nonatomic, strong) NSString *contactName;
@property (nonatomic, strong) NSString *telphone;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, strong) NSString *companyDesc;

@end

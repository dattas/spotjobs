//
//  PostAJobViewController.h
//  MyARSample
//
//  Created by vairat on 21/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"

@interface PostAJobViewController : UIViewController<UIKeyboardViewControllerDelegate>{
    
    UIKeyboardViewController *keyBoardController;
}

- (IBAction)test_Action:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *postajobScrollView;

@property (strong, nonatomic)NSMutableArray *jobDetailsArray;
@property (strong, nonatomic)NSMutableArray *jobSummaryArray;

@property (strong, nonatomic) IBOutlet UITextView *jobDescripitionTextView;
@property (strong, nonatomic) IBOutlet UITextView *jobRequirementTextView;

@property (strong, nonatomic) IBOutlet UITextField *eligibilityTextField;
@property (strong, nonatomic) IBOutlet UITextField *experienceTextField;
@property (strong, nonatomic) IBOutlet UITextField *jobRoleTextField;

@property (strong, nonatomic) IBOutlet UIButton *weekEndsButton;
@property (strong, nonatomic) IBOutlet UIButton *weekDaysButton;
@property (strong, nonatomic) IBOutlet UIButton *indoorsButton;
@property (strong, nonatomic) IBOutlet UIButton *outdoorsButton;
@property (strong, nonatomic) IBOutlet UIButton *customerFacing;
@property (strong, nonatomic) IBOutlet UIButton *moringButton;
@property (strong, nonatomic) IBOutlet UIButton *afternoonButton;
@property (strong, nonatomic) IBOutlet UIButton *eveningButton;


- (IBAction)weeksButtonTapped:(id)sender;
- (IBAction)inOutButtonTapped:(id)sender;
- (IBAction)customerButtonTapped:(id)sender;
- (IBAction)timeOfDayButtonTapped:(id)sender;
@end

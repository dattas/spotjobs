//
//  CompanyInfoParser.h
//  MyARSample
//
//  Created by vairat on 22/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyInfo.h"

@protocol CompanyInfoXMLParser;
@interface CompanyInfoParser : NSObject< NSXMLParserDelegate>


@property (unsafe_unretained) id <CompanyInfoXMLParser> delegate;
@end

@protocol CompanyInfoXMLParser <NSObject>
- (void) parsingCompanyDetailsFinished:(CompanyInfo *) companyDetails;
- (void) companyDetailXMLparsingFailed;
@end
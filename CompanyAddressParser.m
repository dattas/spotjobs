//
//  CompanyAddressParser.m
//  MyARSample
//
//  Created by vairat on 23/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "CompanyAddressParser.h"

@interface CompanyAddressParser()
{
    NSMutableString *charString;
    NSMutableArray  *compAddressList;
    CompanyAddress *compAdress;
    
}
@property (nonatomic, strong) NSMutableArray *compAddressList;
@end


@implementation CompanyAddressParser
@synthesize delegate;
@synthesize compAddressList;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    NSLog(@"parserDidStartDocument");
    compAddressList = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    
    if ([elementName isEqualToString:@"addresses"]) {
        compAdress = [[CompanyAddress alloc] init];
        
        NSLog(@"addresses compAdress creatd");
    }
    /*
    else if ([elementName isEqualToString:@"contactname"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"address"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"street"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"city"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"state"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"country"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"postcode"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"lat"]) {
        
         charString = nil;
    }
    else if ([elementName isEqualToString:@"long"]) {
        
        charString = nil;
    }
    else if ([elementName isEqualToString:@"alt"]) {
        
         charString = nil;
    }*/


}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"id"]) {
        compAdress.addressID = finalString;
    }
    else if ([elementName isEqualToString:@"contactname"]) {
        compAdress.contactName = finalString;
    }
    else if ([elementName isEqualToString:@"mobile"]) {
        compAdress.mobileNumber = finalString;
    }
    else if ([elementName isEqualToString:@"phno"]) {
        compAdress.contactNumber = finalString;
    }
    else if ([elementName isEqualToString:@"address"]) {
        compAdress.address = finalString;
    }
    else if ([elementName isEqualToString:@"street"]) {
        compAdress.street = finalString;
    }
    
    else if ([elementName isEqualToString:@"city"]) {
        
        compAdress.suburb = finalString;
    }
    else if ([elementName isEqualToString:@"state"]) {
        
        compAdress.state = finalString;
    }
    else if ([elementName isEqualToString:@"country"]) {
        
        compAdress.country = finalString;
    }
    else if ([elementName isEqualToString:@"postcode"]) {
        
        compAdress.postCode = finalString;
    }
    else if ([elementName isEqualToString:@"lat"]) {
        
        compAdress.latitude = finalString;
    }
    else if ([elementName isEqualToString:@"long"]) {
        
        compAdress.longitude = finalString;
    }
    else if ([elementName isEqualToString:@"alt"]) {
        
        compAdress.altitude = finalString;
    }
    else if ([elementName isEqualToString:@"addresses"]) {
        
        if(compAdress)
             [compAddressList addObject:compAdress];
        
        
        NSLog(@"%d",[compAddressList count]);
        compAdress = nil;
        
    }
    else if([elementName isEqualToString:@"root"]) {
        [parser abortParsing];
        
        if (delegate) {
            if ([delegate respondsToSelector:@selector(parsingCompanyAddressFinished:)]) {
                
                [delegate parsingCompanyAddressFinished:compAddressList];
            }
        }
        compAdress = nil;
        
    }
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    NSLog(@"%@",[parseError localizedDescription]);
    if (delegate) {
        if ([delegate respondsToSelector:@selector(companyAddressXMLparsingFailed)]) {
           [delegate companyAddressXMLparsingFailed];
        }
    }
}

@end

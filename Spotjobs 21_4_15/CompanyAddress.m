//
//  CompanyAddress.m
//  MyARSample
//
//  Created by vairat on 23/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "CompanyAddress.h"

@implementation CompanyAddress
@synthesize addressID;
@synthesize contactName;
@synthesize contactNumber;
@synthesize mobileNumber;
@synthesize address;
@synthesize street;
@synthesize suburb;
@synthesize state;
@synthesize country;
@synthesize postCode;
@synthesize latitude;
@synthesize longitude;
@synthesize altitude;
@end

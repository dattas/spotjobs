//
//  CompanyInfoParser.m
//  MyARSample
//
//  Created by vairat on 22/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "CompanyInfoParser.h"

@interface CompanyInfoParser()
{
    NSMutableString *charString;
    CompanyInfo *companyDetails;
}
@property (nonatomic, strong) CompanyInfo *companyDetails;
@end

@implementation CompanyInfoParser
@synthesize companyDetails;
@synthesize delegate;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    
    if ([elementName isEqualToString:@"root"]) {
        companyDetails = [[CompanyInfo alloc] init];
    }
    else if ([elementName isEqualToString:@"cname"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"contactname"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"phno"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"mobile"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"email"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"web"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"desc"]) {
        charString = nil;
    }
        
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"employer_info"]) {
        [parser abortParsing];
        if (delegate) {
            if ([delegate respondsToSelector:@selector(parsingCompanyDetailsFinished:)]) {
                
                [delegate parsingCompanyDetailsFinished:companyDetails];
            }
        }
        companyDetails = nil;
    }
    
    else if ([elementName isEqualToString:@"cname"]) {
        companyDetails.businessName = finalString;
    }
    else if ([elementName isEqualToString:@"contactname"]) {
        companyDetails.contactName = finalString;
    }
    else if ([elementName isEqualToString:@"phno"]) {
        companyDetails.telphone = finalString;
    }
    else if ([elementName isEqualToString:@"mobile"]) {
        companyDetails.mobile = finalString;
    }
    else if ([elementName isEqualToString:@"email"]) {
        companyDetails.email = finalString;
    }
    else if ([elementName isEqualToString:@"web"]) {
        companyDetails.website = finalString;
    }
    else if ([elementName isEqualToString:@"desc"]) {
        companyDetails.companyDesc = finalString;
    }

    charString = nil;
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    if (delegate) {
        if ([delegate respondsToSelector:@selector(companyDetailXMLparsingFailed)]) {
            [delegate companyDetailXMLparsingFailed];
        }
    }
}





@end

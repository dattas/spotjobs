//
//  JobDetailsParser.m
//  MyARSample
//
//  Created by vairat on 26/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "JobDetailsParser.h"

@interface JobDetailsParser()
{
    NSMutableString *charString;
    NSMutableArray  *jobsList;
    JobDetails *job;
    
}
@property (nonatomic, strong) NSMutableArray *jobsList;
@end


@implementation JobDetailsParser
@synthesize delegate;
@synthesize jobsList;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    NSLog(@"parserDidStartDocument");
    jobsList = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    
    if ([elementName isEqualToString:@"jobs"]) {
        job = [[JobDetails alloc] init];
        
        NSLog(@"Job Object creatd");
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"id"]) {
        job.jobID = finalString;
    }
    else if ([elementName isEqualToString:@"job_title"]) {
        job.jobRole = finalString;
    }
    else if ([elementName isEqualToString:@"morning"]) {
        job.morning = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"noon"]) {
        job.afternoon = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"evening"]) {
        job.evening = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"weekdays"]) {
        job.weekDays = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"weekends"]) {
        job.weekEnds = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"customer_facing"]) {
        
        job.customerFacing = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"outdoor"]) {
        
        job.outdoors = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"indoor"]) {
        
        job.indoors = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"featured"]) {
        
        job.featured = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"cat_id"]) {
        
        job.jobCategory = finalString;
    }
    else if ([elementName isEqualToString:@"text"]) {
        job.jobDescription = finalString;
    }
    else if ([elementName isEqualToString:@"req"]) {
        
        job.jobRequirements = finalString;
    }
    else if ([elementName isEqualToString:@"exp_required"]) {
        
        job.experience = finalString;
    }
    else if ([elementName isEqualToString:@"other"]) {
        
        job.eligiblity = finalString;
    }
    else if ([elementName isEqualToString:@"user_id"]) {
        
        //job.experience = finalString;
    }
    else if ([elementName isEqualToString:@"starts_on"]) {
        
        job.jobStartDate = finalString;
    }
    else if ([elementName isEqualToString:@"ends_on"]) {
        
        job.jobEndDate = finalString;
    }
    else if ([elementName isEqualToString:@"active"]) {
        
        job.active = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"job_filled"]) {
        
        job.jobFilled = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"jobs"]) {
        
        if(job)
            [jobsList addObject:job];
        
        
        NSLog(@"%d",[jobsList count]);
        job = nil;
        
    }
    else if([elementName isEqualToString:@"root"]) {
        [parser abortParsing];
        
        if (delegate) {
            if ([delegate respondsToSelector:@selector(parsingJobDetailsFinished:)]) {
                
                [delegate parsingJobDetailsFinished:jobsList];
            }
        }
        job = nil;
        
    }
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    NSLog(@"%@",[parseError localizedDescription]);
    if (delegate) {
        if ([delegate respondsToSelector:@selector(jobDetailsXMLparsingFailed)]) {
            [delegate jobDetailsXMLparsingFailed];
        }
    }
}


@end

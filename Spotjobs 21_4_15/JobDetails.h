//
//  JobDetails.h
//  MyARSample
//
//  Created by vairat on 06/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JobDetails : NSObject
{
    NSString *jobID;
    NSString *jobDescription;
    NSString *jobRequirements;
    NSString *eligiblity;
    NSString *experience;
    NSString *jobRole;
    NSString *jobCategory;
    NSString *jobStartDate;
    NSString *jobEndDate;
    
    
    int weekDays;
    int weekEnds;
    int indoors;
    int outdoors;
    int morning, afternoon, evening;
    int customerFacing;
    int featured;
    int active;
    int jobFilled;

}
@property (nonatomic ,strong)NSString *jobID;
@property (nonatomic ,strong)NSString *eligiblity;
@property (nonatomic ,strong)NSString *experience;
@property (nonatomic ,strong)NSString *jobDescription;
@property (nonatomic ,strong)NSString *jobRequirements;
@property (nonatomic ,strong)NSString *jobRole;
@property (nonatomic ,strong)NSString *jobStartDate;
@property (nonatomic ,strong)NSString *jobEndDate;
@property (nonatomic ,strong)NSString *jobCategory;

@property (nonatomic ,readwrite)int weekDays;
@property (nonatomic ,readwrite)int weekEnds;
@property (nonatomic ,readwrite)int indoors;
@property (nonatomic ,readwrite)int outdoors;
@property (nonatomic ,readwrite)int morning;
@property (nonatomic ,readwrite)int afternoon;
@property (nonatomic ,readwrite)int evening;
@property (nonatomic ,readwrite)int customerFacing;
@property (nonatomic ,readwrite)int featured;
@property (nonatomic ,readwrite)int active;
@property (nonatomic ,readwrite)int jobFilled;


@end

//
//  PostAJobViewController.h
//  MyARSample
//
//  Created by vairat on 21/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"
#import "CategoryXMLParser.h"
#import "JobDetails.h"

@interface PostAJobViewController : UIViewController<UIKeyboardViewControllerDelegate, NSURLConnectionDelegate,NSXMLParserDelegate,UIAlertViewDelegate>{
    
    UIKeyboardViewController *keyBoardController;
    NSMutableData *_responseData;
}



@property (strong, nonatomic) IBOutlet UIScrollView *postajobScrollView;

@property (strong, nonatomic) IBOutlet UITextView *jobDescripitionTextView;
@property (strong, nonatomic) IBOutlet UITextView *jobRequirementTextView;

@property (strong, nonatomic) IBOutlet UITextField *eligibilityTextField;
@property (strong, nonatomic) IBOutlet UITextField *experienceTextField;
@property (strong, nonatomic) IBOutlet UITextField *jobRoleTextField;
@property (strong, nonatomic) IBOutlet UITextField *startDateTextField;
@property (strong, nonatomic) IBOutlet UITextField *endDateTextField;
@property (strong, nonatomic) IBOutlet UITextField *categoryTextField;

@property (strong, nonatomic) IBOutlet UIButton *weekEndsButton;
@property (strong, nonatomic) IBOutlet UIButton *weekDaysButton;
@property (strong, nonatomic) IBOutlet UIButton *indoorsButton;
@property (strong, nonatomic) IBOutlet UIButton *outdoorsButton;
@property (strong, nonatomic) IBOutlet UIButton *customerFacing;
@property (strong, nonatomic) IBOutlet UIButton *moringButton;
@property (strong, nonatomic) IBOutlet UIButton *afternoonButton;
@property (strong, nonatomic) IBOutlet UIButton *eveningButton;
@property (strong, nonatomic) IBOutlet UIButton *featureButton;
@property (strong, nonatomic) IBOutlet UIButton *PostAJobButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil currentJob:(JobDetails *)jobObj;
- (IBAction)postAJob_Action:(id)sender;
- (IBAction)weeksButtonTapped:(id)sender;
- (IBAction)inOutButtonTapped:(id)sender;
- (IBAction)customerButtonTapped:(id)sender;
- (IBAction)timeOfDayButtonTapped:(id)sender;
- (IBAction)featureButtonTapped:(id)sender;
@end

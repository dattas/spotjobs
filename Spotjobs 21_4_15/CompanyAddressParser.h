//
//  CompanyAddressParser.h
//  MyARSample
//
//  Created by vairat on 23/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyAddress.h"

@protocol CompanyAddressXMLParserDelegate;

@interface CompanyAddressParser : NSObject <NSXMLParserDelegate>
{
    
    __unsafe_unretained id <CompanyAddressXMLParserDelegate> delegate;
}
@property (unsafe_unretained) id <CompanyAddressXMLParserDelegate> delegate;
@end

@protocol CompanyAddressXMLParserDelegate <NSObject>
- (void) parsingCompanyAddressFinished:(NSArray *) companyAddresses_Array;
- (void) companyAddressXMLparsingFailed;
@end




//
//  JobDetailsParser.h
//  MyARSample
//
//  Created by vairat on 26/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JobDetails.h"

@protocol JobDetailsXMLParserDelegate;

@interface JobDetailsParser : NSObject <NSXMLParserDelegate>{
    
    __unsafe_unretained id <JobDetailsXMLParserDelegate> delegate;
}
@property (unsafe_unretained) id <JobDetailsXMLParserDelegate> delegate;
@end

@protocol JobDetailsXMLParserDelegate <NSObject>
- (void) parsingJobDetailsFinished:(NSArray *) jobDetails_Array;
- (void) jobDetailsXMLparsingFailed;
@end
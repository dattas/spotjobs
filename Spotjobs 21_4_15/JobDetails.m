//
//  JobDetails.m
//  MyARSample
//
//  Created by vairat on 06/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "JobDetails.h"

@implementation JobDetails

@synthesize jobID;
@synthesize eligiblity;
@synthesize experience;
@synthesize jobDescription;
@synthesize jobRequirements;
@synthesize jobRole;
@synthesize jobStartDate;
@synthesize jobEndDate;
@synthesize jobCategory;

@synthesize weekDays;
@synthesize weekEnds;
@synthesize indoors;
@synthesize outdoors;
@synthesize morning;
@synthesize afternoon;
@synthesize evening;
@synthesize customerFacing;
@synthesize featured;
@synthesize active;
@synthesize jobFilled;

@end

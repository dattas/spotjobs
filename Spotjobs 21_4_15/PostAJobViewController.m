//
//  PostAJobViewController.m
//  MyARSample
//
//  Created by vairat on 21/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "PostAJobViewController.h"
#import "AppDelegate.h"
#import "Category.h"

@interface PostAJobViewController (){
    AppDelegate *appDelegate;
    
    BOOL isMorning;
    BOOL isAftrNoon;
    BOOL isEvening;
    BOOL isWeekDays;
    BOOL isWeekEnds;
    BOOL isCustomerFacing;
    BOOL isIndoor;
    BOOL isOutdoor;
    BOOL isFeatured;
    BOOL isEditable;
    
    
    NSMutableArray *jobCategory_Array;
    NSURLConnection *postAJobRequest;
    
   
    NSXMLParser *parser;
    
    int weekEnds;
    int weekDays;
    int indoors,outdoors;
    int custFacing;
    int mrng, aftrn, eveng;
    int featured;
    NSString *selectdCatID;
    NSString *currentTask;
    JobDetails *currentJobObject;
    
}
@property(nonatomic, strong)NSMutableArray *jobCategory_Array;
@end

@implementation PostAJobViewController


@synthesize jobDescripitionTextView;
@synthesize jobRequirementTextView;

@synthesize eligibilityTextField;
@synthesize experienceTextField;
@synthesize startDateTextField;
@synthesize endDateTextField;
@synthesize categoryTextField;
@synthesize jobRoleTextField;
@synthesize customerFacing;

@synthesize weekDaysButton;
@synthesize weekEndsButton;
@synthesize indoorsButton;
@synthesize outdoorsButton;
@synthesize moringButton;
@synthesize afternoonButton;
@synthesize eveningButton;
@synthesize jobCategory_Array;
@synthesize featureButton;
@synthesize PostAJobButton;

@synthesize postajobScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil currentJob:(JobDetails *)jobObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if (jobObj) {
            currentTask = @"EDIT";
            currentJobObject = jobObj;}
        else
            currentTask = @"POST";
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
   // [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     self.jobCategory_Array = [[NSMutableArray alloc]init];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    weekEnds = 0;
    weekDays = 0;
    indoors  = 0;
    outdoors = 0;
    custFacing = 0;
    mrng =0; aftrn =0; eveng =0;
    selectdCatID = @"";
    isEditable = NO;

    if([currentTask isEqualToString:@"EDIT"]){
        self.navigationItem.title=@"Job Details";
        [self disableUserInteraction];
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"edit.png"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(editButtonTapped)];
        self.navigationItem.rightBarButtonItem = editButton;
        [self configureJobDetails];
    }
    else{
        self.navigationItem.title=@"Post A Job";
    

    isMorning        = NO;
    isAftrNoon       = NO;
    isEvening        = NO;
    isWeekDays       = NO;
    isWeekEnds       = NO;
    isCustomerFacing = NO;
    isIndoor         = NO;
    isOutdoor        = NO;
    isFeatured       = NO;
    }
    
    
    
    [postajobScrollView setContentSize:CGSizeMake(320, 1200)];
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
    keyBoardController.currentScrollView = postajobScrollView;
	[keyBoardController addToolbarToKeyboard];
    
}

-(void)editButtonTapped{
    
    if (isEditable) {
        [self disableUserInteraction];
        isEditable = NO;
    }
    else{
    UIAlertView *editAlert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:@"Do you want to edit the job?"
                                                   delegate:self
                                          cancelButtonTitle:@"NO" otherButtonTitles:@"OK",nil];
    [editAlert show];
    }
}


-(void)disableUserInteraction{
    
    PostAJobButton.hidden = YES;
    self.jobDescripitionTextView.userInteractionEnabled = NO;
    self.jobRequirementTextView.userInteractionEnabled  = NO;
    self.eligibilityTextField.userInteractionEnabled    = NO;
    self.experienceTextField.userInteractionEnabled     = NO;
    self.jobRoleTextField.userInteractionEnabled        = NO;
    self.startDateTextField.userInteractionEnabled      = NO;
    self.endDateTextField.userInteractionEnabled        = NO;
    self.categoryTextField.userInteractionEnabled       = NO;
    
    self.weekEndsButton.userInteractionEnabled          = NO;
    self.weekDaysButton.userInteractionEnabled          = NO;
    self.indoorsButton.userInteractionEnabled           = NO;
    self.outdoorsButton.userInteractionEnabled          = NO;
    self.customerFacing.userInteractionEnabled          = NO;
    self.moringButton.userInteractionEnabled            = NO;
    self.afternoonButton.userInteractionEnabled         = NO;
    self.eveningButton.userInteractionEnabled           = NO;
    self.featureButton.userInteractionEnabled           = NO;
    
}

-(void)enableUserInteraction{
    
    PostAJobButton.hidden = NO;
    self.jobDescripitionTextView.userInteractionEnabled = YES;
    self.jobRequirementTextView.userInteractionEnabled  = YES;
    self.eligibilityTextField.userInteractionEnabled    = YES;
    self.experienceTextField.userInteractionEnabled     = YES;
    self.jobRoleTextField.userInteractionEnabled        = YES;
    self.startDateTextField.userInteractionEnabled      = YES;
    self.endDateTextField.userInteractionEnabled        = YES;
    self.categoryTextField.userInteractionEnabled       = YES;
    
    self.weekEndsButton.userInteractionEnabled          = YES;
    self.weekDaysButton.userInteractionEnabled          = YES;
    self.indoorsButton.userInteractionEnabled           = YES;
    self.outdoorsButton.userInteractionEnabled          = YES;
    self.customerFacing.userInteractionEnabled          = YES;
    self.moringButton.userInteractionEnabled            = YES;
    self.afternoonButton.userInteractionEnabled         = YES;
    self.eveningButton.userInteractionEnabled           = YES;
    self.featureButton.userInteractionEnabled           = YES;
    
}


-(void)configureJobDetails{
    
    jobDescripitionTextView.text = currentJobObject.jobDescription;
    jobRequirementTextView.text  = currentJobObject.jobRequirements;
    eligibilityTextField.text    = currentJobObject.eligiblity;
    experienceTextField.text     = currentJobObject.experience;
    jobRoleTextField.text        = currentJobObject.jobRole;
    startDateTextField.text      = currentJobObject.jobStartDate;
    endDateTextField.text        = currentJobObject.jobEndDate;
    categoryTextField.text       = [self getCategory:currentJobObject.jobCategory];
    
    UIButton *button = [[UIButton alloc]init];
    
    if(currentJobObject.weekDays == 1)
        isWeekDays       = NO;
    else
        isWeekDays       = YES;
    
    if(currentJobObject.weekEnds == 1)
        isWeekEnds       = NO;
    else
        isWeekEnds       = YES;
    
    button.tag = 1;
    [self weeksButtonTapped:button];
    button.tag = 2;
    [self weeksButtonTapped:button];
    
    //TimeOfDay
    if(currentJobObject.morning == 1)
        isMorning       = NO;
    else
        isMorning       = YES;
    if(currentJobObject.afternoon == 1)
        isAftrNoon       = NO;
    else
        isAftrNoon       = YES;
    if(currentJobObject.evening == 1)
        isEvening       = NO;
    else
        isEvening       = YES;
    
    button.tag = 1;
    [self timeOfDayButtonTapped:button];
    button.tag = 2;
    [self timeOfDayButtonTapped:button];
    button.tag = 3;
    [self timeOfDayButtonTapped:button];
    
    
    if(currentJobObject.indoors == 1)
        isIndoor       = NO;
    else
        isIndoor       = YES;
    if(currentJobObject.outdoors == 1)
        isOutdoor       = NO;
    else
        isOutdoor       = YES;
    button.tag = 1;
    [self inOutButtonTapped:button];
    button.tag = 2;
    [self inOutButtonTapped:button];
    
    if(currentJobObject.customerFacing == 1)
        isEvening       = NO;
    else
        isEvening       = YES;
    [self customerButtonTapped:button];
    
    if(currentJobObject.featured == 1)
        isFeatured       = NO;
    else
        isFeatured       = YES;
    [self featureButtonTapped:button];
    
    
    
}

-(NSString *)getCategory:(NSString *)catID{
    
    
    for(Category *cat in appDelegate.categoryList_Array){
        
        if([cat.catId isEqualToString:catID])
            return cat.catName;

    }
    
    return NULL;
}

-(void)backaction
{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                    completion:NULL];
    
}



- (IBAction)postAJob_Action:(id)sender {
    
    NSString *eligibilityCriteria = [self.eligibilityTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *experience          = [self.experienceTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *jobRole             = [self.jobRoleTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *jobDescrp           = [self.jobDescripitionTextView.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *jobreqremnt         = [self.jobRequirementTextView.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *userID             = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    NSString *type = @"Job";
    NSString *jobID = currentJobObject.jobID;
    
   
    NSURL *aUrl;
    
    if([currentTask isEqualToString:@"EDIT"]){
        appDelegate.startDate = currentJobObject.jobStartDate;
        appDelegate.endDate = currentJobObject.jobEndDate;
        
        aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/editjob.php?jid=%@&user_id=%@&title=%@&type=%@&morning=%d&noon=%d&evening=%d&weekdays=%d&weekends=%d&cf=%d&outdoor=%d&indoor=%d&cat_id=%@&text=%@&req=%@&other=%@&er=%@&featured=%d&starts_on=%@&ends_on=%@",jobID,userID,jobRole,type,mrng,aftrn,eveng,weekDays,weekEnds,custFacing,outdoors,indoors,currentJobObject.jobCategory,jobDescrp,jobreqremnt,eligibilityCriteria,experience,featured,appDelegate.startDate,appDelegate.endDate]];
        
    }
    else
    aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/addjob.php?user_id=%@&title=%@&type=%@&morning=%d&noon=%d&evening=%d&weekdays=%d&weekends=%d&cf=%d&outdoor=%d&indoor=%d&cat_id=%@&text=%@&req=%@&other=%@&er=%@&featured=%d&starts_on=%@&ends_on=%@",userID,jobRole,type,mrng,aftrn,eveng,weekDays,weekEnds,custFacing,outdoors,indoors,appDelegate.selectdCatID,jobDescrp,jobreqremnt,eligibilityCriteria,experience,featured,appDelegate.startDate,appDelegate.endDate]];
    
  
    NSLog(@"POSTAJOB URL==>%@",aUrl);
     NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    
     postAJobRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.cancelButtonIndex == buttonIndex)
    {
        
        
        
    }
    
    else{
        isEditable = YES;
        [PostAJobButton setTitle:@"Update A Job" forState:UIControlStateNormal];
        [self enableUserInteraction];
       }
}


//=======================-----------NSURLCONNECTION METHODS---------================================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSLog(@"didReceiveResponse");
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
       [_responseData appendData:data];
    
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"PostAJob Request::::%@",responseString);
    
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate = self;
        [xmlParser parse];
        parser = xmlParser;
    
   

}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConnection");
    
}


//=====================------------------ PARSING METHODS --------------=============================//
#pragma mark -- NSXMLParser Delegate methods

NSMutableString *parsrElementStr;

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"EditJob"]) {
    parsrElementStr = [[NSMutableString alloc] init];
    NSLog(@"%@",parsrElementStr);
    }
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [parsrElementStr appendString:string];
    
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"EditJob"]) {
        
        if ([parsrElementStr isEqualToString:@"Success"])
        {
            
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"SUCCESS" message:@"Successfully Updated the job" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [successAlert show];
            
        }
        else
        {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Unable to Update the job" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
        }
        
    }
    else{
        NSLog(@"Post A Job::%@",elementName);
    }
    
    
    
}


- (IBAction)featureButtonTapped:(id)sender{
    
    if(isFeatured)
    {
        [self.featureButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
        isFeatured = NO;
        featured = 0;
    }
    else
    {
        
        [self.featureButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
        isFeatured = YES;
        featured = 1;
        
    }

    
    
}

- (IBAction)weeksButtonTapped:(id)sender{

    switch ([sender tag]) {
        
            
        case 1:
                 if(isWeekEnds)
                {
                   
                    [self.weekEndsButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                    isWeekEnds = NO;
                    weekEnds = 0;
                 }
                else
                {
                   [self.weekEndsButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                  isWeekEnds = YES;
                    weekEnds = 1;
                
                }
                break;
            
        case 2:
               if(isWeekDays)
                {
                  [self.weekDaysButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                  isWeekDays = NO;
                  weekDays = 0;
                }
                else
                {
                  [self.weekDaysButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                  isWeekDays = YES;
                  weekDays = 1;
                }
            break;
            
        default:
            break;
    }

}
- (IBAction)inOutButtonTapped:(id)sender{

    switch ([sender tag]) {
        case 1:
            if(isIndoor)
            {
                
                [self.indoorsButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isIndoor = NO;
                indoors = 0;
            }
            else
            {
                [self.indoorsButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isIndoor = YES;
                indoors = 1;
                
            }
            
            break;
            
        case 2:
            if(isOutdoor)
            {
                
                [self.outdoorsButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isOutdoor = NO;
                outdoors = 0;
            }
            else
            {
                [self.outdoorsButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isOutdoor = YES;
                outdoors = 1;
                
            }
            
            break;
        default:
            break;
    }
}
- (IBAction)customerButtonTapped:(id)sender
{
   
            if(isCustomerFacing)
            {
                
                [self.customerFacing setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isCustomerFacing = NO;
                custFacing = 0;
            }
            else
            {
                [self.customerFacing setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isCustomerFacing = YES;
                custFacing = 1;
                
            }
            
}
- (IBAction)timeOfDayButtonTapped:(id)sender{

    switch ([sender tag]) {
        case 1:
            if(isMorning)
            {
                
                [self.moringButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isMorning = NO;
                mrng = 0;
            }
            else
            {
                [self.moringButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isMorning = YES;
                mrng = 1;
                
            }
            break;
            
        case 2:
            if(isAftrNoon)
            {
                
                [self.afternoonButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isAftrNoon = NO;
                aftrn = 0;
            }
            else
            {
                [self.afternoonButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isAftrNoon = YES;
                aftrn = 1;
                
            }
            break;
            
        case 3:
            if(isEvening)
            {
                
                [self.eveningButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isEvening = NO;
                eveng = 0;
            }
            else
            {
                [self.eveningButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isEvening = YES;
                eveng = 1;
                
            }
            break;
        default:
            break;
    }




}

- (void)alttextFieldDidEndEditing:(UITextField *)textField{
    
    if(textField.tag == 6)
        self.startDateTextField.text = appDelegate.startDate;
    else  if(textField.tag == 7)
        self.endDateTextField.text = appDelegate.endDate;
    else if(textField.tag == 10)
        self.categoryTextField.text = appDelegate.selectdCatName;
    
    
    
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//   
//    CGPoint offset = scrollView.contentOffset;
//    if (offset.x != 0) {
//        [scrollView setContentOffset:CGPointMake(0, offset.y)];
//    }
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCustomerFacing:nil];
    [self setMoringButton:nil];
    [self setAfternoonButton:nil];
    [self setEveningButton:nil];
    [super viewDidUnload];
}

@end
/*
 //========= UIPICKERVIEW DELEGATE METHODS ==============//
 - (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
 {
 
 Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
 selectdCatID = catObj.catId;
 
 
 }
 //-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
 //{
 //    Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
 //    return catObj.catName;
 //}
 
 - (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
 {
 UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 45)];
 
 [label setTextAlignment:UITextAlignmentCenter];
 label.opaque=NO;
 label.backgroundColor=[UIColor clearColor];
 label.textColor = [UIColor blackColor];
 UIFont* textFont = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
 
 label.font = textFont;
 Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
 label.text = catObj.catName;
 return label;
 
 }
 -(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
 {
 
 return [appDelegate.categoryList_Array count];
 
 }
 
 
 -(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
 return 1;
 }
 */


//
//  Reference.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "Reference.h"
#import "Resume.h"


@implementation Reference

@dynamic company;
@dynamic email;
@dynamic phone;
@dynamic referencename;
@dynamic ownby;

@end

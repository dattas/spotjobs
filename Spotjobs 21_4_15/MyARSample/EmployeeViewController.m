//
//  EmployeeViewController.m
//  MyARSample
//
//  Created by vairat on 26/10/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "EmployeeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SKBounceAnimation.h"

#import "FillComInfoViewController.h"
#import "EmployeeHomeViewController.h"

#define TIME_FOR_SHRINKING 0.61f // Has to be different from SPEED_OF_EXPANDING and has to end in 'f'
#define TIME_FOR_EXPANDING 0.60f // Has to be different from SPEED_OF_SHRINKING and has to end in 'f'
#define SCALED_DOWN_AMOUNT 0.01  // For example, 0.01 is one hundredth of the normal size
@interface EmployeeViewController (){
    
    BOOL menuViewUp;
    PostAJobViewController *postjobViewController;
    FillComInfoViewController *addressViewController;
    EmployeeHomeViewController *homeViewController;
  
    UINavigationController *navController;
    
    
    
}
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;
@end

@implementation EmployeeViewController
@synthesize loginView;
@synthesize menuButton;
@synthesize menuView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Home";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setTranslucent:NO];
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    
    loginView.layer.cornerRadius = 10.0;
    loginView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    loginView.layer.shadowOpacity = 1.0;
    loginView.layer.shadowOffset = CGSizeMake(0.0, 10.0);
    loginView.layer.shadowRadius = 10.0;
    
    self.loginView.center = self.view.center;
    [self.view addSubview:loginView];
    
    
    
  
    
    
    
}


- (IBAction)menuButton_Action:(id)sender
{
    
    if(menuViewUp)
    {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect ContainerFrame = self.menuView.frame;
            ContainerFrame.origin.y = self.menuView.frame.origin.y + 48.0;
            self.menuView.frame = ContainerFrame;}
         
                         completion:^(BOOL finished) {
                             menuViewUp = NO;
                             
                             [self.menuButton setImage:[UIImage imageNamed:@"bMenu.png"] forState:UIControlStateNormal];
                         } ];
        
    }
    else
    {
        
        
        NSString *keyPath = @"position.y";
        id finalValue = [NSNumber numberWithFloat:516 - 45.0];
        
        SKBounceAnimation *bounceAnimation = [SKBounceAnimation animationWithKeyPath:keyPath];
        bounceAnimation.fromValue = [NSNumber numberWithFloat:516];
        bounceAnimation.toValue = finalValue;
        bounceAnimation.duration = 0.6f;
        bounceAnimation.numberOfBounces = 4;
        bounceAnimation.stiffness = SKBounceAnimationStiffnessLight;
        bounceAnimation.shouldOvershoot = YES;
        
        [self.menuView.layer addAnimation:bounceAnimation forKey:@"someKey"];
        
        [self.menuView.layer setValue:finalValue forKeyPath:keyPath];
        
        menuViewUp = YES;
        [self.menuButton setImage:[UIImage imageNamed:@"bClose.png"] forState:UIControlStateNormal];
        
    }
}



- (IBAction)bottomMenuButtonTapped:(id)sender{
    
    switch ([sender tag]) {
        case 0:
               self.title = @"Home";
               [self presentEmployeHomeViewController];
               break;
        case 1:
                self.title = @"Post A Job";
                [self presentPostAJobController];
                break;
        case 2:
               self.title = @"Edit Company Info";
               [self presentEditAddressController];
               break;
        case 3:
            
            break;
            
        default:
            break;
    }
    
    
}


//==================================METHOD TO LOAD SEARCH VIEW====================================//

-(void)presentEmployeHomeViewController
{
    
    if (!homeViewController)
    {
        homeViewController =[[EmployeeHomeViewController alloc]initWithNibName:@"EmployeeHomeViewController" bundle:nil];
    }
    
    [self presentNewViewController:homeViewController];
    
}

-(void)presentPostAJobController
{
    
    if (!postjobViewController)
    {
        postjobViewController =[[PostAJobViewController alloc]initWithNibName:@"PostAJobViewController" bundle:nil currentJob:  nil];
    }
    
    [self presentNewViewController:postjobViewController];
    
}

-(void)presentEditAddressController
{
    
    addressViewController =  [[FillComInfoViewController alloc]initWithNibName:@"FillComInfoViewController" bundle:nil];
   
   
    [self presentNewViewController:addressViewController];
    
}

- (void) presentNewViewController:(UIViewController *) controller
{

    navController = [[UINavigationController alloc]initWithRootViewController:controller];
    [self performSelector:@selector(animateTransition:) withObject:[NSNumber numberWithFloat: TIME_FOR_EXPANDING]];
}

//================================== EXPAND AND SHRINK ANIMATION ====================================//


-(void)animateTransition:(NSNumber *)duration
{
    NSLog(@"animateTransition");
    
	self.view.userInteractionEnabled=NO;
    
    
    
	[[self view] addSubview:navController.view];
    [self.view bringSubviewToFront:self.menuView];
    
    if ((navController.view.hidden==false) && ([duration floatValue]==TIME_FOR_EXPANDING))
    {
        NSLog(@"IF");
        navController.view.frame=[[UIScreen mainScreen] bounds];
        navController.view.transform=CGAffineTransformMakeScale(SCALED_DOWN_AMOUNT, SCALED_DOWN_AMOUNT);
    }
	navController.view.hidden=false;
	if ([duration floatValue]==TIME_FOR_SHRINKING)
    {
        NSLog(@"SHRINKING");
		[UIView beginAnimations:@"animationShrink" context:NULL];
		[UIView setAnimationDuration:[duration floatValue]];
		navController.view.transform=CGAffineTransformMakeScale(SCALED_DOWN_AMOUNT, SCALED_DOWN_AMOUNT);
	}
	else
    {
        NSLog(@"EXPAND");
		[UIView beginAnimations:@"animationExpand" context:NULL];
		[UIView setAnimationDuration:[duration floatValue]];
		navController.view.transform=CGAffineTransformMakeScale(1.01, 1.01);
	}
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView commitAnimations];
}

-(void)animationDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
    
	self.view.userInteractionEnabled=YES;
	if ([animationID isEqualToString:@"animationExpand"])
    {
		//[[self navigationController] pushViewController:navController animated:NO];
	}
	else {
		navController.view.hidden=true;
	}
}
//====================================================================================//


-(void)back
{

    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         //[self.navigationController popViewControllerAnimated:NO];
         [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:NO];
     }
     completion:NULL];
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

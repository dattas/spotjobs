//
//  Experience.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "Experience.h"
#import "Resume.h"


@implementation Experience

@dynamic company;
@dynamic jobresponse;
@dynamic location;
@dynamic period;
@dynamic position;
@dynamic insertedTime;
@dynamic ownby;

@end

//
//  Reference.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Resume;

@interface Reference : NSManagedObject

@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * referencename;
@property (nonatomic, retain) Resume *ownby;

@end

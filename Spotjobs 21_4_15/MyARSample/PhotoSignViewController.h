//
//  PhotoSignViewController.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Resume.h"
#import "ReaderViewController.h"
#import "PhotoSign.h"
#import "UIKeyboardViewController.h"

@interface PhotoSignViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,ReaderViewControllerDelegate,UIKeyboardViewControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate>
{
    UIKeyboardViewController *keyBoardController;
    int numLines;
    
}
@property (strong, nonatomic) IBOutlet UITextView *objectiveTextView;
@property (strong, nonatomic) IBOutlet UITextField *placeTextField;
@property(nonatomic,strong)IBOutlet UIImageView *photoImageView;
@property(nonatomic,strong)IBOutlet UIImageView *signImageView;
@property (strong, nonatomic)  UITextField *currentTextField;
@property (strong, nonatomic) IBOutlet UIView *accessoryView;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, assign) int numLines;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj;
- (IBAction)DigitalSignature:(id)sender;
- (IBAction)dateAction:(id)sender;
- (IBAction)didClickOpenPDF:(id)sender;
- (IBAction)addPhoto:(id)sender;
@end

//
//  personalinfoViewController.h
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#import "UIKeyboardViewController.h"
#import <UIKit/UIKit.h>
#import "Resume.h"
@interface personalinfoViewController : UIViewController<UIKeyboardViewControllerDelegate>
{
     IBOutlet UILabel *datelabel;
     NSMutableArray *personaldetails;
     UIKeyboardViewController *keyBoardController;
}
@property (nonatomic, strong) NSMutableArray *personaldetails;
@property (strong, nonatomic) IBOutlet UIScrollView *personalinfoScrollView;
@property (strong, nonatomic) IBOutlet UITextView *addressTextView;

@property (strong, nonatomic) IBOutlet UITextField *fullnameTextField;
@property (strong, nonatomic) IBOutlet UITextField *languagesKnownTextField;
@property (strong, nonatomic) IBOutlet UITextField *phonenumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailIdTextField;
@property (nonatomic, strong) UITextField *currentTextField;

@property (strong, nonatomic) IBOutlet UIButton *FemaleButton;
@property (strong, nonatomic) IBOutlet UIButton *Male_Button;

@property (strong, nonatomic) IBOutlet UIView *accessoryView;

@property (nonatomic, retain) IBOutlet UILabel *datelabel;
@property (strong, nonatomic) IBOutlet UILabel *genderLabel;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj;
- (IBAction)genderButton_Tapped:(id)sender;
- (IBAction)selectdateButtonTapped:(id)sender;
- (IBAction)doneButton_Pressed:(id)sender;
@end

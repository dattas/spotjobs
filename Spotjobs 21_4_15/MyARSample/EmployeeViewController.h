//
//  EmployeeViewController.h
//  MyARSample
//
//  Created by vairat on 26/10/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostAJobViewController.h"


@interface EmployeeViewController : UIViewController{
    
    
}
@property (nonatomic, retain) IBOutlet UIView *loginView;

@property (strong, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) IBOutlet UIView *menuView;


- (IBAction)submit_Tapped:(id)sender;
- (IBAction)postResume:(id)sender;
- (IBAction)PostaJob:(id)sender;
- (IBAction)bottomMenuButtonTapped:(id)sender;
@end
//  THIS VIEWCONTROLLER NOT REQUIRED
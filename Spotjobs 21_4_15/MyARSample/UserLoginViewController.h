//
//  UserLoginViewController.h
//  MyARSample
//
//  Created by vairat on 19/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import "FBRequestWrapper.h"
#import "FBRequest.h"
#import "UserDataXMLParser.h"

@interface UserLoginViewController : UIViewController<FBSessionDelegate,FBRequestDelegate, NSURLConnectionDelegate, UITextFieldDelegate, NSXMLParserDelegate,UserDataXMLParser, UITabBarDelegate, UITabBarControllerDelegate>{
    
    Facebook *facebook;
    FBRequestWrapper *FRwrapper;
    NSString * facebookid;
    NSString * facebookname;
    NSMutableDictionary *facebookdictionary;
    
    NSMutableData *_responseData;
    
    
}
@property (nonatomic, strong) Facebook *facebook;
@property (nonatomic, strong) IBOutlet UITextField *userName_Textfield;
@property (nonatomic, strong) IBOutlet UITextField *pwd_Textfield;
@property (strong, nonatomic) IBOutlet UIButton *checkAvailability_Button;
- (IBAction)availability_Action:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil userType:(NSString *)user;

- (IBAction)signIn_Action:(id)sender;
- (IBAction)signUp_Action:(id)sender;
- (IBAction)loginWithFB_Action:(id)sender;
-(IBAction)backaction;
//fb
@property (strong, nonatomic) IBOutlet UINavigationItem *customnavbar;
@property (strong, nonatomic) IBOutlet UINavigationBar *customnavigationbar;

- (void)fbDidExtendToken:(NSString *)accessTokens expiresAt:(NSDate *)expiresAt;

- (void)loginToFacebook:(id) loginDelegate;
- (void)logoutFromFacebook: (id) logoutDelegate;
- (void)faceBookImageDataSent;
- (void)publishPostWithDelegate:(id) _delegate ;

@end

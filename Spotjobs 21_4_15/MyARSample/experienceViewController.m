//
//  experienceViewController.m
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#define kOFFSET_FOR_KEYBOARD 135
#import "experienceViewController.h"
#import "AppDelegate.h"
#import "MediatorViewController.h"
@interface experienceViewController ()
{
    AppDelegate *appDelegate;
    UILabel *mylabel;
    NSString *textView_Text;
    Resume *currRes;
    
    NSMutableDictionary *exp_Dictionary;
}
@property (nonatomic,strong)IBOutlet UILabel *mylabel;
@property (nonatomic,strong)NSString *textView_Text;

@end

@implementation experienceViewController

@synthesize currentExperienceObj;
@synthesize experienceScrollView;
@synthesize currentTextField;
@synthesize accessoryView;
@synthesize mylabel;
@synthesize textView_Text;
@synthesize jobresponsibiltyTextView;
@synthesize experiencedetails;
@synthesize companyTextField;
@synthesize positionTextField;
@synthesize periodTextField;
@synthesize locationTextfield;
@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
               currRes = resumeObj;
               experiencedetails=[[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.title=@"Experience";
    textView_Text=@"";
    [super viewDidLoad];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back.png"]
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(backAction)];
    
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Save.png"]
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(saveData)];
    
    if(currentExperienceObj)
        [self loadExperienceDetails];

}

-(void)viewWillAppear:(BOOL)animated
{
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
	[keyBoardController addToolbarToKeyboard];
  //  [keyBoardController re]

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([appDelegate isIphone5]) {
        experienceScrollView.frame=CGRectMake(0,   0, 320, 500);
        [experienceScrollView setContentSize:CGSizeMake(320, 678)];
        
    }
    else
    {
        experienceScrollView.frame=CGRectMake(0,  0, 320, 460);
        [experienceScrollView setContentSize:CGSizeMake(320, 678)];
    }
    //[self performSelector:@selector(loadExperienceDetails) withObject:nil afterDelay:0.01];
    
}
-(void)backAction
{
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

-(void)saveData
{
    
    
    
    BOOL success = [self addExperienceinfo];
   
    if(success)
    {
        exp_Dictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SortDic"] mutableCopy];
        if(!exp_Dictionary){
            exp_Dictionary = [[NSMutableDictionary alloc]init];
        }
        NSMutableArray *arr = [[exp_Dictionary objectForKey:@"Experience"]mutableCopy];
        [arr addObject:self.companyTextField.text];
        [exp_Dictionary setValue:arr forKey:@"Experience"];
        
        [[NSUserDefaults standardUserDefaults] setValue:exp_Dictionary forKey:@"SortDic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Saved" message:@"data saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"data not  saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }


}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    MediatorViewController *mVc = [[MediatorViewController alloc]initWithNibName:@"MediatorViewController" bundle:Nil resume:currRes listType:@"experience"];
    mVc.managedObjectContext = self.managedObjectContext;
    [self.navigationController pushViewController:mVc animated:NO];
    
    
}


-(BOOL) addExperienceinfo
{
    
    NSManagedObjectContext *context = self.managedObjectContext;
    
    if(currentExperienceObj){
        
        currentExperienceObj.company      = self.companyTextField.text;
        currentExperienceObj.position     = self.positionTextField.text;
        currentExperienceObj.period       = self.periodTextField.text;
        currentExperienceObj.location     = self.locationTextfield.text;
        currentExperienceObj.jobresponse  = self.jobresponsibiltyTextView.text;
        currentExperienceObj.ownby        = currRes;
        
    }
    else{
    
    Experience *experience = (Experience *)[NSEntityDescription insertNewObjectForEntityForName:@"Experience" inManagedObjectContext:context];
    
    experience.company      = self.companyTextField.text;
    experience.position     = self.positionTextField.text;
    experience.period       = self.periodTextField.text;
    experience.location     = self.locationTextfield.text;
    experience.jobresponse  = self.jobresponsibiltyTextView.text;
    experience.insertedTime = [NSDate date];    
    experience.ownby        = currRes;
    }
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding Experience failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}



-(void) loadExperienceDetails
{

    /*
    NSSet *experienceSet  = currRes.experience;
    NSArray *experienceArray = [experienceSet allObjects];
    
    for (Experience *exp in experienceArray) {
        
        NSLog(@"projecttitle::%@",exp.company);
        NSLog(@"duration::%@",exp.position);
        NSLog(@"role::%@",exp.period);
        NSLog(@"teamsize::%@",exp.location);
        NSLog(@"expertise::%@",exp.jobresponse);
        
    }*/

    self.companyTextField.text         =currentExperienceObj.company ;
    self.positionTextField.text        =currentExperienceObj.position;
    self.periodTextField.text          =currentExperienceObj.period ;
    self.locationTextfield.text        =currentExperienceObj.location;
    self.jobresponsibiltyTextView.text =currentExperienceObj.jobresponse ;
 
    
    
}

//=======METHOD TO ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN =========//

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        NSLog(@"INSIDE MOVEDUP AND SCROLL VIEW Y IS::%f",rect.origin.y);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD+20;
        
        
        
    }
    else
    {
        
        rect.origin.y += kOFFSET_FOR_KEYBOARD+20;
        
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    
    [textField setInputAccessoryView:accessoryView];
    
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=3  )
        {
            
            [self setViewMovedUp:YES];
        }
    }
    else
    {
        
        if( textField.tag<=3   )
        {
            
            [self setViewMovedUp:YES];
        }
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=3  )
        {
            [self setViewMovedUp:NO];
            
        }
        
    } else
    {
        
        if( textField.tag<=3 )
        {
            
            [self setViewMovedUp:NO];
        }
        
    }
    
}
//================================================================================================//
//=======================METHOD TO DONE BUTTON ====================//

-(IBAction)doneButton_Pressed:(id)sender
{
    
    [currentTextField resignFirstResponder];
    [jobresponsibiltyTextView resignFirstResponder];
}



//================UITEXTVIEW DELEGATE METHODS======================//

- (BOOL)textViewShouldBeginEditing:(UITextView *)aTextView
{
    NSLog(@"textfieldbegin");
    [self setViewMovedUp:YES];
    mylabel.hidden = YES;
    
    [jobresponsibiltyTextView setInputAccessoryView:accessoryView];
    
    return YES;
    
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if ([appDelegate isIphone5]) {
        
        
        if(jobresponsibiltyTextView.text.length == 0){
            mylabel.hidden = NO;
            [jobresponsibiltyTextView resignFirstResponder];
        }
        textView_Text = textView.text;
    }else
    {
        NSLog(@"textfieldchange");
        if(jobresponsibiltyTextView.text.length == 0){
            mylabel.hidden = NO;
            [jobresponsibiltyTextView resignFirstResponder];
        }
        textView_Text = textView.text;
        
    }
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{  NSLog(@"textfieldend");
    [self setViewMovedUp:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

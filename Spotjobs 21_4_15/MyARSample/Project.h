//
//  Project.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Resume;

@interface Project : NSManagedObject

@property (nonatomic, retain) NSString * duration;
@property (nonatomic, retain) NSString * expertise;
@property (nonatomic, retain) NSString * projecttitle;
@property (nonatomic, retain) NSString * role;
@property (nonatomic, retain) NSString * teamsize;
@property (nonatomic, retain) NSDate * insertedTime;
@property (nonatomic, retain) Resume *ownby;

@end

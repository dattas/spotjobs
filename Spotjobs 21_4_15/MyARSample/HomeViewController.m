//
//  HomeViewController.m
//  MyARSample
//
//  Created by vairat on 22/08/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//
#import <MapKit/MapKit.h>
#import "HomeViewController.h"
#import "ViewController.h"
#import "ProductAnnotation.h"
#import "ZSPinAnnotation.h"
#import "AppDelegate.h"
#import "ProductCell.h"
#import "Product.h"


#define ProductCellHeight 64.0;
@interface HomeViewController (){
    
    AppDelegate *appDelegate;
    NSIndexPath * openProductIndex;
   
    
    NSString *category;
    NSURLConnection *conn;
    BOOL isListViewLoaded;
    BOOL isMapViewLoaded;
    BOOL isARViewLoaded;
    ViewController *ref;
    NSInteger previousButtontag;
    NSMutableArray *jobsList;
}
@property (nonatomic, readwrite)NSInteger previousButtontag;
@property (nonatomic, strong) JobDetailViewController *jobDetailController;
-(void)getCurrentLocation;
@end

@implementation HomeViewController
@synthesize map_View;
@synthesize listView;
@synthesize tblView;
@synthesize previousButtontag;

@synthesize mkView;
@synthesize locationManager;
@synthesize currentLocation;
@synthesize jobDetailController;
@synthesize bottom_Menu;

@synthesize test;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil jobsList:(NSArray *)jobsArr
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        jobsList = [[NSMutableArray alloc]init];
        [jobsList addObjectsFromArray:jobsArr];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Search Results";
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    openProductIndex = nil;
    
    [mkView setMapType:MKMapTypeStandard];
    [mkView setZoomEnabled:YES];
    [mkView setScrollEnabled:YES];
    isMapViewLoaded = NO;
    isARViewLoaded = NO;
    previousButtontag = 0;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    [self.mkView setDelegate:self];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
     
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
	{
		UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
		[self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
        
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self.navigationController.parentViewController action:@selector(revealToggle:)];
        
        
        
        
        
        
    }
    
   [self.view addSubview:self.listView];
   // [self.view addSubview:self.map_View];
    isListViewLoaded = YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.tblView reloadData];
    
    [self.view bringSubviewToFront:self.bottom_Menu];
    // [self getCurrentLocation];
   // [self.mkView reloadInputViews];
}



-(void)getCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
}
- (void) setSearchCategoryID:(NSString *) catId
{
    NSLog(@"%@",catId);
    category = catId;
}


#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    NSLog(@"tableView numberOfRowsInSection");
    return [jobsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == [appDelegate.productsList_Array count]) {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell) {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        loadingCell.textLabel.text = @"Loading ....";
        loadingCell.textLabel.textColor = [UIColor blackColor];//[UIColor colorWithRed:255/255.0 green:97/255.0 blue:55/255.0 alpha:1.0];
        
        loadingCell.backgroundColor = [UIColor whiteColor];
        
        UIActivityIndicatorView *loadingActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        loadingActivityView.backgroundColor = [UIColor clearColor];
        [loadingActivityView startAnimating];
        loadingCell.accessoryView = loadingActivityView;
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    
    ProductCell *cell = (ProductCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
        
        for (UIView *cellview in views) {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProductCell*)cellview;
            }
        }
        
    }
    
    JobDetails *job = [jobsList objectAtIndex:indexPath.row];
    
    cell.jobTitle_Label.text = job.jobRole;
    cell.jobDesc_Label.text = job.jobDescription;
    
    if (openProductIndex && (indexPath.row == openProductIndex.row) ) {
        
        cell.headerContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
        cell.headerContainerView.layer.shadowOpacity = 1.0;
        cell.headerContainerView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
        cell.headerContainerView.layer.shadowRadius = 5.0;
        
        if (jobDetailController) {
            if ([jobDetailController.view superview]) {
                [jobDetailController.view removeFromSuperview];
            };
            jobDetailController = nil;
        }
        
    JobDetailViewController *jobDetailVC = [[JobDetailViewController alloc] initWithNibName:@"JobDetailViewController" bundle:nil];
        jobDetailVC.delegate = self;
        
        
        CGRect frame = jobDetailVC.view.frame;
        float headerHeight = ProductCellHeight;
        
        frame.origin.y = headerHeight+90;
        frame.size.height = self.tblView.frame.size.height - headerHeight;
        jobDetailVC.view.frame = frame;
        
               
        [cell.contentView addSubview:jobDetailVC.view];
        [cell.contentView bringSubviewToFront:cell.headerContainerView];
        
       // jobDetailController = jobDetailVC;
        
    }
   
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (openProductIndex && (indexPath.row == openProductIndex.row) ) {
        NSLog(@"ROW=%d toBeHeight=%f",indexPath.row,self.tblView.frame.size.height);
        return self.tblView.frame.size.height;
    }
    
    return ProductCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // **** Close the Product Details **** //
    if (openProductIndex) {
        
        NSLog(@"closing Product List");
        openProductIndex = nil;
        
        int totalRows = [self.tblView numberOfRowsInSection:0];
        
        // Update tapped cell
        [self.tblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // update cells under it
        NSMutableArray *indexPathsArray = [NSMutableArray array];
        for (int i = (indexPath.row + 1); i < totalRows; i++) {
            NSIndexPath *indPath = [NSIndexPath indexPathForRow:i inSection:0];
            [indexPathsArray addObject:indPath];
        }
        
        if (indexPathsArray.count > 0) {
            [self.tblView reloadRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        
        
        self.tblView.scrollEnabled = YES;
        return;
    }
    
    
    
    // **** Open the Product Details **** //
    openProductIndex = indexPath;
    
    float toScrollOffsetY = openProductIndex.row * ProductCellHeight;
    
    // update content size
    if ( (self.tblView.contentSize.height - toScrollOffsetY) < self.tblView.frame.size.height) {
        self.tblView.contentSize = CGSizeMake(self.tblView.contentSize.width, (self.tblView.frame.size.height + toScrollOffsetY));
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.tblView setContentOffset:CGPointMake(self.tblView.contentOffset.x, toScrollOffsetY)];
    } completion:^(BOOL finished) {
        
        int totalRows = [self.tblView numberOfRowsInSection:0];
        
        // Update tapped cell
        [self.tblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // update cells under it
        NSMutableArray *indexPathsArray = [NSMutableArray array];
        for (int i = (indexPath.row + 1); i < totalRows; i++) {
            NSIndexPath *indPath = [NSIndexPath indexPathForRow:i inSection:0];
            [indexPathsArray addObject:indPath];
        }
        
        if (indexPathsArray.count > 0) {
            [self.tblView reloadRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationBottom];
        }
        
    }];
    
    self.tblView.scrollEnabled = NO;
    
    //Product *pro = [productsList objectAtIndex:indexPath.row];
    //[self fetchProductWithProductID:pro.productId];

   
}
//================================================================
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    ProductAnnotation *ann =(ProductAnnotation *)view.annotation;
   
    //[self fetchProductWithProductID:ann.prod.productId];
    /*
    ProductDetailViewController *myref = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil productID:ann.prod.productId];
    
    [self.navigationController pushViewController:myref animated:YES]; */
}
/*
- (void) fetchProductWithProductID:(NSString *) productId {
    
    // Make server call for more products.
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.myrewards.com.au/app/webroot/newapp/get_product.php?id=%@",productId]]];
    
     conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
}

*/









//=========================== MAPVIEW DELEGATES METHODS  ===================================//
- (void) addNewAnnotations
{
    NSLog(@"==========AddNewAnnotations===========");
        //int i=0;
    NSLog(@"count %d",[appDelegate.productsList_Array count] );
        for(Product *prod in appDelegate.productsList_Array)
        {
            NSLog(@"\n Product offers in addNewAnnotations Method = %@",prod.productName);
            ProductAnnotation *ann = [[ProductAnnotation alloc]init];
            ann.coordinate = prod.coordinate;
            ann.title = prod.productName;
            ann.prod = prod;
            ann.pinColor = prod.pin_type;
            [self.mkView addAnnotation:ann];
        }
        
//    }
    [self.mkView reloadInputViews];
    NSLog(@"reload");
}



#pragma mark CoreLocation Delegate Methods.........

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"==========locationManager===========");
    CLLocationCoordinate2D coordinate = [newLocation coordinate];
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    
    NSLog(@"latitude : %@", latitude);
    NSLog(@"longitude : %@",longitude);
    self.currentLocation = coordinate;
    MKCoordinateRegion region = mkView.region;
    
    region.center.latitude = self.currentLocation.latitude;
    region.center.longitude = self.currentLocation.longitude;
    
    if ([appDelegate.productsList_Array count] == 0) {
        region.span.longitudeDelta = 0.05f;
        region.span.latitudeDelta = 0.0f;
    }
    else{
        region.span.longitudeDelta =  17.05f;
        region.span.latitudeDelta =  17.05f;
    }
    [mkView setRegion:region animated:YES];
    [mkView showsUserLocation];
    
//    if([appDelegate.productsList_Array count] == 0)
//        [self searchProductsOnKeywordBasis];
//    else
        [self addNewAnnotations];
    [locationManager stopUpdatingLocation];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (error.code ==  kCLErrorDenied) {
        NSLog(@"Location manager denied access - kCLErrorDenied");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location issue"
                                                        message:@"Your location cannot be determined."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    NSLog(@"viewForAnnotation");
    
    if ([[(ProductAnnotation*)annotation title] isEqualToString:@"Current Location"]) {
        NSLog(@"viewForAnnotation1");
        return nil;
        
    }
    
    static NSString *annotationIdentifier = @"AnnotationIdentifier";
    
    MKPinAnnotationView *pinView = (MKPinAnnotationView *) [mapView
                                                            dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    if (!pinView)
    {
        NSLog(@"viewForAnnotation2");
        pinView = [[MKPinAnnotationView alloc]
                   initWithAnnotation:annotation
                   reuseIdentifier:annotationIdentifier];
        
        if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"1"]) {
            pinView.image = [ZSPinAnnotation pinAnnotationWithRed:51 green:79 blue:163];
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"2"]) {
            pinView.image = [ZSPinAnnotation pinAnnotationWithRed:255 green:0 blue:0];
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"3"]) {
            pinView.image = [ZSPinAnnotation pinAnnotationWithRed:213 green:135 blue:183];
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"4"]) {
            pinView.image = [ZSPinAnnotation pinAnnotationWithRed:118 green:123 blue:127];
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"5"]) {
            pinView.image = [ZSPinAnnotation pinAnnotationWithRed:38 green:182 blue:133];
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"6"]) {
            pinView.image = [ZSPinAnnotation pinAnnotationWithRed:126 green:66 blue:152];
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"7"]) {
            pinView.image = [ZSPinAnnotation pinAnnotationWithRed:190 green:219 blue:126];
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"0"]) {
            pinView.image = [ZSPinAnnotation pinAnnotationWithRed:10 green:19 blue:126];
        }
        else{
        // pinView.image = [ZSPinAnnotation pinAnnotationWithColor:[UIColor yellowColor]];
         pinView.image =  [ZSPinAnnotation pinAnnotationWithRed:25 green:234 blue:34];
            NSLog(@"prashanth");
        }
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        pinView.userInteractionEnabled = YES;
        
    }
    else
    {
        pinView.annotation = annotation;
    }
    
    return pinView; 
}


//=============

/*
- (IBAction)bottomMenuButton_Tapped:(id)sender{


    switch ([sender tag]) {
        case 1:
               
                   if(isMapViewLoaded){
                       NSLog(@"map alredy loaded....");
                       [self.view bringSubviewToFront:self.map_View];
                       
                   }
                   else{
                       NSLog(@"map loading....");
                       [self getCurrentLocation];
                       [self.view addSubview:self.map_View];
                       [self.mkView reloadInputViews];
                       isMapViewLoaded = YES;
                   }
                   [self.view bringSubviewToFront:self.bottom_Menu];
                   break;
            
        case 2:
                   ref = [[ViewController alloc]initWithNibName:@"ViewController_iPhone" bundle:nil];
                   [self.view addSubview:ref.view];
                   isARViewLoaded = YES;
            
                   [self.view bringSubviewToFront:self.bottom_Menu];
                   break;
        case 3:
                   [self.view bringSubviewToFront:self.listView];
                   [self.view bringSubviewToFront:self.bottom_Menu];
                   break;
        default:
            break;
    }


}
*/
- (IBAction)mapButton_Tapped:(id)sender {
    
    if(isMapViewLoaded){
        NSLog(@"map alredy loaded....");
        [self.view bringSubviewToFront:self.map_View];
        
    }
    else{
        NSLog(@"map loading....");
    [self getCurrentLocation];
    [self.view addSubview:self.map_View];
    [self.mkView reloadInputViews];
        isMapViewLoaded = YES;
    }
    [self.view bringSubviewToFront:self.bottom_Menu];
}

- (IBAction)arButton_Tapped:(id)sender {
    
//    if(isARViewLoaded)
//    {
//       [self.view bringSubviewToFront:ref.view]; 
//    }
//    else{
    ref = [[ViewController alloc]initWithNibName:@"ViewController_iPhone" bundle:nil];
    [self.view addSubview:ref.view];
        isARViewLoaded = YES;
  //  }
    [self.view bringSubviewToFront:self.bottom_Menu];
}

- (IBAction)listButton_Tapped:(id)sender {
    
    [self.view bringSubviewToFront:self.listView];
    [self.view bringSubviewToFront:self.bottom_Menu];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

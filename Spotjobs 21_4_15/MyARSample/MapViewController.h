//
//  MapViewController.h
//  MyARSample
//
//  Created by vairat on 04/12/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol MapDelegate;

@interface MapViewController : UIViewController<MKMapViewDelegate>
 {
    
    CLGeocoder *_geocoder;
    CLLocationCoordinate2D currentCoordinates;
    __unsafe_unretained id <MapDelegate> delegate;
}
@property (unsafe_unretained) id <MapDelegate> delegate;
@property (nonatomic, strong) CLGeocoder *geocoder;
@property (nonatomic, readwrite) CLLocationCoordinate2D currentCoordinates;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil location:(NSString *)locaddr;
@end

@protocol MapDelegate <NSObject>
-(void) mapViewControllerDismissed:(CLLocationCoordinate2D)coordinate;
@end






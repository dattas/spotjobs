//
//  AppDelegate.m
//  MyARSample
//
//  Created by vairat on 26/06/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>

#import "HomeViewController.h"
#import "SearchCriteriaViewController.h"
#import "HomePageViewController.h"

#define Login_Token_Delimiter @""
#define Login_Token_Expiry_period (7*24*60*60)//1*60
#define Login_Token_Username_Key @"Login_Token_Username_Key"
#define Login_Token_Password_Key @"Login_Token_Password_Key"
#define Login_Token_Timestamp_Key @"Login_Token_Timestamp_Key"
#define User_object_Key @"User_object"

@implementation AppDelegate
@synthesize homeViewController;
@synthesize searchCriteriaViewController;
@synthesize homePageViewController;
@synthesize productsList_Array;
@synthesize categoryList_Array;
@synthesize resumeObjects_Array;
@synthesize sessionUser;
@synthesize startDate;
@synthesize endDate;
@synthesize selectdCatID;
@synthesize selectdCatName;
@synthesize tabBarController;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize signImage;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.productsList_Array = [[NSMutableArray alloc]init];
    self.categoryList_Array = [[NSMutableArray alloc]init];
    self.resumeObjects_Array = [[NSMutableArray alloc]init];
    
   // homeViewController = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
        [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self showHomeScreen];
    
        return YES;
}


-(void)showHomeScreen{
    
    homePageViewController = [[HomePageViewController alloc]initWithNibName:@"HomePageViewController" bundle:nil];
    searchCriteriaViewController = [[SearchCriteriaViewController alloc]initWithNibName:@"SearchCriteriaViewController" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homePageViewController];
    navigationController.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    RevealController *revealController = [[RevealController alloc] initWithFrontViewController:navigationController rearViewController:searchCriteriaViewController];
    
    
    self.window.rootViewController = revealController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

    
}


- (BOOL) isIphone5 {
    
    if( [ [ UIScreen mainScreen ] bounds ].size.height == 568.0){
        
        NSLog(@"====IPHONE5====");
        return YES;
    }
    NSLog(@"====IPHONE4====");
    return NO;
}

- (void) logoutSession{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"USERID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self showHomeScreen];
}

/*
- (void) loginSuccessfulWithUserdetails:(User *) userDetails password:(NSString *) pWord{
    
    self.sessionUser = userDetails;
    
    NSDate *loginDate = [NSDate date];
    NSString *dateString = [loginDate description];
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.username forKey:Login_Token_Username_Key];
    [[NSUserDefaults standardUserDefaults] setValue:pWord forKey:Login_Token_Password_Key];
    [[NSUserDefaults standardUserDefaults] setValue:dateString forKey:Login_Token_Timestamp_Key];
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_id forKey:@"client_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.domain_id forKey:@"domain_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.type forKey:@"type"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.first_name forKey:@"first_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.last_name forKey:@"last_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.state forKey:@"state"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.country forKey:@"country"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.mobile forKey:@"mobile"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.card_ext forKey:@"card_ext"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_name forKey:@"client_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.newsletter forKey:@"newsletter"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.clientDomainName forKey:@"clientDomainName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    
}
*/




-(NSManagedObjectContext *)getManagedObject{
    
    
    return self.managedObjectContext;
    
}











#pragma mark -- Favorite methods

- (BOOL) addProductToFavorites:(Product *) product {
    
    if ([self productExistsInFavorites:product]) {
        // Product already exists. return.
        return NO;
    }
    
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *favoriteProduct = [NSEntityDescription insertNewObjectForEntityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [favoriteProduct setValue:product.productId forKey:@"productId"];
    [favoriteProduct setValue:product.productName forKey:@"productName"];
    [favoriteProduct setValue:product.productOffer forKey:@"productHighlight"];
   // [favoriteProduct setValue:self.sessionUser.userId forKey:@"userId"];
    [favoriteProduct setValue:@"1" forKey:@"userId"];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Adding product failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

- (BOOL) removeProductFromfavorites:(Product *) product {
    
    if ([self productExistsInFavorites:product]) {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        // Fetch the said NSManagedObject(favoriteProduct)
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
        [fetchRequest setEntity:favProduct];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId == %@ && userId==1",product.productId];
        [fetchRequest setPredicate:predicate];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedObjects.count > 0) {
            NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
            
            [context deleteObject:favoriteProduct];
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Removing product failed %@", [error localizedDescription]);
                
                // Removing object failed.
                return NO;
            }
            
            // Success
            return YES;
        }
        
        // There is no Product as such specified by input param in favorites.
    }
    
    // There is no Product as such specified by input param in favorites.
    return NO;
}



- (BOOL) productExistsInFavorites:(Product *) product {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId==%@",product.productId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0) {
        return YES;
    }
    
    return NO;
}

- (NSArray *) myFavoriteProducts {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId==1"];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *productsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects) {
        NSLog(@"Product Name: %@", [info valueForKey:@"productName"]);
        NSLog(@"Product Id: %@", [info valueForKey:@"productId"]);
        
        Product *pr = [[Product alloc] init];
        pr.productId = [[info valueForKey:@"productId"] copy];
        pr.productName = [[info valueForKey:@"productName"] copy];
        pr.productOffer = [[info valueForKey:@"productHighlight"] copy];
        [productsArr addObject:pr];
    }
    
    return productsArr;
}


-(NSMutableArray *)sorting:(NSMutableArray *)insertOrder_Array Source:(NSMutableArray *)source_Array listType:(int)type{
    
    NSLog(@"appdelegate sorting");
    for(int i =0;i<[insertOrder_Array count];i++)
    {
        
        for (int j=i;j<[source_Array count]; j++)
        {
            
            
            switch (type) {
                case 1:{
                    Education *edu = [source_Array objectAtIndex:j];
                    if([[insertOrder_Array objectAtIndex:i]isEqualToString:edu.degreecourse])
                    {
                        [source_Array removeObject:edu];
                        [source_Array insertObject:edu atIndex:i];
                    }}
                    break;
                case 2:{
                    Project *pro = [source_Array objectAtIndex:j];
                    if([[insertOrder_Array objectAtIndex:i]isEqualToString:pro.projecttitle])
                    {
                        [source_Array removeObject:pro];
                        [source_Array insertObject:pro atIndex:i];
                    }}
                    break;
                case 3:{
                    Experience *exp = [source_Array objectAtIndex:j];
                    if([[insertOrder_Array objectAtIndex:i]isEqualToString:exp.company])
                    {
                        [source_Array removeObject:exp];
                        [source_Array insertObject:exp atIndex:i];
                    }}
                    break;
                case 4:{
                    Reference *ref = [source_Array objectAtIndex:j];
                    if([[insertOrder_Array objectAtIndex:i]isEqualToString:ref.referencename])
                    {
                        [source_Array removeObject:ref];
                        [source_Array insertObject:ref atIndex:i];
                    }}
                    break;
                    
                default:
                    break;
            }
            
            
            
            
            
        }
    }
    return source_Array;
    
}











//============================ ++++++++++ RESUME COREDATA METHODS ++++++++++ =====================//

/*
-(BOOL) addNewResume:(NSString *)title{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if([self isResumeExits:title]){
        
        return NO;
        
    }
    
       
    NSManagedObject *newResume = [NSEntityDescription insertNewObjectForEntityForName:@"Resume" inManagedObjectContext:context];
        [newResume setValue:@"1" forKey:@"resumeId"];
        [newResume setValue:title forKey:@"title"];
        
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Adding Resume failed %@", [error localizedDescription]);
            return NO;
        }

    
    return YES;
}


-(BOOL) isResumeExits:(NSString *)name{
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *resDesc = [NSEntityDescription entityForName:@"Resume" inManagedObjectContext:context];
    [fetchRequest setEntity:resDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title==%@",name];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0) {
        return YES;
    }
    
    return NO;
    
}

-(NSArray *)getResumes{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *resumeDesc = [NSEntityDescription entityForName:@"Resume" inManagedObjectContext:context];
    [fetchRequest setEntity:resumeDesc];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        NSLog(@"Resume Title: %@", [info valueForKey:@"title"]);
       
        
        Resume *resume = [[Resume alloc] init];
        resume.title = [[info valueForKey:@"title"] copy];
        resume.resumeId = [[info valueForKey:@"resumeId"] copy];
        
        [arr addObject:resume];
    }
    
        return arr;
}


-(BOOL) addPersonalinfo:(Personal *) personalinfo to:(Resume *)resume
{
      
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *newPersonal = [NSEntityDescription insertNewObjectForEntityForName:@"Personal" inManagedObjectContext:context];
    
   
    
  
    
    [newPersonal setValue:personalinfo.title forKey:@"title"];
    [newPersonal setValue:personalinfo.fullname forKey:@"fullName"];
    [newPersonal setValue:personalinfo.birthdate forKey:@"dob"];
    [newPersonal setValue:personalinfo.permanentaddress forKey:@"address"];
    [newPersonal setValue:personalinfo.languagesknown forKey:@"language"];
    [newPersonal setValue:personalinfo.phonenumber forKey:@"phNo"];
    [newPersonal setValue:personalinfo.emailid forKey:@"emailId"];
    [newPersonal setValue:personalinfo.gender forKey:@"gender"];
    [newPersonal setValue:resume forKey:@"ownby"];
    //newPersonal.ownby = (Resume *)[fetchedObjects objectAtIndex:0];
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding Personal Details failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}*/
/*
-(Personal *)getPersonalinfo:(NSString *)name
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *personalDesc = [NSEntityDescription entityForName:@"Personal" inManagedObjectContext:context];
    [fetchRequest setEntity:personalDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title==%@",name];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    if([fetchedObjects count] > 0)
    {
        
        NSManagedObject *info = [fetchedObjects objectAtIndex:0];
        
        Personal *personalInfo = [[Personal alloc]init];
        personalInfo.title = [[info valueForKey:@"title"] copy];
        personalInfo.fullname = [[info valueForKey:@"fullName"] copy];
        personalInfo.birthdate = [[info valueForKey:@"dob"] copy];
        personalInfo.permanentaddress=[[info valueForKey:@"address"]copy];
        personalInfo.languagesknown=[[info valueForKey:@"languages"]copy];
        personalInfo.phonenumber=[[info valueForKey:@"phNo"]copy];
        personalInfo.emailid=[[info valueForKey:@"emailId"]copy];
        personalInfo.gender=[[info valueForKey:@"gender"]copy];
        [arr addObject:personalInfo];
    }
    if([arr count]>0)
        return [arr objectAtIndex:0];
    else
        return NULL;
}

//----------------- project data
-(BOOL) addProjectinfo:(Project *) projectinfo
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *project = [NSEntityDescription insertNewObjectForEntityForName:@"Project" inManagedObjectContext:context];
    
    [project setValue:projectinfo.projecttitle forKey:@"projecttitle"];
    [project setValue:projectinfo.durationtime forKey:@"duration"    ];
    [project setValue:projectinfo.role         forKey:@"role"        ];
    [project setValue:projectinfo.teamsize     forKey:@"teamsize"    ];
    [project setValue:projectinfo.expertise    forKey:@"expertise"   ];
   
     NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding project data failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

-(NSArray *)getProjectinfo
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *projectDesc = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:context];
    [fetchRequest setEntity:projectDesc];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *projectsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
               
        Project *project = [[Project alloc] init];
        project.projecttitle = [[info valueForKey:@"projecttitle"] copy];
        project.durationtime= [[info valueForKey:@"duration"] copy];
        project.role=[[info valueForKey:@"role"]copy];
        project.teamsize=[[info valueForKey:@"teamsize"]copy];
        project.expertise=[[info valueForKey:@"expertise"]copy];
        
        [projectsArr addObject:project];
    }
    return projectsArr;
    
}

//-----------------Education data
-(BOOL) addEducationinfo:(Education *) educationinfo
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *education = [NSEntityDescription insertNewObjectForEntityForName:@"Education" inManagedObjectContext:context];
    
    [education setValue:educationinfo.collegeschool  forKey:@"collegeschool"];
    [education setValue:educationinfo.degreecourse    forKey:@"degreecourse"    ];
    [education setValue:educationinfo.result          forKey:@"result"        ];
    [education setValue:educationinfo.universityboard    forKey:@"universityboard"    ];
    [education setValue:educationinfo.yearofpassing    forKey:@"yearofpassing"   ];
    [education setValue:educationinfo.perorcgpa    forKey:@"perorcgpa"   ];
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding Education data failed %@", [error localizedDescription]);
        return NO;
    }
    return YES;
}

-(NSArray *)getEducationinfo
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *educationDesc = [NSEntityDescription entityForName:@"Education" inManagedObjectContext:context];
    [fetchRequest setEntity:educationDesc];
        
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *educationArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        
        
        Education *education = [[Education alloc] init];
        education.collegeschool  = [[info valueForKey:@"collegeschool"] copy];
        education.degreecourse   = [[info valueForKey:@"degreecourse"] copy];
        education.result         = [[info valueForKey:@"result"]copy];
        education.universityboard= [[info valueForKey:@"universityboard"]copy];
        education.yearofpassing  =[[info valueForKey:@"yearofpassing"]copy];
        education.perorcgpa  =[[info valueForKey:@"perorcgpa"]copy];
        [educationArr addObject:education];
    }
    
    
    return educationArr;
    
}

//----------------Experience data
-(BOOL) addExperienceinfo:(Experience *) experienceinfo
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
   
    NSManagedObject *experience = [NSEntityDescription insertNewObjectForEntityForName:@"Experience" inManagedObjectContext:context];
    
    [experience setValue:experienceinfo.company forKey:@"company"];
    [experience setValue:experienceinfo.jobresponsibility forKey:@"jobresponse"    ];
    [experience setValue:experienceinfo.location        forKey:@"location"        ];
    [experience setValue:experienceinfo.period     forKey:@"period"    ];
    [experience setValue:experienceinfo.position   forKey:@"position"   ];
    [experience setValue:experienceinfo.widthoftextview forKeyPath:@"width"];
    [experience setValue:experienceinfo.heightoftextview forKeyPath:@"height"];
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding contact failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}
-(NSArray *)getExperienceinfo
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *expDesc = [NSEntityDescription entityForName:@"Experience" inManagedObjectContext:context];
    [fetchRequest setEntity:expDesc];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *expArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        
        
        Experience *experience = [[Experience alloc] init];
        experience.company = [[info valueForKey:@"company"] copy];
        experience.jobresponsibility= [[info valueForKey:@"jobresponse"] copy];
        experience.location=[[info valueForKey:@"location"]copy];
        experience.period=[[info valueForKey:@"period"]copy];
        experience.position=[[info valueForKey:@"position"]copy];
        experience.widthoftextview=[[info valueForKey:@"width"]copy];
        experience.heightoftextview=[[info valueForKey:@"height"]copy];
        
        [expArr addObject:experience];
    }
    
    
   
    return expArr;
    
    
}
 
//------------------Reference

-(BOOL) addReferenceinfo:(Reference *) referenceinfo
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *reference = [NSEntityDescription insertNewObjectForEntityForName:@"Reference" inManagedObjectContext:context];
    
    [reference setValue:referenceinfo.companyname       forKey:@"company"];
    [reference setValue:referenceinfo.email             forKey:@"email"    ];
    [reference setValue:referenceinfo.phone             forKey:@"phone"        ];
    [reference setValue:referenceinfo.referencename     forKey:@"referencename"    ];
    
    
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding refernce data failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}
-(NSArray *)getReferenceinfo
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *refDesc = [NSEntityDescription entityForName:@"Reference" inManagedObjectContext:context];
    [fetchRequest setEntity:refDesc];
    
   
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *refArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        
        
        Reference *reference = [[Reference alloc] init];
        reference.companyname = [[info valueForKey:@"company"] copy];
        reference.email = [[info valueForKey:@"email"] copy];
        reference.phone=[[info valueForKey:@"phone"]copy];
        reference.referencename=[[info valueForKey:@"referencename"]copy];
        
        
        [refArr addObject:reference];
    }
    
    return refArr;
    
    
    
}

//-------------------Other Info
-(BOOL) addOtherinfo:(OtherInfo *) otherinfo
{
    NSManagedObjectContext *context = [self managedObjectContext];
        
    NSManagedObject *otherInfo = [NSEntityDescription insertNewObjectForEntityForName:@"Other" inManagedObjectContext:context];
    
    [otherInfo setValue:otherinfo.drivinglicenceno forKey:@"drivinglcno"];
    [otherInfo setValue:otherinfo.passportnumber forKey:@"panno"    ];
    [otherInfo setValue:otherinfo.pannumber        forKey:@"passno"        ];
        
    
   NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding otherInfo failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;

}
-(NSArray *)getOtherinfo
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *otherDesc = [NSEntityDescription entityForName:@"Other" inManagedObjectContext:context];
    [fetchRequest setEntity:otherDesc];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *othersArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        OtherInfo *otherinfo = [[OtherInfo alloc] init];
        otherinfo.drivinglicenceno = [[info valueForKey:@"drivinglcno"] copy];
        otherinfo.passportnumber = [[info valueForKey:@"passno"] copy];
        otherinfo.pannumber=[[info valueForKey:@"panno"]copy];
        
        
        [othersArr addObject:otherinfo];
    }
    return othersArr;
    
}
*/

//============================ ++++++++++ END RESUME COREDATA METHODS ++++++++++ =====================//


//====================================Job CoreData methods========================================//
-(BOOL) addJobDetails:(JobDetails *) jobdetails
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    //remove old  object
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *delContact = [NSEntityDescription entityForName:@"JobDetails" inManagedObjectContext:context];
    [fetchRequest setEntity:delContact];
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@ && number==%@",contact.name,contact.number];
    // [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        NSManagedObject *cont = [fetchedObjects objectAtIndex:0];
        
        [context deleteObject:cont];
        
        //NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Removing contact failed %@", [error localizedDescription]);
            
            // Removing object failed.
            // return NO;
        }
        
        // Success
        //return YES;
    }
    
    // NSLog(@"FN %@",projectinfo.projecttitle);
    // NSLog(@"PN: %@",projectinfo.durationtime);
    
    // NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *newPersonal = [NSEntityDescription insertNewObjectForEntityForName:@"JobDetails" inManagedObjectContext:context];
    
//    [newPersonal setValue:jobdetails.companyName    forKey:@"companyname" ];
//    [newPersonal setValue:jobdetails.websiteaddress forKey:@"webaddress"  ];
    [newPersonal setValue:jobdetails.eligiblity     forKey:@"eligibility" ];
    [newPersonal setValue:jobdetails.experience     forKey:@"experience"  ];
   // [newPersonal setValue:jobdetails.location       forKey:@"location"    ];
    [newPersonal setValue:jobdetails.jobRole        forKey:@"jobrole"     ];
    //[newPersonal setValue:referenceinfo.referencename     forKey:@"referencename"    ];
    //[newPersonal setValue:referenceinfo   forKey:@"position"   ];
    //[newPersonal setValue:projectinfo.durationtime forKey:@"emailid"];
    
    
    // NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding contact failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
    
    
    
    
}
-(NSArray *)jobdetails
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *contact = [NSEntityDescription entityForName:@"JobDetails" inManagedObjectContext:context];
    [fetchRequest setEntity:contact];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId==%@",self.sessionUser.userId];
    //[fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *contactsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        //NSLog(@"Contact Name: %@", [info valueForKey:@"fullname"]);
        //NSLog(@"Contact Number: %@", [info valueForKey:@"phonenumber"]);
        
        JobDetails *currContact = [[JobDetails alloc] init];
//        currContact.companyName = [[info valueForKey:@"companyname"] copy];
//        currContact.websiteaddress = [[info valueForKey:@"webaddress"] copy];
        currContact.eligiblity=[[info valueForKey:@"eligibility"]copy];
        currContact.experience=[[info valueForKey:@"experience"]copy];
       // currContact.location=[[info valueForKey:@"location"]copy];
        currContact.jobRole =[[info valueForKey:@"jobrole"]copy];
        
        //currContact.referencename=[[info valueForKey:@"referencename"]copy];
        //currContact.position=[[info valueForKey:@"position"]copy];
        
        [contactsArr addObject:currContact];
    }
    
    return contactsArr;
}


//Job Summary
-(BOOL) addjobSummary:(jobSummary *) jobsummary
{
    NSManagedObjectContext *context = [self managedObjectContext];
    //remove old  object
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *delContact = [NSEntityDescription entityForName:@"JobSummary" inManagedObjectContext:context];
    [fetchRequest setEntity:delContact];
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@ && number==%@",contact.name,contact.number];
    // [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        NSManagedObject *cont = [fetchedObjects objectAtIndex:0];
        
        [context deleteObject:cont];
        
        //NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Removing contact failed %@", [error localizedDescription]);
            
            // Removing object failed.
            // return NO;
        }
        
        // Success
        //return YES;
    }
    
    // NSLog(@"FN %@",projectinfo.projecttitle);
    // NSLog(@"PN: %@",projectinfo.durationtime);
    
    // NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *newPersonal = [NSEntityDescription insertNewObjectForEntityForName:@"JobSummary" inManagedObjectContext:context];
    
    [newPersonal setValue:jobsummary.companyProfile    forKey:@"companyprofile" ];
    [newPersonal setValue:jobsummary.jobDescription    forKey:@"jobdesc"  ];
    [newPersonal setValue:jobsummary.jobRequirements   forKey:@"jobreq" ];
    if (![context save:&error])
    {
        NSLog(@"Adding contact failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
    
    
}
-(NSArray *)jobsummary
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *contact = [NSEntityDescription entityForName:@"JobSummary" inManagedObjectContext:context];
    [fetchRequest setEntity:contact];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId==%@",self.sessionUser.userId];
    //[fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *contactsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        jobSummary *currContact = [[jobSummary alloc] init];
        currContact.companyProfile = [[info valueForKey:@"companyprofile"]  copy];
        currContact.jobDescription = [[info valueForKey:@"jobdesc"       ]  copy];
        currContact.jobRequirements= [[info valueForKey:@"jobreq"        ]  copy];
        
        [contactsArr addObject:currContact];
    }
    
    return contactsArr;
    
    
    
    
}
#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}



- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FavoritesDataModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

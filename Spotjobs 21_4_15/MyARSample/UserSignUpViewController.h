//
//  UserSignUpViewController.h
//  MyARSample
//
//  Created by vairat on 19/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataXMLParser.h"

@interface UserSignUpViewController :UIViewController<NSURLConnectionDelegate, UITextFieldDelegate, NSXMLParserDelegate, UserDataXMLParser> {
    
    NSMutableData *_responseData;
}
@property (strong, nonatomic) IBOutlet UITextField *userName_Field;
@property (strong, nonatomic) IBOutlet UITextField *password_TextField;
@property (strong, nonatomic) IBOutlet UITextField *conformPwd_TextField;
@property (strong, nonatomic) IBOutlet UILabel *warningLabel;
@property (strong, nonatomic) IBOutlet UIImageView *checkAvailability_ImageView;
- (IBAction)actin:(id)sender;
@property (strong, nonatomic) IBOutlet UINavigationBar *signInNavigationBar;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *checkAvailability_ActivityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil userType:(NSString *)user;
- (IBAction)registerButton_Action:(id)sender;
- (IBAction)cancelAction;
@end

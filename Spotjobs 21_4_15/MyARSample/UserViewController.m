//
//  UserViewController.m
//  MyARSample
//
//  Created by vairat on 26/10/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "UserViewController.h"
#import "HelpViewController.h"
#import "ListViewController.h"
#import "ResumeViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "SKBounceAnimation.h"
#import "Cell1.h"
#import "Cell2.h"

@interface UserViewController ()
{
     BOOL menuViewUp;
    ResumeViewController *resume;
    AppDelegate *appDelegate;
}
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;
@end

@implementation UserViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTranslucent:NO];
    appDelegate =  (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //menuViewUp=NO;
    self.expansionTableView.sectionFooterHeight = 0;
    self.expansionTableView.sectionHeaderHeight = 0;
    self.isOpen = NO;
    
    self.title = @"Home";
    
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"logout.png"]
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(logoutTapped)];
    
    
    
    UIViewController *parentViewController = appDelegate.tabBarController.parentViewController;
    RevealController *baseParentViewController = (RevealController *)parentViewController.parentViewController;
//    Class parentClass = [baseParentViewController class];
//    NSString *pclassName = NSStringFromClass(parentClass);
//    
//    NSLog(@"pclassName::%@",pclassName);
    
    
    if ([baseParentViewController respondsToSelector:@selector(revealGesture:)] && [baseParentViewController respondsToSelector:@selector(revealToggle:)])
	{
        UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:baseParentViewController action:@selector(revealGesture:)];
        [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:baseParentViewController
                                                                             action:@selector(revealToggle:)];

    }

    
    
}

-(void)logoutTapped{
    
    
    NSLog(@"logoutTapped");
    
    UIAlertView *logoutAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to Logout" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    [logoutAlert show];
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (buttonIndex == alertView.cancelButtonIndex) {
        
        [appDelegate logoutSession];
    }
    
    
}
/*
-(void)backaction
{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        //[self.navigationController popViewControllerAnimated:NO];
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:NO];
                    }
                    completion:NULL];
    
}

- (IBAction)menuButton_Action:(id)sender
{
    
    if(menuViewUp)
    {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect ContainerFrame = self.menuView.frame;
            ContainerFrame.origin.y = self.menuView.frame.origin.y + 48.0;
            self.menuView.frame = ContainerFrame;}
         
                         completion:^(BOOL finished) {
                             menuViewUp = NO;
                             
                             [self.menuButton setImage:[UIImage imageNamed:@"bMenu.png"] forState:UIControlStateNormal];
                         } ];
        
    }
    else
    {
        
        
        NSString *keyPath = @"position.y";
        id finalValue = [NSNumber numberWithFloat:516 - 45.0];
        
        SKBounceAnimation *bounceAnimation = [SKBounceAnimation animationWithKeyPath:keyPath];
        bounceAnimation.fromValue = [NSNumber numberWithFloat:516];
        bounceAnimation.toValue = finalValue;
        bounceAnimation.duration = 0.6f;
        bounceAnimation.numberOfBounces = 4;
        bounceAnimation.stiffness = SKBounceAnimationStiffnessLight;
        bounceAnimation.shouldOvershoot = YES;
        
        [self.menuView.layer addAnimation:bounceAnimation forKey:@"someKey"];
        
        [self.menuView.layer setValue:finalValue forKeyPath:keyPath];
        
        menuViewUp = YES;
        [self.menuButton setImage:[UIImage imageNamed:@"bClose.png"] forState:UIControlStateNormal];
        
    }
}*/
//=======================-----------UITABLEVIEW DELEGATE METHODS---------================================//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen) {
        if (self.selectIndex.section == section) {
            return 4+1;
        }
    }
    return 1;
}
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if((indexPath.section == 0 && indexPath.row == 0) || (indexPath.section == 1 && indexPath.row == 0) || (indexPath.section == 2 && indexPath.row == 0))
        return 50;
    else
        return 40;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isOpen&&self.selectIndex.section == indexPath.section&&indexPath.row!=0) {
        static NSString *CellIdentifier = @"Cell2";
        Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        
        cell.titleLabel.text = @"Tech-Mahindra";
        return cell;
    }else
    {
        static NSString *CellIdentifier = @"Cell1";
        Cell1 *cell = (Cell1*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        if (indexPath.section == 0)
            cell.titleLabel.text = @"Job Responses";
        else
            cell.titleLabel.text = @"Jobs Applied";
        [cell changeArrowWithUp:([self.selectIndex isEqual:indexPath]?YES:NO)];
        return cell;
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if ([indexPath isEqual:self.selectIndex]) {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex = nil;
            
        }else
        {
            if (!self.selectIndex) {
                self.selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }else  // click on rows
    {
        
        
       
        
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert
{
    self.isOpen = firstDoInsert;
    
    Cell1 *cell = (Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    int section = self.selectIndex.section;
    // int contentCount = [[[_dataList objectAtIndex:section] objectForKey:@"list"] count];
	NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
	for (NSUInteger i = 1; i < 5; i++) {
		NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
		[rowToInsert addObject:indexPathToInsert];
	}
	
	if (firstDoInsert)
    {   [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
	else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
	
	
	[self.expansionTableView endUpdates];
    if (nextDoInsert) {
        self.isOpen = YES;
        self.selectIndex = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}



-(void)back
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}




- (IBAction)postResume:(id)sender {
    
  //  resume=[[ResumeViewController alloc]initWithNibName:@"ResumeViewController" bundle:nil];
  //  [self.navigationController pushViewController:resume animated:YES];
    
    //move to list view controller
    
    ListViewController *listVC = [[ListViewController alloc]initWithNibName:@"ListViewController" bundle:nil];
    [self.navigationController pushViewController:listVC animated:YES];
    
    
}


- (IBAction)submit_Tapped:(id)sender {
}


//===========---------------







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
   
    [super viewDidUnload];
}
@end

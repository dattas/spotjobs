//
//  ListViewController.m
//  MyARSample
//
//  Created by vairat on 06/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "ListViewController.h"
#import "AppDelegate.h" 
#import "ResumeViewController.h"
#import "Resume.h"
#import "PhotoSign.h"
#import <CoreData/CoreData.h>
#import "CustomIOS7AlertView.h"
@interface ListViewController (){
    
    AppDelegate *appDelegate;
    NSMutableArray *resumes_Array;
     CustomIOS7AlertView *alertView7;
     UIView *demoView;
    
}

@end

@implementation ListViewController
@synthesize myTableView;
@synthesize alertTextField;
@synthesize alertTextField7;
@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = @" My Resumes";
    [self.navigationController.navigationBar setTranslucent:NO];
    /*
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(addButtonTapped) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 45,45);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;  */
    
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"add.png"]
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(addButtonTapped)];
    
    
    self.managedObjectContext = [appDelegate getManagedObject];
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    //self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    
    
    /*    if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
     {
     // iOS7
     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:60/255.f green:60/255.f blue:60/255.f alpha:1.0f];
     self.navigationController.navigationBar.translucent = NO;
     }
     else
     {
     // older
     self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60/255.f green:60/255.f blue:60/255.f alpha:1.0f];
     }*/
   /* UIImage *myImage1 = [UIImage imageNamed:@"Back.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;*/
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"My Resumes";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    TitleLabel.textColor=[UIColor whiteColor];
    self.navigationItem.titleView = TitleLabel;
    

    
    
    
    [self refreshTableContents];
    [self.myTableView reloadData];
}

-(void)refreshTableContents{
    
    if(resumes_Array)
    {
        [resumes_Array removeAllObjects];
    }
    resumes_Array = [[NSMutableArray alloc]initWithArray:[self getResumes]];
    
}

-(void)back
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

-(void)addButtonTapped{
    
   /* UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Enter Resume Title" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
    
    alertTextField = [[UITextField alloc] initWithFrame:CGRectMake(22.0, 45.0, 240.0, 25.0)];
    alertTextField.placeholder = @"Enter Title";
    alertTextField.font = [UIFont systemFontOfSize:14];
    alertTextField.borderStyle = UITextBorderStyleRoundedRect;
    [alertTextField setBackgroundColor:[UIColor whiteColor]];
    [myAlert addSubview:alertTextField];
    [myAlert show];*/
    
      NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7)
    {
        
        [self launchDialog];
    }
    else
    {
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Enter Resume Title" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
        
        alertTextField = [[UITextField alloc] initWithFrame:CGRectMake(22.0, 45.0, 240.0, 25.0)];
        alertTextField.delegate=self;

        
        alertTextField.placeholder = @"Enter Title";
        alertTextField.font = [UIFont systemFontOfSize:14];
        alertTextField.borderStyle = UITextBorderStyleRoundedRect;
        [alertTextField setBackgroundColor:[UIColor whiteColor]];
       [myAlert addSubview:alertTextField];
        [myAlert show];
        NSLog(@"IOS 6 HERE ");
    }

    
    

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSLog(@"BUTTON INDEX IS %d  length of textfield is %d  name is   %@",buttonIndex,[alertTextField.text length],alertTextField7.text);
    if (alertView.cancelButtonIndex == buttonIndex)
    
    {}
    else
    {
        
        if([alertTextField.text length]>0)
        {
            NSLog(@"alertTextField.text length is %@",alertTextField.text);
            if([self addNewResume:alertTextField.text])
            {
                [self refreshTableContents];
                [self.myTableView reloadData];
                
            }
            else
            {
            
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Resume with this name already Exits" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [myAlert show];
            }
            
            
                
        }
        else
        {
            
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Resume should have valid Title" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [myAlert show];
            }
        
      /*  if([self addNewResume:alertTextField.text])
        {
            [self refreshTableContents];
            [self.myTableView reloadData];
            
        }
        else
        {
            
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Resume with this name already Exits" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [myAlert show];
        }*/

    }
    
}
- (IBAction)launchDialog
{
    // Here we need to pass a full frame
    alertView7 = [[CustomIOS7AlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView7 setContainerView:[self createDemoView]];
    
    // Modify the parameters
    [alertView7 setButtonTitles:[NSMutableArray arrayWithObjects:@"Cancel", @"Create", nil]];
    [alertView7 setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    /*   [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
     NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
     [alertView close];
     }];  */
    
    [alertView7 setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView7 show];
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView7 tag]);
     [alertView7 close];
    if(buttonIndex == 0)
    {
        [alertView7 close];
        
    }
    else
    {
        // [alertView close];
        if([alertTextField.text length]>0)
        {
            NSLog(@"alertTextField.text length is %@",alertTextField.text);
            if([self addNewResume:alertTextField.text])
            {
                [self refreshTableContents];
                [self.myTableView reloadData];
                
            }
            else
            {
                
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Resume with this name already Exits" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [myAlert show];
            }
            
            
            
        }
        else
        {
            
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Resume should have valid Title" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [myAlert show];
        }
        
    }
}



- (UIView *)createDemoView
{
    demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 275, 170)];
    
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    //    [imageView setImage:[UIImage imageNamed:@"demo"]];
    //    [demoView addSubview:imageView];
    
    UILabel *alertlabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 240, 70)];
    alertlabel.numberOfLines = 3;
    //alertlabel.font = [UIFont fontNamesForFamilyName:@"System Bold"];
    alertlabel.font = [UIFont boldSystemFontOfSize:17];
    alertlabel.textAlignment = NSTextAlignmentCenter;
    alertlabel.text = @"Enter Resume Title.";
    [demoView addSubview:alertlabel];
    
    alertTextField = [[UITextField alloc]initWithFrame:CGRectMake(25, 120, 230, 30)];
    alertTextField.delegate = self;
    alertTextField.borderStyle = UITextBorderStyleRoundedRect;
    alertTextField.placeholder = @"Resume Title";
    [demoView addSubview:alertTextField];
    
    return demoView;
}



#pragma mark -- UITableView Datasource & Delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [resumes_Array count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    Resume *resume = [resumes_Array objectAtIndex:indexPath.row];
    cell.textLabel.text = resume.title;
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.managedObjectContext deleteObject:[resumes_Array objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        else{
            NSLog(@"deleted sucessfully");
            [resumes_Array removeObjectAtIndex:indexPath.row];
            [self.myTableView reloadData];
        }
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Resume *resumeObj = [resumes_Array objectAtIndex:indexPath.row];
   
   ResumeViewController *resumeVC=[[ResumeViewController alloc]initWithNibName:@"ResumeViewController" bundle:nil resume:resumeObj];
    resumeVC.managedObjectContext = self.managedObjectContext;
      [self.navigationController pushViewController:resumeVC animated:YES];
    
    
    
    
}


//==================

-(BOOL) addNewResume:(NSString *)title{
    
    NSManagedObjectContext *context = self.managedObjectContext;
    
    if([self isResumeExits:title]){
        
        return NO;
        
    }
    
    
    NSManagedObject *newResume = [NSEntityDescription insertNewObjectForEntityForName:@"Resume" inManagedObjectContext:context];
    [newResume setValue:@"1" forKey:@"resumeId"];
    [newResume setValue:title forKey:@"title"];
    
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Adding Resume failed %@", [error localizedDescription]);
        return NO;
    }
    else
    {
        NSLog(@"Adding Resume success ");

    
    }
    
    
    return YES;
}


-(BOOL) isResumeExits:(NSString *)name{
    
    
    NSManagedObjectContext *context = self.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *resDesc = [NSEntityDescription entityForName:@"Resume" inManagedObjectContext:context];
    [fetchRequest setEntity:resDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title==%@",name];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0) {
        return YES;
    }
    
    return NO;
    
}

-(NSArray *)getResumes{
    
    NSManagedObjectContext *context = self.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *resumeDesc = [NSEntityDescription entityForName:@"Resume" inManagedObjectContext:context];
    [fetchRequest setEntity:resumeDesc];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];

    for (Resume *resume in fetchedObjects)
    {
        
        NSLog(@"ID::%@",resume.resumeId);
        NSLog(@"Title::%@",resume.title);
        
        Personal *personalInfo = resume.personal;
        
        NSLog(@"=================PERSONAL INFO");
        NSLog(@"FullName::%@",personalInfo.fullName);
        NSLog(@"Gender::%@",personalInfo.gender);
        NSLog(@"DOB::%@",personalInfo.dob);
        NSLog(@"Address::%@",personalInfo.address);
        NSLog(@"Phone::%@",personalInfo.phNo);
        NSLog(@"Email::%@",personalInfo.emailId);
        
        
        NSSet *eduSet = resume.education;
        
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"insertedTime" ascending:YES]];
        
        NSArray *sortedArr = [[eduSet allObjects] sortedArrayUsingDescriptors:sortDescriptors];
        
        
        
       // NSArray *arr = [eduSet allObjects];
        
        NSLog(@"=================EDUCATION");
        for (Education *edu in sortedArr) {
             NSLog(@"course::%@",edu.degreecourse);
            NSLog(@"collegeschool::%@",edu.collegeschool);
            NSLog(@"universityboard::%@",edu.universityboard);
            NSLog(@"perorcgpa::%@",edu.perorcgpa);
            NSLog(@"universityboard::%@",edu.universityboard);
            NSLog(@"Result::%@",edu.result);
            NSLog(@"yearofpassing::%@",edu.yearofpassing);
            NSLog(@"------------------------------");
        
        }
        NSLog(@"=================PROJECTS");
        NSSet *projectSet  = resume.projects;
        NSArray *projectsArray = [projectSet allObjects];
        
        for (Project *pro in projectsArray)
        {
            
            NSLog(@"projecttitle::%@",pro.projecttitle);
            NSLog(@"duration::%@",pro.duration);
            NSLog(@"role::%@",pro.role);
            NSLog(@"teamsize::%@",pro.teamsize);
            NSLog(@"expertise::%@",pro.expertise);
            NSLog(@"------------------------------");
        }
        NSLog(@"=================EXPERIENCE");
        NSSet *experienceSet  = resume.experience;
        NSArray *experienceArray = [experienceSet allObjects];
        
        for (Experience *exp in experienceArray)
        {
            
            NSLog(@"projecttitle::%@",exp.company);
            NSLog(@"duration::%@",exp.position);
            NSLog(@"role::%@",exp.period);
            NSLog(@"teamsize::%@",exp.location);
            NSLog(@"expertise::%@",exp.jobresponse);
            NSLog(@"------------------------------");
        }
        NSLog(@"=================REFERENCE");
        NSSet *referenceSet  = resume.reference;
        NSArray *referenceArray = [referenceSet allObjects];
        
        for (Reference *ref in referenceArray)
        {
            
            NSLog(@"referencename::%@",ref.referencename);
            NSLog(@"company::%@",ref.company);
            NSLog(@"phone::%@",ref.phone);
            NSLog(@"email::%@",ref.email);
            NSLog(@"------------------------------");
            
        }

        NSLog(@"=================OTHER INFO");
        Others *otherInfo = resume.other;
        
        NSLog(@"drivingLicence::%@",otherInfo.drivingLicence);
        NSLog(@"panNo::%@",otherInfo.panNo);
        NSLog(@"passportNo::%@",otherInfo.passportNo);

        PhotoSign *photosign = resume.photosign;
        
        NSLog(@"place::%@",photosign.place);
        NSLog(@"objective::%@",photosign.objective);
        
        
        NSLog(@"=============================================");
        
    }

    
    
    
    
    
    return fetchedObjects;
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSLog(@"willShowViewController");
    [self viewWillAppear:animated];
}




//==========================+++++++++++















- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

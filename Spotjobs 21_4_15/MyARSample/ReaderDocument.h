//
//  ReaderDocument.h
//  MyARSample
//
//  Created by vairat on 05/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReaderDocument : NSObject<NSCoding>
{
@private // Instance variables
    
	NSString *_guid;
    
	NSDate *_fileDate;
    
	NSDate *_lastOpen;
    
	NSNumber *_fileSize;
    
	NSNumber *_pageCount;
    
	NSNumber *_pageNumber;
    
	NSMutableIndexSet *_bookmarks;
    
	NSString *_fileName;
    
	NSString *_password;
    
	NSURL *_fileURL;
}

@property (nonatomic, readonly) NSString *guid;
@property (nonatomic, readonly) NSDate *fileDate;
@property (nonatomic, readwrite) NSDate *lastOpen;
@property (nonatomic, readonly) NSNumber *fileSize;
@property (nonatomic, readonly) NSNumber *pageCount;
@property (nonatomic, readwrite) NSNumber *pageNumber;
@property (nonatomic, readonly) NSMutableIndexSet *bookmarks;
@property (nonatomic, readonly) NSString *fileName;
@property (nonatomic, readonly) NSString *password;
@property (nonatomic, readonly) NSURL *fileURL;

+ (ReaderDocument *)withDocumentFilePath:(NSString *)filename password:(NSString *)phrase;

+ (ReaderDocument *)unarchiveFromFileName:(NSString *)filename password:(NSString *)phrase;

- (id)initWithFilePath:(NSString *)fullFilePath password:(NSString *)phrase;

- (void)saveReaderDocument;

- (void)updateProperties;


@end

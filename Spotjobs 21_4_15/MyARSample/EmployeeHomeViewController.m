//
//  EmployeeHomeViewController.m
//  MyARSample
//
//  Created by vairat on 26/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "EmployeeHomeViewController.h"
#import "PostAJobViewController.h"
#import "AppDelegate.h"
#import "Cell1.h"
#import "Cell2.h"

@interface EmployeeHomeViewController (){
    
    NSURLConnection *getJobsRequest;
    JobDetailsParser *jobDetailXMLParser;
    
    NSMutableArray *jobsList_Array;
    NSMutableArray *futureJobs_Array;
    NSMutableArray *runningJobs_Array;
    NSMutableArray *expiredJobs_Array;
    AppDelegate *appDelegate;
    
}
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;
@end

@implementation EmployeeHomeViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Home";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};

    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    appDelegate =  (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"logout.png"]
                                                                          style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(logoutTapped)];
    
    UIViewController *parentViewController = appDelegate.tabBarController.parentViewController;
    RevealController *baseParentViewController = (RevealController *)parentViewController.parentViewController;
        
    
    if ([baseParentViewController respondsToSelector:@selector(revealGesture:)] && [baseParentViewController respondsToSelector:@selector(revealToggle:)])
	{
        		UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:baseParentViewController action:@selector(revealGesture:)];
        		[self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:baseParentViewController
                                                                             action:@selector(revealToggle:)];
        
        
    }

    
    futureJobs_Array = [[NSMutableArray alloc]init];
    runningJobs_Array = [[NSMutableArray alloc]init];
    expiredJobs_Array = [[NSMutableArray alloc]init];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
   // [self.navigationController setNavigationBarHidden:YES animated:NO];
    NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/get_jobs.php?id=%@",userID]];
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    
    
    NSLog(@"Get Jobs URL==>%@",aUrl);
    
    getJobsRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
}


-(void)logoutTapped{
    
    
    NSLog(@"logoutTapped");
    
    UIAlertView *logoutAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to Logout" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    [logoutAlert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
   
        if (buttonIndex == alertView.cancelButtonIndex) {
            
            [appDelegate logoutSession];
        }
    
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSLog(@"didReceiveResponse");
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
    
    NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    NSLog(@"GetJobs Request::%@",responseString);
    
    NSXMLParser *jobParser = [[NSXMLParser alloc] initWithData:_responseData];
    jobDetailXMLParser = [[JobDetailsParser alloc] init];
    jobDetailXMLParser.delegate = self;
    jobParser.delegate = jobDetailXMLParser;
    [jobParser parse];
}


//=======================-----------UITABLEVIEW DELEGATE METHODS---------================================//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen) {
        if (self.selectIndex.section == section) {
            if (self.selectIndex.section == 0) {
                return [runningJobs_Array count]+1;
            }
            else if (self.selectIndex.section == 1) {
                return [futureJobs_Array count]+1;
            }
            else
                return [expiredJobs_Array count]+1;
        }
        }
    
    return 1;
}
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if((indexPath.section == 0 && indexPath.row == 0) || (indexPath.section == 1 && indexPath.row == 0) || (indexPath.section == 2 && indexPath.row == 0))
        return 50;
    else
        return 40;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isOpen&&self.selectIndex.section == indexPath.section&&indexPath.row!=0) {
        static NSString *CellIdentifier = @"Cell2";
        Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        
                
        JobDetails *job;
        if(indexPath.section == 0 ){
            if([runningJobs_Array count]>0)
            job = [runningJobs_Array objectAtIndex:indexPath.row-1];
        }
        else if (indexPath.section == 1 ){
            if([futureJobs_Array count]>0)
                job = [futureJobs_Array objectAtIndex:indexPath.row-1];
        }
        else if (indexPath.section == 2 ){
            if([expiredJobs_Array count]>0)
                job =  [expiredJobs_Array objectAtIndex:indexPath.row-1];
        }
        
        cell.titleLabel.text = job.jobRole;
        return cell;
    }else
    {
        static NSString *CellIdentifier = @"Cell1";
        Cell1 *cell = (Cell1*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        if (indexPath.section == 0)
            cell.titleLabel.text = @"Current Jobs";
        else if (indexPath.section == 1)
            cell.titleLabel.text = @"Expired Jobs";
        else
            cell.titleLabel.text = @"Future Jobs";
        [cell changeArrowWithUp:([self.selectIndex isEqual:indexPath]?YES:NO)];
        return cell;
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if ([indexPath isEqual:self.selectIndex]) {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex = nil;
            
        }else
        {
            if (!self.selectIndex) {
                self.selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }else  // click on rows
    {
        
        JobDetails *job;
        if (indexPath.section == 0)
            job  = [runningJobs_Array objectAtIndex:indexPath.row-1];
        else if (indexPath.section == 1)
            job  = [futureJobs_Array objectAtIndex:indexPath.row-1];
        else
            job  = [expiredJobs_Array objectAtIndex:indexPath.row-1];
        
       
        
        PostAJobViewController *postjob = [[PostAJobViewController alloc] initWithNibName:@"PostAJobViewController" bundle:nil currentJob:job];
        
        [self.navigationController pushViewController:postjob animated:YES];
        
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert
{
    self.isOpen = firstDoInsert;
    
    Cell1 *cell = (Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    int section = self.selectIndex.section;
    int limit;
    if (section == 0)
        limit = [runningJobs_Array count];
    else  if (section == 1)
        limit = [futureJobs_Array count];
    else
        limit = [expiredJobs_Array count];
	NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
	for (NSUInteger i = 1; i < limit+1; i++) {
		NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
		[rowToInsert addObject:indexPathToInsert];
	}
	
	if (firstDoInsert)
    {   [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
	else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
	
	
	[self.expansionTableView endUpdates];
    if (nextDoInsert) {
        self.isOpen = YES;
        self.selectIndex = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}

//=====================================

- (void) parsingJobDetailsFinished:(NSArray *) jobDetails_Array{
    
    
    if(!jobsList_Array){
        jobsList_Array = [[NSMutableArray alloc]init];
    }
    [jobsList_Array removeAllObjects];
    [jobsList_Array addObjectsFromArray:jobDetails_Array];
    [runningJobs_Array removeAllObjects];
    [expiredJobs_Array removeAllObjects];
    [futureJobs_Array removeAllObjects];
    
    
    //to seperete future,current and past deals basing on date
    NSDate *currentDate = [NSDate date];
    NSComparisonResult result;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
    // NSLog(@"currentDateStr%@",currentDateStr);
    //  NSLog(@"=============================");
    currentDate = [dateFormatter dateFromString:currentDateStr];
    
    for (int i = 0; i< [jobsList_Array count]; i++) {
        
        JobDetails *job = [jobsList_Array objectAtIndex:i];
        result = [[dateFormatter dateFromString:job.jobStartDate] compare:currentDate];
        
        
        switch (result)
        {
            case NSOrderedAscending:  //to search end date
                //NSLog(@"NSOrderedAscending");
                result = [[dateFormatter dateFromString:job.jobEndDate] compare:currentDate];
                switch (result)
            {
                case NSOrderedAscending:
                    //  NSLog(@"Expired Deal Name: %@",pro.productName);
                    [expiredJobs_Array addObject:[jobsList_Array objectAtIndex:i]];
                    break;
                case NSOrderedDescending:
                    // NSLog(@"current Deal Name: %@",pro.productName);
                    [runningJobs_Array addObject:[jobsList_Array objectAtIndex:i]];
                    break;
                    
                default:
                    break;
            }
                
                
                break;
                
            case NSOrderedDescending:// NSLog(@"Future Deal Name %@",pro.productName);
                [futureJobs_Array addObject:[jobsList_Array objectAtIndex:i]];
                break;
                
            case NSOrderedSame:      // NSLog(@"current Deal Name: %@",pro.productName);
                [runningJobs_Array addObject:[jobsList_Array objectAtIndex:i]];
                break;
                
            default:  break;
        }
    }
    [self.expansionTableView reloadData];
    NSLog(@"runningJobs_Array::%d",[runningJobs_Array count]);
    NSLog(@"futureJobs_Array::%d",[futureJobs_Array count]);
    NSLog(@"expiredJobs_Array::%d",[expiredJobs_Array count]);
}
- (void) jobDetailsXMLparsingFailed{
    
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)buttonAction:(id)sender {
//    
//    PostAJobViewController *postjobViewController =[[PostAJobViewController alloc]initWithNibName:@"PostAJobViewController" bundle:nil];
//    [self.navigationController pushViewController:postjobViewController animated:NO];
//}
@end

//
//  Experience.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Resume;

@interface Experience : NSManagedObject

@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * jobresponse;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * period;
@property (nonatomic, retain) NSString * position;
@property (nonatomic, retain) NSDate * insertedTime;
@property (nonatomic, retain) Resume *ownby;

@end

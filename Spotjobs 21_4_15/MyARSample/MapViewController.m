//
//  MapViewController.m
//  MyARSample
//
//  Created by vairat on 04/12/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "MapViewController.h"
#import "NSString+SBJSON.h"
#import "myAnnotation.h"
#define METERS_PER_MILE 1609.344

@interface MapViewController (){
    
    NSString *locationAddress;
}

@end

@implementation MapViewController

@synthesize delegate;
@synthesize mapView;
@synthesize geocoder;
@synthesize currentCoordinates;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil location:(NSString *)locaddr
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        locationAddress = locaddr;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    self.title = @"Map";
    [mapView setDelegate:self];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"bClose.png"]
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(doneTapped)];
    
    if([locationAddress length] >0){
    
    NSString *esc_addr =  [locationAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"=======>>%@",esc_addr);
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    
    NSDictionary *googleResponse = [[NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL] JSONValue];
    
    NSDictionary    *resultsDict = [googleResponse valueForKey:  @"results"];   // get the results dictionary
    NSDictionary   *geometryDict = [resultsDict valueForKey: @"geometry"];   // geometry dictionary within the  results dictionary
    NSDictionary   *locationDict = [geometryDict valueForKey: @"location"];   // location dictionary within the geometry dictionary
    
    NSArray *latArray = [locationDict valueForKey: @"lat"];
    NSString *latString = [latArray lastObject];     // (one element) array entries provided by the json parser
    
    NSArray *lngArray = [locationDict valueForKey: @"lng"];
    NSString *lngString = [lngArray lastObject];     // (one element) array entries provided by the json parser
    
   
    currentCoordinates.latitude = [latString doubleValue];
    currentCoordinates.longitude = [lngString doubleValue];
    
    
    
    NSLog(@"latArray::%d",[latArray count]);
    NSLog(@"lonArray::%d",[lngArray count]);
    
    NSLog(@"location.latitude::%f",currentCoordinates.latitude);
    NSLog(@"location.longitude::%f",currentCoordinates.longitude);
    }
    
    
    myAnnotation *annotation = [[myAnnotation alloc] initWithCoordinate:currentCoordinates title:@""];
    [self.mapView addAnnotation:annotation];
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = currentCoordinates.latitude;
    zoomLocation.longitude= currentCoordinates.longitude;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.3*METERS_PER_MILE, 0.3*METERS_PER_MILE);
    [self.mapView setRegion:viewRegion animated:YES];
    
}

-(void)doneTapped{
    
    
    if([self.delegate respondsToSelector:@selector(mapViewControllerDismissed:)])
    {
        [self.delegate mapViewControllerDismissed:currentCoordinates];
    }
    [self dismissModalViewControllerAnimated:YES];
    
}

//=================== MKMAPVIEW DELEGATE METHODS ====================//

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    
    static NSString *identifier = @"myAnnotation";
    MKPinAnnotationView * annotationView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (!annotationView)
    {
        
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.pinColor = MKPinAnnotationColorPurple;
        annotationView.animatesDrop = YES;
        annotationView.canShowCallout = NO;
        annotationView.draggable = YES;
    }else {
        annotationView.annotation = annotation;
    }
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    
    // code to move the map
    NSLog(@"didChangeDragState");
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        NSLog(@"Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        
        //NSLog(@"%@",[NSString stringWithFormat:@"Lattitude = %f longitude = %f",droppedAt.latitude,droppedAt.longitude]);
        
        currentCoordinates = droppedAt;
        
        MKMapRect mapRect = [self.mapView visibleMapRect];
        MKMapPoint point = MKMapPointForCoordinate(annotationView.annotation.coordinate);
        mapRect.origin.x = point.x - mapRect.size.width * 0.3;
        mapRect.origin.y = point.y - mapRect.size.height * 0.70;
        
        [self.mapView setVisibleMapRect:mapRect animated:YES];
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

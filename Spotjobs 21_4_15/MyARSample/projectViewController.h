//
//  projectViewController.h
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#import "UIKeyboardViewController.h"
#import <UIKit/UIKit.h>
#import "Resume.h"
#import "Project.h"

@interface projectViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate, UIKeyboardViewControllerDelegate>
{

    NSMutableArray *projectdetails;
    UIKeyboardViewController *keyBoardController;
}
@property (nonatomic, retain)Project *currentProjectObj;

@property (strong, nonatomic) NSMutableArray *projectdetails;
@property (strong, nonatomic) IBOutlet UIScrollView *projectScrollview;
@property (strong, nonatomic) IBOutlet UITextField *projecttitleTextField;
@property (strong, nonatomic) IBOutlet UITextField *durationtimeTextField;
@property (strong, nonatomic) IBOutlet UITextField *roleTextField;
@property (strong, nonatomic) IBOutlet UITextField *teamsizeTextField;
@property (strong, nonatomic) IBOutlet UITextField *expertiseTextField;
@property (strong, nonatomic) IBOutlet UITextField *currentTextField;
@property (strong, nonatomic) IBOutlet UIView *accessoryView;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UIButton *durationButton;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
- (IBAction)durationTime:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj;
-(IBAction)doneButton_Pressed:(id)sender;
-(void)projectDurationPicker;
@end

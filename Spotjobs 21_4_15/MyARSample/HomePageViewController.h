//
//  HomePageViewController.h
//  MyARSample
//
//  Created by vairat on 11/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePageViewController : UIViewController{
    
}
@property (unsafe_unretained, nonatomic) IBOutlet UIPageControl *myPage_Control;
- (IBAction)MenuButtonTapped:(id)sender;
- (UIViewController *) controllerAtIndex:(NSInteger) index ;
@end

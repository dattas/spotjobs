//
//  SearchCriteriaViewController.m
//  MyARSample
//
//  Created by vairat on 22/08/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "SearchCriteriaViewController.h"
#import "HomeViewController.h"

#import "HomePageViewController.h"
//#import "MyAccountViewController.h"
#import "HelpViewController.h"
#import "AppDelegate.h"
#import "SearchCell.h"
#import "Category.h"

@interface SearchCriteriaViewController (){
    
    NSArray *resultSet_Array;
    NSArray *subUrbs_Array;
   // NSMutableArray *categoryArray;
    
    int selectedCategory;
    
     JobDetailsParser *jobDetailXMLParser;
    CategoryXMLParser *categoryParserDelegate;
    AppDelegate *appDelegate;
    
    UIActionSheet *searchCriteriaActionSheet;
    UIView *pickerContainerView;
    UIPickerView  *searchCriteriaPicker;
    NSArray *searchCriteriaItems;
    
    NSURLConnection *searchCriteria;
    NSURLConnection *dealsFetchRequest;
    
    
    
}
@property(nonatomic,retain)NSArray *searchCriteriaItems;
-(void)presentSearchCriteriaPicker;
- (void) fetchCategoryList;
- (void) searchDealsOnKeywordBasis;

@end

@implementation SearchCriteriaViewController

@synthesize suburb_TextField;
@synthesize searchCriteria_TextField;
@synthesize keyword_TextField;
@synthesize searchCriteriaItems;
@synthesize current_TextField;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
           // categoryArray = [[NSMutableArray alloc] init];
            selectedCategory = -1;
            }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"searchViewController");
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // self.searchCriteriaItems = [[NSArray alloc]initWithObjects:@"Automotive",@"Dining & FastFood",@"Golf Courses",@"Healthy & Beauty",@"Home & lifestyle",@"Leisure & Entertainment",@"Shopping & Vochers",@"Travel & Accommodation", nil];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self fetchCategoryList];
    
}

-(void)fetchCategoryList{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_cat.php?cid=1406&country=Australia",URL_Prefix]];
    NSLog(@"URl is %@ ",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    searchCriteria = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}
//================================---------------UIPICKERVIEW METHODS----------------====================================//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
        return [appDelegate.categoryList_Array count];

}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
//    
//           Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
//           return catObj.catName;
//    
//}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
        
        selectedCategory = row;
        Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
        self.searchCriteria_TextField.text = catObj.catName;
    
    
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 45)];
    
    [label setTextAlignment:UITextAlignmentCenter];
    label.opaque=NO;
    label.backgroundColor=[UIColor clearColor];
    label.textColor = [UIColor blackColor];
    UIFont* textFont = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    
    label.font = textFont;
    Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
    label.text = catObj.catName;
    
    NSLog(@"label text is %@ ",catObj.catName);
    return label;
    
}


//======================------------------TEXTFIELD DELEGATE METHODS-------------------===========================//

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    current_TextField = textField;
    if(textField.tag == 1){
        [textField resignFirstResponder];
        [self presentSearchCriteriaPicker];
         textField.inputView = pickerContainerView;
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
        [textField resignFirstResponder];
        return YES;
}

//======================-------------------CUSTOM METHODS------------------===========================//

-(void)presentSearchCriteriaPicker{
    
    searchCriteriaActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self     cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    searchCriteriaActionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;//UIActionSheetStyleBlackOpaque;
    
    
    searchCriteriaPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
    searchCriteriaPicker.delegate = self;
    searchCriteriaPicker.showsSelectionIndicator = YES;
   // [searchCriteriaPicker selectRow:0 inComponent:0 animated:NO];
    [searchCriteriaActionSheet addSubview:searchCriteriaPicker];
    
    
    
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,320,40)];
    tools.barStyle = UIBarStyleBlackTranslucent;//UIBarStyleBlackOpaque;
    [searchCriteriaActionSheet addSubview:tools];
   
    
    
    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(doneButtonClicked)];
    doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
    
    UIBarButtonItem *cancelButton=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClicked)];
    cancelButton.imageInsets=UIEdgeInsetsMake(0, 6, 50, 25);
    
    
    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *array = [[NSArray alloc]initWithObjects:cancelButton,flexSpace,flexSpace,doneButton,nil];
    [tools setItems:array];
        
    
    //picker title
    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(60,8, 200, 25)];
    lblPickerTitle.text=@"Select Category";
    lblPickerTitle.backgroundColor=[UIColor clearColor];
    lblPickerTitle.textColor=[UIColor whiteColor];
    lblPickerTitle.textAlignment=UITextAlignmentCenter;
    lblPickerTitle.font=[UIFont boldSystemFontOfSize:15];
    [tools addSubview:lblPickerTitle];
    
    
    [searchCriteriaActionSheet showFromRect:CGRectMake(0,480, 320,215) inView:self.view animated:YES];
    [searchCriteriaActionSheet setBounds:CGRectMake(0,0, 320, 500)];
   
    
}
-(void)doneButtonClicked{
       if(selectedCategory == -1)
           self.searchCriteria_TextField.text = @"Automotive";
       [searchCriteriaActionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)cancelButtonClicked{
    [searchCriteriaActionSheet dismissWithClickedButtonIndex:0 animated:YES];
}
- (IBAction)resetButtonTapped:(id)sender{
    selectedCategory = -1;
    self.searchCriteria_TextField.text = @"";
    self.keyword_TextField.text = @"";
    self.suburb_TextField.text = @"";
}
- (IBAction)homeButtonTapped:(id)sender{
    
    RevealController *revealController = [self.parentViewController isKindOfClass:[RevealController class]] ? (RevealController *)self.parentViewController : nil;
    
        HomePageViewController *home = [[HomePageViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:home];
        navigationController.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
        [revealController setFrontViewController:navigationController animated:NO];
    
}

- (IBAction)findMeDeal_Action:(id)sender {
   
    
    
 if ((selectedCategory == -1 && suburb_TextField.text.length == 0) && keyword_TextField.text.length == 0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a search criteria" message:@"Please select a category or enter a keyword or a location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
        return;
    }
    if (selectedCategory == -1 && keyword_TextField.text.length != 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a search criteria" message:@"Please select a category " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
        
    }
    [current_TextField resignFirstResponder];
    [self searchDealsOnKeywordBasis];
    
    
}

-(void)searchDealsOnKeywordBasis{
    
    NSLog(@"searchDealsOnKeywordBasis");
    Category *cat;
    NSString *categoryID = @"-1";
    if(selectedCategory != -1)
    {
        cat = [appDelegate.categoryList_Array objectAtIndex:selectedCategory];
        categoryID = cat.catId;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@search_with_map.php?",URL_Prefix];
    
    
    if(selectedCategory != -1)
    {
        urlString = [NSString stringWithFormat:@"%@cat_id=%@",urlString,categoryID];
        
    }
    if([self.suburb_TextField.text length]>0)
    {
        
        if(selectedCategory != -1)
        {
            
            urlString = [NSString stringWithFormat:@"%@&",urlString];
            
        }
        urlString = [NSString stringWithFormat:@"%@p=%@",urlString,self.suburb_TextField.text];
    }
    if([keyword_TextField.text length]>0)
    {
        
        if(selectedCategory != -1 || ([self.suburb_TextField.text length]!= 0))
        {
            
            urlString = [NSString stringWithFormat:@"%@&",urlString];
            
        }
        urlString = [NSString stringWithFormat:@"%@q=%@",urlString,keyword_TextField.text];
    }
    /*
    urlString = [NSString stringWithFormat:@"%@&cid=24&country=Australia&start=0&limit=30",urlString];
    
    NSLog(@"Search URL::%@",urlString);
    NSString *searchURL = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:searchURL]];
    dealsFetchRequest = [[NSURLConnection alloc] initWithRequest:request delegate:self];*/
    
    NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/get_jobs.php?id=%@",userID]];
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    
    
    NSLog(@"Get Jobs URL==>%@",aUrl);
    
    dealsFetchRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
    
}
//=======================-----------NSURLCONNECTION METHODS---------================================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
   
    [_responseData appendData:data];
    
    if(connection == searchCriteria)
    {
        NSLog(@"searchCriteria Request...");
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"RESPONSE::::%@",responseString);
        
        NSXMLParser *categoryParser = [[NSXMLParser alloc] initWithData:_responseData];
        categoryParserDelegate = [[CategoryXMLParser alloc] init];
        categoryParserDelegate.delegate = self;
        categoryParser.delegate = categoryParserDelegate;
        [categoryParser parse];
        
    }
    else if(connection == dealsFetchRequest)
    {
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"JOBS RESPONSE::::%@",responseString);
        
        NSXMLParser *jobParser = [[NSXMLParser alloc] initWithData:_responseData];
        jobDetailXMLParser = [[JobDetailsParser alloc] init];
        jobDetailXMLParser.delegate = self;
        jobParser.delegate = jobDetailXMLParser;
        [jobParser parse];

    }
}


//=====================------------------ PARSING METHODS --------------=============================//

#pragma mark -- Category Parser Delegate methods

- (void)parsingCategoriesFinished:(NSArray *)categoryList
{
    NSLog(@"catogerlist is %@ ",categoryList);
    for (Category *cat in categoryList)
        [appDelegate.categoryList_Array addObject:cat];

}

- (void)categoryXMLparsingFailed
{
    
}

- (void) parsingJobDetailsFinished:(NSArray *) jobDetails_Array{
    
    
    NSLog(@"parsingJobDetailsFinished");
    
    RevealController *revealController = [self.parentViewController isKindOfClass:[RevealController class]] ? (RevealController *)self.parentViewController : nil;
    
     HomeViewController *homeViewController = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil jobsList:jobDetails_Array];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    navigationController.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    [revealController setFrontViewController:navigationController animated:NO];
    
   
    [self.navigationController pushViewController:homeViewController animated:YES];
}

- (void) jobDetailsXMLparsingFailed{
    NSLog(@"jobDetailsXMLparsingFailed");
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  educationViewController.m
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#define  kOFFSET_FOR_KEYBOARD 135
#import "educationViewController.h"
#import "MediatorViewController.h"
#import "AppDelegate.h"

@interface educationViewController ()
{

    AppDelegate *appDelegate;
    Resume *currRes;
    
    NSMutableDictionary *edu_Dictionary;
    UIActionSheet *myActionSheet;
    NSArray *indian_StatesList;
    NSArray * Countries_List ;
    NSString *country;
    NSString *state;
    NSMutableArray *months_Array;
    NSMutableArray *years_Array;
}


@end

@implementation educationViewController

@synthesize educationdetails;

@synthesize educationScrollView;
@synthesize degreecourseTextField;
@synthesize universityboardTextField;
@synthesize collegeschoolTextField;
@synthesize yearofpassingTextField;
@synthesize accessoryView;
@synthesize currentTextField;
@synthesize cgpaButtonTapped;
@synthesize percentageButton;
@synthesize resultLabel;

@synthesize currentEducationObj;
@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        currRes = resumeObj;
    
    }
    return self;
}

- (void)viewDidLoad
{
    
    months_Array = [[NSMutableArray alloc]initWithCapacity:12];
    [months_Array addObjectsFromArray:[[NSArray alloc]initWithObjects:@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec",nil]];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    years_Array = [[NSMutableArray alloc]initWithCapacity:200];
    for(int i = 1900;i<=2100;i++)
        [years_Array addObject:[NSString stringWithFormat:@"%d",i]];

    self.navigationItem.title=@"Education";
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    educationdetails=[[NSMutableArray alloc]init];
    
    
    self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
           
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back.png"]
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(backAction)];
    
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Save.png"]
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(saveData)];
    
    
    if(currentEducationObj)
        [self loadEducationdetails];
    country = @"";
    state = @"";

}
-(void) projectDurationPicker
{
    
    // [self durationTime:];
    myActionSheet = [[UIActionSheet alloc] init];
    myActionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    
    
    UIPickerView *myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
    myPickerView.dataSource = self;
    myPickerView.delegate   = self;
    myPickerView.showsSelectionIndicator = YES;
    [myActionSheet addSubview:myPickerView];
    
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,320,40)];
    tools.barStyle=UIBarStyleBlackOpaque;
    [myActionSheet addSubview:tools];
    
    
    
    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btnActinDoneClicked:)];
    doneButton.tag=1;
    doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
    
    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSArray *array = [[NSArray alloc]initWithObjects:flexSpace,flexSpace,doneButton,nil];
    [tools setItems:array];
    
    
    
    //picker title
    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(60,8, 200, 25)];
    lblPickerTitle.text=@"Select Project Duration";
    lblPickerTitle.backgroundColor=[UIColor clearColor];
    lblPickerTitle.textColor=[UIColor whiteColor];
    lblPickerTitle.textAlignment=UITextAlignmentCenter;
    lblPickerTitle.font=[UIFont boldSystemFontOfSize:15];
    [tools addSubview:lblPickerTitle];
    // [sheet showInView:[UIApplication sharedApplication].keyWindow];
    [myActionSheet showInView:[self.view superview]];
   // [myActionSheet showFromRect:CGRectMake(0,480, 320,215) inView:self.view animated:YES];
    [myActionSheet setBounds:CGRectMake(0,0, 320, 500)];
    
}

- (void) btnActinDoneClicked:(id)sender
{
    
    [myActionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
    
    
}

//========= UIPICKERVIEW DELEGATE METHODS ==============//
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if(component == 0)
    {
        
        country =[[NSString alloc] initWithFormat:@"%@" , [months_Array objectAtIndex:row]];
    }
    if (component == 1)
    {
        state =[[NSString alloc] initWithFormat:@"%@" , [years_Array objectAtIndex:row]];
        state = [years_Array objectAtIndex:row];
    }
    [self.YearofpassingButton setTitle:[[NSString alloc]initWithFormat:@"%@ %@",country,state] forState:UIControlStateNormal];
    self.yearofpassingTextField.text=[[NSString alloc]initWithFormat:@"%@ %@",country,state];
}
-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0 || component == 2)
        return  [months_Array objectAtIndex:row];
    
    else
        return  [years_Array objectAtIndex:row];
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    
    if (component == 0 || component == 2)
        return  [months_Array count];
    
    else
        return  [years_Array count];
}


-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(void)viewWillAppear:(BOOL)animated
 {
     keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
     [keyBoardController addToolbarToKeyboard];

    
    
    
    if ([appDelegate isIphone5]) {
        educationScrollView.frame=CGRectMake(0,   0, 320, 500);
        [educationScrollView setContentSize:CGSizeMake(320, 678)];
        
    }
    else
    {
        educationScrollView.frame=CGRectMake(0,  0, 320, 460);
        [educationScrollView setContentSize:CGSizeMake(320, 678)];
    }
    
    
 }

-(void)backAction
 {
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
 }

-(void)saveData
{
    
    BOOL success = [self addEducationinfo];
    
    if(success)
    {
        edu_Dictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SortDic"] mutableCopy];
        if(!edu_Dictionary){
            edu_Dictionary = [[NSMutableDictionary alloc]init];
        }
        NSMutableArray *arr = [[edu_Dictionary objectForKey:@"Education"]mutableCopy];
        [arr addObject:self.degreecourseTextField.text];
        [edu_Dictionary setValue:arr forKey:@"Education"];
        
        [[NSUserDefaults standardUserDefaults] setValue:edu_Dictionary forKey:@"SortDic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Saved" message:@"data saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"data not  saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    MediatorViewController *mVc = [[MediatorViewController alloc]initWithNibName:@"MediatorViewController" bundle:Nil resume:currRes listType:@"education"];
    mVc.managedObjectContext = self.managedObjectContext;
    [self.navigationController pushViewController:mVc animated:NO];
    
    
}

-(void)loadEducationdetails
{
    
    /*
    NSSet *eduSet  = currRes.education;
    NSArray *eduArray = [eduSet allObjects];
    
    for (Education *edu in eduArray) {
        
        NSLog(@"collegeschool::%@",edu.collegeschool);
        NSLog(@"degreecourse::%@",edu.degreecourse);
        NSLog(@"perorcgpa::%@",edu.perorcgpa);
        NSLog(@"universityboard::%@",edu.universityboard);
        NSLog(@"yearofpassing::%@",edu.yearofpassing);
        
    }  */
    
    self.degreecourseTextField.text      = currentEducationObj.degreecourse ;
    self.universityboardTextField.text   = currentEducationObj.universityboard;
    self.collegeschoolTextField.text     = currentEducationObj.collegeschool ;
    self.resultTextField.text            = currentEducationObj.result;
    self.yearofpassingTextField.text     = currentEducationObj.yearofpassing;
     [self.YearofpassingButton setTitle:currentEducationObj.yearofpassing forState:UIControlStateNormal];

    if ([currentEducationObj.perorcgpa isEqualToString:@"cgpa"])
    
        [self.cgpaButtonTapped setImage:[UIImage imageNamed:@"circle_hl.png"] forState:UIControlStateNormal];
    
    else
    
        [self.percentageButton setImage:[UIImage imageNamed:@"circle_hl.png"] forState:UIControlStateNormal];
    

}
//++++++++=====================================
-(BOOL) addEducationinfo
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if(currentEducationObj)
    {
        NSLog(@"updating");
        currentEducationObj.degreecourse        =self.degreecourseTextField.text;
        currentEducationObj.universityboard     =self.universityboardTextField.text;
        currentEducationObj.collegeschool       =self.collegeschoolTextField.text;
        currentEducationObj.result              =self.resultTextField.text;
        currentEducationObj.yearofpassing       =yearofpassingTextField.text;
        currentEducationObj.perorcgpa           =resultLabel.text;
        currentEducationObj.ownby               =currRes;
        
    }
    
    
    else
    {
        NSLog(@"Adding New Object");
        Education *education = (Education *)[NSEntityDescription insertNewObjectForEntityForName:@"Education" inManagedObjectContext:context];
        
        education.degreecourse        =self.degreecourseTextField.text;
        education.universityboard     =self.universityboardTextField.text;
        education.collegeschool       =self.collegeschoolTextField.text;
        education.result              =self.resultTextField.text;
        education.yearofpassing       =yearofpassingTextField.text;
        education.perorcgpa           =resultLabel.text;
        education.insertedTime        =[NSDate date];
        education.ownby               =currRes;
    }
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding Education data failed %@", [error localizedDescription]);
        return NO;
    }
    return YES;
}



//=======METHOD TO ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN =========//

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        NSLog(@"INSIDE MOVEDUP AND SCROLL VIEW Y IS::%f",rect.origin.y);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD+20;
        
        
        
    }
    else
    {
        
        rect.origin.y += kOFFSET_FOR_KEYBOARD+20;
        
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    
    [textField setInputAccessoryView:accessoryView];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
   
            
}
-(IBAction)doneButton_Pressed:(id)sender
{
    
    [currentTextField resignFirstResponder];
}

//================================================================================================//
//=======================METHOD TO DONE BUTTON ====================//

- (IBAction)yop:(id)sender
{
    
    [self projectDurationPicker];
}

- (IBAction)percentageorcdpaTapped:(id)sender {
    
    switch ([sender tag]) {
        case 0:
            
            NSLog(@"Percentage");
            [self.cgpaButtonTapped setImage:[UIImage imageNamed:@"circle.png"]  forState:UIControlStateNormal];
            [self.percentageButton setImage:[UIImage imageNamed:@"circle_hl.png"] forState:UIControlStateNormal];
            
            resultLabel.text=@"Percentage";
            break;
        case 1:
            NSLog(@"cgpa");
            
            [self.cgpaButtonTapped setImage:[UIImage imageNamed:@"circle_hl.png"] forState:UIControlStateNormal];
            
            [self.percentageButton setImage:[UIImage imageNamed:@"circle.png"]  forState:UIControlStateNormal];
            resultLabel.text=@"cgpa";

            break;
            
        default:
            break;
    }

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setYearofpassingButton:nil];
    [super viewDidUnload];
}
@end

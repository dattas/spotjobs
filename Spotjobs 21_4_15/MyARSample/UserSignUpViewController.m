//
//  UserSignUpViewController.m
//  MyARSample
//
//  Created by vairat on 19/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "UserSignUpViewController.h"
#import "ListViewController.h"
#import "FillComInfoViewController.h"
#import "UserViewController.h"
#import "UserHelpViewController.h"
#import "AppDelegate.h"

#define Status_XML_tag @"status"
#define Status_success_message @"SUCCESS"
#define Status_failure_message @"FAILURE"

@interface UserSignUpViewController (){
    
    NSString *userType;
    NSURLConnection *registerRequest;
    NSURLConnection *loginRequest;
    NSURLConnection *checkRequest;
    NSXMLParser *parser;
    BOOL checked;
    
    NSString *currentUser;
    UserDataXMLParser *parserDelegate;
    AppDelegate *appDelegate;
}

@end

@implementation UserSignUpViewController
@synthesize userName_Field;
@synthesize password_TextField;
@synthesize conformPwd_TextField;
@synthesize warningLabel;
@synthesize checkAvailability_ImageView;
@synthesize checkAvailability_ActivityIndicator;
@synthesize signInNavigationBar;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil userType:(NSString *)user
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        userType = user;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [signInNavigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTranslucent:NO];
    checked = NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([userType isEqualToString:@"jobSeeker"]){
        self.title = @"User SignUp";
        currentUser = @"member";
    }
    else{
        self.title = @"Employee SignUp";
        currentUser = @"employer";
    }
    
    
}


- (IBAction)cancelAction{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                    completion:NULL];
    
}

- (void)availability_Action {
    
    NSString *uName = self.userName_Field.text;
    
    
    NSLog(@"%@",uName);
    
    
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/check.php?username=%@",uName]];
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    
    NSLog(@"=======>>%@",aUrl);
    
    checkRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
}



- (IBAction)registerButton_Action:(id)sender {
    
    
    NSString *uName = self.userName_Field.text;
    NSString *pWord = self.password_TextField.text;
    NSString *type = @"";
    NSString *fbID = @"123456";
    NSString *newslttr = @"1";
    NSString *status = @"1";
     
    
    
    if([userType isEqualToString:@"jobSeeker"])
            type = @"member";
            
    else
           type = @"employer";
        
       NSLog(@"%@",type);
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/adduser.php?username=%@&password=%@&facebook_id=%@&type=%@&active=%@&newsletter=%@",uName,pWord,fbID,type,status,newslttr]];
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    
    NSLog(@"=======>>%@",aUrl);
    
    registerRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
    
  
    
}





//=======================-----------NSURLCONNECTION METHODS---------================================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSLog(@"didReceiveResponse");
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
    
    if(connection == registerRequest)
    {
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"RegisterRequest...%@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate =self;
        [xmlParser parse];
        parser = xmlParser;
        
    }
    else if(connection == checkRequest){
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"checkRequest...%@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate =self;
        [xmlParser parse];
        parser = xmlParser;
        
        
    }
    else if(connection == loginRequest){
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"checkRequest...%@",responseString);
        
        NSXMLParser *userDetailsParser = [[NSXMLParser alloc] initWithData:_responseData];
        parserDelegate = [[UserDataXMLParser alloc] init];
        parserDelegate.delegate = self;
        userDetailsParser.delegate = parserDelegate;
        [userDetailsParser parse];
        
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConnection");
    
}
//=====================------------------ PARSING METHODS --------------=============================//
#pragma mark -- NSXMLParser Delegate methods

NSMutableString *parserElementString;


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"checkstatus"]) {
        parserElementString = [[NSMutableString alloc] init];
        NSLog(@"%@",parserElementString);
    }
    else if ([elementName isEqualToString:@"status"]) {
        parserElementString = [[NSMutableString alloc] init];
        NSLog(@"%@",parserElementString);
        
    }
   
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [parserElementString appendString:string];
    
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"status"]) {
        
        NSLog(@"=>>%@",elementName);
        
        //you have write code to login here and then if Success pass go below code
        
        if ([parserElementString isEqualToString:Status_success_message]) {
            
            NSString *uName = self.userName_Field.text;
            NSString *pWord = self.password_TextField.text;
            
            NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/login.php?username=%@&password=%@&type=%@",uName,pWord,currentUser]];
            NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                timeoutInterval:60.0];
            
            
            NSLog(@"Login URL:==>>%@",aUrl);
            
            loginRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
            
            
            
            
        }
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Registration Failed" message:@"Please provide valid details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }
     }
    else{
          checked = YES;
        [self.checkAvailability_ActivityIndicator stopAnimating];
        if ([parserElementString isEqualToString:@"EXISTS"])
        {
            
            self.warningLabel.hidden = NO;
            self.checkAvailability_ImageView.image = [UIImage imageNamed:@"bClose.png"];
            
        }
        else
        
            self.checkAvailability_ImageView.image = [UIImage imageNamed:@"checkbox_on.png"];
        

       }
}


- (void)parserDidEndDocument:(NSXMLParser *)parserLocal {
    parser.delegate = nil;
    parser = nil;
}

- (void)parser:(NSXMLParser *)parserLocal parseErrorOccurred:(NSError *)parseError {
    parser.delegate = nil;
    parser = nil;
}
//=========Userdata parser
- (void)parsingUserDetailsFinished:(User *)userDetails {
    
    if([userDetails.result isEqualToString:Status_success_message]){
        
        [[NSUserDefaults standardUserDefaults] setValue:userDetails.userId forKey:@"USERID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"USERID::::%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"]);
        
        if([userType isEqualToString:@"jobSeeker"])
        {

            [self configureUserViewControllers];
            
        }
        else
        {
            
            FillComInfoViewController *addressVC =  [[FillComInfoViewController alloc]initWithNibName:@"FillComInfoViewController" bundle:nil];
            [self.navigationController pushViewController:addressVC animated:YES];
        }
        
        
    }
    else{
        UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Network Connection Problem" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [failureAlert show];
        
    }
    
}


-(void)configureUserViewControllers{
    
    appDelegate.tabBarController =[[UITabBarController alloc]init];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor whiteColor], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateNormal];
    UIColor *titleHighlightedColor = [UIColor colorWithRed:153/255.0 green:192/255.0 blue:48/255.0 alpha:1.0];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, UITextAttributeTextColor,
                                                       nil] forState:UIControlStateHighlighted];
    
    // Change the tab bar background
    UIImage* tabBarBackground = [UIImage imageNamed:@"tabbar.png"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_selected.png"]];
    
    UserViewController *userVC = [[UserViewController alloc]initWithNibName:@"UserViewController" bundle:Nil];
    ListViewController *resumeVC = [[ListViewController alloc]initWithNibName:@"ListViewController" bundle:Nil];
    
    UserHelpViewController *userHelpVC = [[UserHelpViewController alloc]initWithNibName:@"UserHelpViewController" bundle:Nil];
    
    
    
    UINavigationController *nav1 = [[UINavigationController alloc]initWithRootViewController:userVC];
    nav1.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    UINavigationController *nav2 = [[UINavigationController alloc]initWithRootViewController:resumeVC];
    nav2.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    UINavigationController *nav3 = [[UINavigationController alloc]initWithRootViewController:userHelpVC];
    nav3.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    
    [userVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"home_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"home.png"]];
    [resumeVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"Resume_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"Resume.png"]];
    
    [userHelpVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"Help_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"Help.png"]];
    
    userVC.tabBarItem.title = @"Home";
    resumeVC.tabBarItem.title= @"My Resume/s";
    userHelpVC.tabBarItem.title= @"Help";
    
    appDelegate.tabBarController.viewControllers=[NSArray arrayWithObjects:nav1,nav2,nav3, nil];

    [self.navigationController pushViewController:appDelegate.tabBarController animated:YES];

}


-(void)presentNewViewController:(UIViewController *) controller
{
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController pushViewController:controller animated:NO];}
     completion:NULL];
    
}



- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag == 1 && checked)
      self.warningLabel.hidden = YES;
    self.checkAvailability_ImageView.image = nil;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if(textField.tag == 1){
        [self availability_Action];
        [self.checkAvailability_ActivityIndicator startAnimating];
    }
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
  
    [self setPassword_TextField:nil];
    [self setConformPwd_TextField:nil];
    [super viewDidUnload];
}
- (IBAction)actin:(id)sender {
    FillComInfoViewController *addressVC =  [[FillComInfoViewController alloc]initWithNibName:@"FillComInfoViewController" bundle:nil];
    [self.navigationController pushViewController:addressVC animated:YES];
}
@end

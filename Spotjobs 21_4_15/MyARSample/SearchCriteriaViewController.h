//
//  SearchCriteriaViewController.h
//  MyARSample
//
//  Created by vairat on 22/08/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RevealController.h"
#import "CategoryXMLParser.h"
#import "JobDetailsParser.h"
#import "JobDetails.h"

@interface SearchCriteriaViewController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate, NSURLConnectionDelegate, JobDetailsXMLParserDelegate, CategoryXMLParserDelegate>
{
     NSMutableData *_responseData;
}

@property(nonatomic,retain)IBOutlet UITextField *searchCriteria_TextField;
@property(nonatomic,retain)IBOutlet UITextField *suburb_TextField;
@property(nonatomic,retain)IBOutlet UITextField *keyword_TextField;
@property(nonatomic,retain)UITextField *current_TextField;

- (IBAction)findMeDeal_Action:(id)sender;
- (IBAction)resetButtonTapped:(id)sender;
- (IBAction)homeButtonTapped:(id)sender;
@end

//
//  EmployeSignUpViewController.m
//  MyARSample
//
//  Created by vairat on 26/10/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "FillComInfoViewController.h"
#import "NSString+SBJSON.h"
#import "AppDelegate.h"
#import "EditComInfoViewController.h"
#import "EmployeeHomeViewController.h"
#import "PostAJobViewController.h"
#import "UserHelpViewController.h"

#define Status_XML_tag @"status"
#define Status_success_message @"Success"

@interface FillComInfoViewController (){
    
    NSURLConnection *addCompanyRequest;
    NSURLConnection *addAddressRequest;
    
    NSXMLParser *parser;
    NSString *userID;
    
    AppDelegate *appDelegate;
}

@end

@implementation FillComInfoViewController
@synthesize frontView;
@synthesize backView;
@synthesize flipView;

@synthesize businessName_TextField;
@synthesize contactName_TextField;
@synthesize telephone_TextField;
@synthesize mobile_TextField;
@synthesize email_TextField;
@synthesize website_TextField;
@synthesize companyDec_TextView;

@synthesize address_TextField;
@synthesize street_TextField;
@synthesize suburb_TextField;
@synthesize state_TextField;
@synthesize country_TextField;
@synthesize postCode_TextField;
@synthesize adContctNo_TextField;
@synthesize adContctPer_TextField;
@synthesize adMobile_TextField;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil 
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setTranslucent:NO];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.title = @"Company Info";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
   
//    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
//	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
//	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
//	myButton1.showsTouchWhenHighlighted = YES;
//	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
//	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
//	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
//	self.navigationItem.leftBarButtonItem = leftButton;
    
   
    [flipView addSubview:frontView];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
    [keyBoardController addToolbarToKeyboard];
    
   
}


//=====================---------- BUTTON ACTIONS ---------========================//

- (IBAction)proceedButton_Action:(id)sender {
    
   // self.segmentedControl.hidden = YES;
    
    NSString *businessName = [self.businessName_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *contactName  = [self.contactName_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *telphone     = [self.telephone_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *mobile       = [self.mobile_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *email        = [self.email_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *website      = [self.website_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *companyDesc  = [self.companyDec_TextView.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"%@%@%@%@",businessName,contactName,telphone,mobile);
    
    
    userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    
     NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/addcompany.php?id=%@&cname=%@&contactname=%@&phno=%@&mobile=%@&email=%@&web=%@&desc=%@",userID,businessName,contactName,telphone,mobile,email,website,companyDesc]];
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    //NSLog(@"%@",[NSString stringWithFormat:@"http://184.107.152.50/app/webroot/newapp/spotjobs/addcompany.php?id=%@&cname=%@&contactname=%@&phno=%@&mobile=%@&email=%@&web=%@&desc=%@",userID,businessName,contactName,telphone,mobile,email,website,companyDesc]);

    NSLog(@"ADD COMPANYINFO URL==>%@",aUrl);
    
    addCompanyRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
  }


- (IBAction)getCoordinates_Action:(id)sender{
    
    
    
   
   
    NSString *str = [NSString stringWithFormat:@"%@ %@ %@",[self.address_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.street_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [self.suburb_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    
    /*
    NSString *esc_addr =  [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"=======>>%@",esc_addr);
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    
    NSDictionary *googleResponse = [[NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL] JSONValue];
    
    NSDictionary    *resultsDict = [googleResponse valueForKey:  @"results"];   // get the results dictionary
    NSDictionary   *geometryDict = [resultsDict valueForKey: @"geometry"];   // geometry dictionary within the  results dictionary
    NSDictionary   *locationDict = [geometryDict valueForKey: @"location"];   // location dictionary within the geometry dictionary
    
    NSArray *latArray = [locationDict valueForKey: @"lat"];
    NSString *latString = [latArray lastObject];     // (one element) array entries provided by the json parser
    
    NSArray *lngArray = [locationDict valueForKey: @"lng"];
    NSString *lngString = [lngArray lastObject];     // (one element) array entries provided by the json parser
    
    CLLocationCoordinate2D location;
    location.latitude = [latString doubleValue];// latitude;
    location.longitude = [lngString doubleValue]; //longitude;
    
    
    
    NSLog(@"latArray::%d",[latArray count]);
    NSLog(@"lonArray::%d",[lngArray count]);
    
    NSLog(@"location.latitude::%f",location.latitude);
    NSLog(@"location.longitude::%f",location.longitude);
    
    self.lat_Label.text = [NSString stringWithFormat:@"%f",location.latitude];
    self.lon_Label.text = [NSString stringWithFormat:@"%f",location.longitude]; */
    
    MapViewController *mapVC = [[MapViewController alloc]initWithNibName:@"MapViewController" bundle:Nil location:str];
    mapVC.delegate = self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mapVC];
    navController.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    [self presentModalViewController:navController animated:YES];
    
}
- (IBAction)addAddress_Action:(id)sender{
    
    
    NSString *contactName = [self.adContctPer_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *contactNo   = [self.adContctNo_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *mobileNo    = [self.adMobile_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *addres      = [self.address_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *street      = [self.street_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *suburb      = [self.suburb_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *state       = [self.state_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *country     = [self.country_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *postCode    = [self.postCode_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *latt        = self.lat_Label.text;
    NSString *lon         = self.lon_Label.text;
    userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    
    NSURL *aUrl;
    
        
    aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/addcompaddress.php?id=%@&contactname=%@&street=%@&address=%@&mobile=%@&phno=%@&city=%@&state=%@&postcode=%@&country=%@&lat=%@&long=%@&alt=%@",userID,contactName,street,addres,contactNo,mobileNo,suburb,state,postCode,country,latt,lon,latt]];
    
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    
    
    NSLog(@"ADDRESS URL==>%@",aUrl);
    
    addAddressRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
}



//=======================-----------NSURLCONNECTION METHODS---------================================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSLog(@"didReceiveResponse");
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
    
    if(connection == addCompanyRequest)
    {
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"addCompany Request::%@",responseString);
        
         NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
         xmlParser.delegate = self;
         [xmlParser parse];
         parser = xmlParser;
    }
    else if(connection == addAddressRequest)
    {
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"addAddress Request::::%@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate = self;
        [xmlParser parse];
        parser = xmlParser;
        
    }

    
   
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConnection");
    
}
//=============================================================================================//

#pragma mark -- NSXMLParser Delegate methods

NSMutableString *parserElementSt;



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"AddCompany"]) {
        parserElementSt = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"AddAddress"]) {
        parserElementSt = [[NSMutableString alloc] init];
    }
        
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [parserElementSt appendString:string];
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"AddCompany"])
    {
        
        if ([parserElementSt isEqualToString:Status_success_message]) {
            [self flip_Action];
                       
        }
          
        
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Unable to Add Company Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            }
    }
    else if ([elementName isEqualToString:@"AddAddress"])
    {
        if ([parserElementSt isEqualToString:@"Success"]){
            
            [self configureEmployeeViewControllers];
        
        }
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Unable to Add Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
        }
    }

}

//
-(void) mapViewControllerDismissed:(CLLocationCoordinate2D)coordinate{
    
    self.lat_Label.text = [NSString stringWithFormat:@"%f",coordinate.latitude];
    self.lon_Label.text = [NSString stringWithFormat:@"%f",coordinate.longitude];
    
}

-(void)configureEmployeeViewControllers{
    
    EmployeeHomeViewController *employee = [[EmployeeHomeViewController alloc]initWithNibName:@"EmployeeHomeViewController" bundle:Nil];
    
    PostAJobViewController *postVC =[[PostAJobViewController alloc]initWithNibName:@"PostAJobViewController" bundle:nil currentJob:  nil];
    
   EditComInfoViewController *editVC = [[EditComInfoViewController alloc]initWithNibName:@"EditComInfoViewController" bundle:Nil];
    
    UserHelpViewController *userHelpVC = [[UserHelpViewController alloc]initWithNibName:@"UserHelpViewController" bundle:Nil];
    
    
    UINavigationController *nav1 = [[UINavigationController alloc]initWithRootViewController:employee];
    nav1.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    UINavigationController *nav2 = [[UINavigationController alloc]initWithRootViewController:postVC];
    nav2.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    UINavigationController *nav3 = [[UINavigationController alloc]initWithRootViewController:editVC];
    nav3.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    UINavigationController *nav4 = [[UINavigationController alloc]initWithRootViewController:userHelpVC];
    nav4.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    
      nav1.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
      nav2.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
      nav3.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
      nav4.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    [employee.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"home_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"home.png"]];
    [postVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"Postjobselected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"PostAjob.png"]];
    [editVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabEdit_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tabEdit.png"]];
    [userHelpVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"Help_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"Help.png"]];
    
    employee.tabBarItem.title = @"Home";
    postVC.tabBarItem.title= @"Post A Job";
    editVC.tabBarItem.title= @"Edit Company";
    userHelpVC.tabBarItem.title= @"Help";
    
    appDelegate.tabBarController.viewControllers=[NSArray arrayWithObjects:nav1,nav2,nav3,nav4, nil];

    
    [self.navigationController pushViewController:appDelegate.tabBarController animated:YES];
    
    
    
}




- (IBAction)flip_Action{
    
    NSLog(@"testButton_Action");
    self.title = @"Company Address";
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
						   forView:flipView
							 cache:NO];
	
	[UIView commitAnimations];
    
    
    [flipView addSubview:backView];
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
    [keyBoardController addToolbarToKeyboard];
    
}


//=======

-(IBAction)backAction
{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                    completion:NULL];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
  
    [super viewDidUnload];
}
@end

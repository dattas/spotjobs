//
//  experienceViewController.h
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#import "UIKeyboardViewController.h"
#import <UIKit/UIKit.h>
#import "Resume.h"
#import "Experience.h"
@interface experienceViewController : UIViewController<UIKeyboardViewControllerDelegate>
{
    UIKeyboardViewController *keyBoardController;
    NSMutableArray *experiencedetails;
}
@property (nonatomic, retain)Experience *currentExperienceObj;

@property (strong, nonatomic)NSMutableArray *experiencedetails;
@property (strong, nonatomic) IBOutlet UIScrollView *experienceScrollView;
@property (strong, nonatomic)IBOutlet UITextField *currentTextField;
@property (strong, nonatomic) IBOutlet UITextField *companyTextField;
@property (strong, nonatomic) IBOutlet UITextField *positionTextField;
@property (strong, nonatomic) IBOutlet UITextField *periodTextField;
@property (strong, nonatomic) IBOutlet UITextField *locationTextfield;
@property (strong, nonatomic) IBOutlet UITextView *jobresponsibiltyTextView;
@property (strong, nonatomic) IBOutlet UIView *accessoryView;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj;
-(IBAction)doneButton_Pressed:(id)sender;
@end

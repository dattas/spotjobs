//
//  Project.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "Project.h"
#import "Resume.h"


@implementation Project

@dynamic duration;
@dynamic expertise;
@dynamic projecttitle;
@dynamic role;
@dynamic teamsize;
@dynamic insertedTime;
@dynamic ownby;

@end

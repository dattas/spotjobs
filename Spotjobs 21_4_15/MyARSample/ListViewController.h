//
//  ListViewController.h
//  MyARSample
//
//  Created by vairat on 06/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"

@interface ListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UINavigationControllerDelegate,UITextFieldDelegate,CustomIOS7AlertViewDelegate>
{
    
}
@property (strong, nonatomic)IBOutlet UITableView *myTableView;
@property (strong, nonatomic)UITextField *alertTextField;
@property (strong, nonatomic)UITextField *alertTextField7;

@property (nonatomic, retain) NSManagedObjectContext        *managedObjectContext;
@end

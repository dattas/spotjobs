//
//  ResumeViewController.h
//  MyARSample
//
//  Created by vairat on 01/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "personalinfoViewController.h"
#import "projectViewController.h"
#import "educationViewController.h"
#import "experienceViewController.h"
#import "referenceViewController.h"
#import "otherinfoViewController.h"
#import "PhotoSignViewController.h"
#import "ReaderViewController.h"

@interface ResumeViewController : UIViewController<ReaderViewControllerDelegate, UIAlertViewDelegate>
{
        
    
}
@property (strong, nonatomic) personalinfoViewController *personalinfoView;
@property (strong, nonatomic) projectViewController      *projectView;
@property (strong, nonatomic) educationViewController    *educationView;
@property (strong, nonatomic) experienceViewController   *experienceView;
@property (strong, nonatomic) referenceViewController    *referenceView;
@property (strong, nonatomic) otherinfoViewController    *otherinfoView;
@property (strong, nonatomic)UITextField *myTextField;
@property (nonatomic, retain) NSManagedObjectContext   *managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj;

- (IBAction)doneButton_Pressed:(id)sender;
- (IBAction)menuButton_Tapped:(id)sender;

@end

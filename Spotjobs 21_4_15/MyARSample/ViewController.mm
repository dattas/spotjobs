//
//  ViewController.m
//  MyARSample
//
//  Created by vairat on 26/06/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "EAGLView.h"
#include <metaioSDK/SensorsComponentIOS.h>
#import "AppDelegate.h"

@interface ViewController (){
    NSMutableArray *productsList;
    NSMutableArray *billboardList;
    ProductListParser *productsXMLParser;
    NSString *billboard_Name;
    AppDelegate *appDelegate;
}
@property(nonatomic,strong) NSMutableArray *productsList;
@property(nonatomic,strong) NSMutableArray *billboardList;
- (UIImage*) getBillboardImageForTitle: (NSString*) title;


@end

@implementation ViewController
@synthesize currentLocation;
@synthesize productsList;
@synthesize billboardList;
@synthesize locationManager;

#pragma mark - UIViewController lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // [self getCurrentLocation];
   // [self getproducts];
   
    self.billboardList = [[NSMutableArray alloc]init];
   // NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.myrewards.com.au/app/webroot/newapp/get_products_by_loc.php?lat=17.469383&lng=78.511902&cid=24&b=0.010000&c=Australia"]];
    
    // Create url connection and fire request
  //  NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    bool success = m_metaioSDK->setTrackingConfiguration("GPS");
    if( !success)
        NSLog(@"No success setting the tracking configuration");
    
    
    // if we want to use ImageBillboards, we should use UIBillboard Groups
	billboardGroup = m_metaioSDK->createBillboardGroup(580, 800);
	billboardGroup->setBillboardExpandFactors(0.8, 3, 10 );
   // m_metaioSDK->setRendererClippingPlaneLimits( 10, 10000000 );
    
    m_metaioSDK->setLLAObjectRenderingLimits(10, 10000);
    m_metaioSDK->setRendererClippingPlaneLimits(50, 100 * 1e+6);
    
    
    
    
    
    // load the content after some delay to let the GPS initialize
    [self performSelector:@selector(loadContent) withObject:nil afterDelay:1];
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
	// if the renderer appears we start rendering and capturing the camera
    
    [self startAnimation];
    m_metaioSDK->startCamera(0);
    [super viewWillAppear:animated];
    
//    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"product"]){
//          [self performSelector:@selector(getproducts) withObject:nil afterDelay:0.2];
//          [[NSUserDefaults standardUserDefaults] setValue:@"productsAvailable" forKey:@"product"];
//    }
}


-(void)getproducts{
    
    
   // NSString *urlString = [NSString stringWithFormat:@"http://www.myrewards.com.au/app/webroot/newapp/get_products_by_loc.php?lat=%f&lng=%f&cid=24&b=0.030000&c=Australia",self.currentLocation.latitude,self.currentLocation.longitude];
    
   // NSLog(@"url::%@",urlString);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.myrewards.com.au/app/webroot/newapp/get_products_by_loc.php?lat=17.469383&lng=78.511902&cid=24&b=0.010000&c=Australia"]];
    
    
   // NSString *urlString = [NSString stringWithFormat:@"http://www.myrewards.com.au/app/webroot/newapp/get_products_by_loc.php?lat=17.469383&lng=78.511902&cid=24&b=0.010000&c=Australia"];
    
    
   // NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}


-(void)getCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
  
    NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:_responseData];
    productsXMLParser = [[ProductListParser alloc] init];
    productsXMLParser.delegate = self;
    productsParser.delegate = productsXMLParser;
    [productsParser parse];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    locUpdate = true;
    //self.currentLocation = [newLocation coordinate];
    CLLocationCoordinate2D coordinate = [newLocation coordinate];
   
    self.currentLocation = coordinate;

    
    NSString *latitude = [NSString stringWithFormat:@"%f", self.currentLocation.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", self.currentLocation.longitude];
    
    
    NSLog(@"latitude : %@", latitude);
    NSLog(@"longitude : %@",longitude);
   
    [locationManager stopUpdatingLocation];
    
}

- (void)updateLocation
{
    NSLog(@"Location update is received");
    metaio::LLACoordinate currentPosition = m_sensors->getLocation();
      
    for(int i=0; i<[productsList count];i++)
    {
        Product *currentProduct = [appDelegate.productsList_Array objectAtIndex:i];
        
        metaio::LLACoordinate var = metaio::LLACoordinate(currentProduct.coordinate.latitude, currentProduct.coordinate.longitude, currentPosition.altitude, currentPosition.accuracy);
        
        if (myBillboard)
        {
            myBillboard->setTranslationLLA(var);
        }
    
    }
    
    
}





- (void)viewWillDisappear:(BOOL)animated
{
	// as soon as the view disappears, we stop rendering and stop the camera
   
    [self stopAnimation];
    m_metaioSDK->stopCamera();
    [super viewWillDisappear:animated];
}


- (void)viewDidUnload
{
    // Release any retained subviews of the main view.
    [self setGlView:nil];
    [super viewDidUnload];
}


- (void) loadContent
{
    locUpdate = false;
    
    // now let's load the content
    
    // hopefully we already have a location, so we can position our testcontent nearby
    metaio::LLACoordinate currentPosition = m_sensors->getLocation();
    
    if( currentPosition.accuracy > 0.0f )
    {
       
        
        m_radar = m_metaioSDK->createRadar();
        m_radar->setBackgroundTexture([[[NSBundle mainBundle] pathForResource:@"radar" ofType:@"png"] UTF8String]);
        m_radar->setObjectsDefaultTexture([[[NSBundle mainBundle] pathForResource:@"yellow" ofType:@"png"] UTF8String]);
        m_radar->setRelativeToScreen(metaio::IGeometry::ANCHOR_TL);
        
        for(int i=0; i<[appDelegate.productsList_Array count];i++)
        {
            Product *currentProduct = [appDelegate.productsList_Array objectAtIndex:i];
             NSLog(@"%@",currentProduct.productName);
            
            CLLocation *locA = [[CLLocation alloc] initWithLatitude:self.currentLocation.latitude longitude:self.currentLocation.longitude];
            
            CLLocation *locB = [[CLLocation alloc] initWithLatitude:currentProduct.coordinate.latitude longitude:currentProduct.coordinate.longitude];
            
            CLLocationDistance distance = [locA distanceFromLocation:locB];
            
            NSLog(@"%@ distancs from user is %f",currentProduct.productName,distance/1000);
            
            metaio::LLACoordinate var = metaio::LLACoordinate(currentProduct.coordinate.latitude, currentProduct.coordinate.longitude, currentPosition.altitude, distance/1000);
            
           NSString* formattedNumber = [NSString stringWithFormat:@"%.02f", distance/1000];
        
            billboard_Name = [NSString stringWithFormat:@"%@\n""%@ KM",currentProduct.productName,formattedNumber];
            
            myImage = [self getBillboardImageForTitle:billboard_Name];
            myBillboard = m_metaioSDK->createGeometryFromImage("A", [myImage CGImage]);
            myBillboard->setTranslationLLA(var);
            myBillboard->setLLALimitsEnabled(true);
          
            myBillboard->setRenderOrder(i);
            billboardGroup->addBillboard(myBillboard);
            
             m_radar->add(myBillboard);
            
        }
        
        
        
        
		metaio::SensorsComponentIOS* iosComponent = reinterpret_cast<metaio::SensorsComponentIOS*>(m_sensors);
		SensorsComponentImpl* iosImpl = iosComponent->getSensorComponentImpl();
		[iosImpl setLocationManagerDelegate:self];
        
		iosComponent->start(metaio::ISensorsComponent::SENSOR_LOCATION);
		
        //        CLLocationManager *locationManager = [[CLLocationManager alloc] init];
        //        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //        locationManager.delegate = self;
        //        [locationManager startUpdatingLocation];
    }
    else
    {
        // try again after some delay
        [self performSelector:@selector(loadContent) withObject:nil afterDelay:1];
        
    }
}




#pragma mark - Handling Touches

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Here's how to pick a geometry
	UITouch *touch = [touches anyObject];
	CGPoint loc = [touch locationInView:glView];
	
    // get the scale factor (will be 2 for retina screens)
    float scale = glView.contentScaleFactor;
    
	// ask sdk if the user picked an object
	// the 'true' flag tells sdk to actually use the vertices for a hit-test, instead of just the bounding box
    metaio::IGeometry* model = m_metaioSDK->getGeometryFromScreenCoordinates(loc.x * scale, loc.y * scale, true);
	
	if ( model )
	{
        metaio::LLACoordinate modelCoordinate = model->getTranslationLLA();
        int billboardIndex = model->getRenderOrder();
		NSLog(@"You picked a model at location %f, %f!", modelCoordinate.latitude, modelCoordinate.longitude);
        m_radar->setObjectsDefaultTexture([[[NSBundle mainBundle] pathForResource:@"yellow" ofType:@"png"] UTF8String]);
        m_radar->setObjectTexture(model, [[[NSBundle mainBundle] pathForResource:@"red" ofType:@"png"] UTF8String]);
        
        Product *prodt = [appDelegate.productsList_Array objectAtIndex:billboardIndex];
        NSLog(@"%@",prodt.productName);
      //=============&&&
//        ProductViewController *pvc = [[ProductViewController alloc]initWithNibName:@"ProductViewController" bundle:nil productID:prodt.productId];
//        
//        [self presentViewController:pvc animated:YES completion: nil];
      //++++++++++++++++
	}
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Implement if you need to handle touches
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Implement if you need to handle touches
}



- (void)drawFrame
{
    // tell the superclass to renderer
    [super drawFrame];
    if (locUpdate)
    {
        [self updateLocation];
        locUpdate = false;
    }
}



#pragma mark - Helper methods

- (UIImage*) getBillboardImageForTitle: (NSString*) title
{
    // first lets find out if we're drawing retina resolution or not
    float scaleFactor = [UIScreen mainScreen].scale;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        scaleFactor = 2;        // draw in high-res for iPad
    
    // then lets draw
    UIImage* bgImage = nil;
    NSString* imagePath;
    if( scaleFactor == 1 )	// potentially this is not necessary anyway, because iOS automatically picks 2x version for iPhone4
    {
        imagePath = [[NSBundle mainBundle] pathForResource:@"POI_bg" ofType:@"png"];
    }
    else
    {
        imagePath = [[NSBundle mainBundle] pathForResource:@"POI_bg@2x" ofType:@"png"];
    }
    
    bgImage = [[UIImage alloc] initWithContentsOfFile:imagePath];
    
    UIGraphicsBeginImageContext( bgImage.size );			// create a new image context
    CGContextRef currContext = UIGraphicsGetCurrentContext();
    
    // mirror the context transformation to draw the images correctly
    CGContextTranslateCTM( currContext, 0, bgImage.size.height );
    CGContextScaleCTM(currContext, 1.0, -1.0);
    CGContextDrawImage(currContext,  CGRectMake(0, 0, bgImage.size.width, bgImage.size.height), [bgImage CGImage]);
    
    // now bring the context transformation back to what it was before
    CGContextScaleCTM(currContext, 1.0, -1.0);
    CGContextTranslateCTM( currContext, 0, -bgImage.size.height );
    
    // and add some text...
    CGContextSetRGBFillColor(currContext, 1.0f, 1.0f, 1.0f, 1.0f);
    CGContextSetTextDrawingMode(currContext, kCGTextFill);
    CGContextSetShouldAntialias(currContext, true);
    
    // draw the heading
    float border = 5*scaleFactor;
    [title drawInRect:CGRectMake(border, border,
                                 bgImage.size.width - 2 * border,
                                 bgImage.size.height - 2 * border )
             withFont:[UIFont systemFontOfSize:5*scaleFactor]];
    
    // retrieve the screenshot from the current context
    UIImage* blendetImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return blendetImage;
}
//=========================================================================================






#pragma mark -- Product Parser Delegate methods

- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal {
    
    NSLog(@".......... productsListLocal: %d",[prodcutsListLocal count]);
    
    // Here update products list
    if (!productsList) {
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    }
    else {
        [productsList addObjectsFromArray:prodcutsListLocal];
    }
    NSLog(@"Products Count %d",[self.productsList count]);
    
}
- (void)parsingProductListXMLFailed {
    
}



@end

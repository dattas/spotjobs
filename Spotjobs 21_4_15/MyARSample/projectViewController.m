//
//  projectViewController.m
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#define kOFFSET_FOR_KEYBOARD 135
#import "projectViewController.h"
#import "AppDelegate.h"
#import "MediatorViewController.h"
@interface projectViewController ()
{
    
    AppDelegate *appDelegate;
    NSMutableDictionary *pro_Dictionary;
    
    NSMutableArray *months_Array;
    NSMutableArray *years_Array;
 
    UIActionSheet *myActionSheet;
    Resume *currRes;
    NSString *startMonth;     //startMonth
    NSString *startYear;   //startYear
    NSString *endMonth;    //endMonth
    NSString *endYear;  //endYear
    
}

@end

@implementation projectViewController

@synthesize currentProjectObj;
@synthesize projectScrollview;
@synthesize projecttitleTextField;
@synthesize durationtimeTextField;
@synthesize roleTextField;
@synthesize teamsizeTextField;
@synthesize expertiseTextField;
@synthesize currentTextField;
@synthesize accessoryView;
@synthesize projectdetails;
@synthesize durationButton;
@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currRes = resumeObj;
        projectdetails=[[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
 {
    
     [super viewDidLoad];
     
     startMonth= @"";
     startYear = @"";
     endMonth  = @"";
     endYear   = @"";
     [self.navigationController.navigationBar setTranslucent:NO];
     months_Array = [[NSMutableArray alloc]initWithCapacity:12];
     [months_Array addObjectsFromArray:[[NSArray alloc]initWithObjects:@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec",nil]];
    
     years_Array = [[NSMutableArray alloc]initWithCapacity:170];
     for(int i = 1970;i<=2100;i++)
         [years_Array addObject:[NSString stringWithFormat:@"%d",i]];
         

     
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationItem.title=@"Project Details";
    
     
    
    self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    
     self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back.png"]
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(backAction)];
     
     self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Save.png"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(saveData)];
    
     if(currentProjectObj)
         [self loadProjectdetails];

}

-(void)viewWillAppear:(BOOL)animated
{
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
	[keyBoardController addToolbarToKeyboard];


    if ([appDelegate isIphone5]) {
        projectScrollview.frame=CGRectMake(0,   0, 320, 500);
        [projectScrollview setContentSize:CGSizeMake(320, 678)];
        
    }
    else
    {
        projectScrollview.frame=CGRectMake(0,  0, 320, 460);
        [projectScrollview setContentSize:CGSizeMake(320, 678)];
    }

    //[self performSelector:@selector(loadProjectdetails) withObject:nil afterDelay:0.01];
}

-(void)backAction
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

-(void)saveData
{
    
    BOOL success = [self addProjectinfo];
   
    if(success)
    {
        pro_Dictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SortDic"] mutableCopy];
        if(!pro_Dictionary){
            pro_Dictionary = [[NSMutableDictionary alloc]init];
        }
        NSMutableArray *arr = [[pro_Dictionary objectForKey:@"Project"]mutableCopy];
        [arr addObject:self.projecttitleTextField.text];
        [pro_Dictionary setValue:arr forKey:@"Project"];
        
        [[NSUserDefaults standardUserDefaults] setValue:pro_Dictionary forKey:@"SortDic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Saved" message:@"data saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"data not  saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    MediatorViewController *mVc = [[MediatorViewController alloc]initWithNibName:@"MediatorViewController" bundle:Nil resume:currRes listType:@"project"];
    mVc.managedObjectContext = self.managedObjectContext;
    [self.navigationController pushViewController:mVc animated:NO];
    
    
}

-(BOOL) addProjectinfo
{
    
    NSManagedObjectContext *context = self.managedObjectContext;
    NSString *duration = [[NSString alloc]initWithFormat:@"%@ %@ - %@ %@",startMonth,startYear,endMonth,endYear];
    
    if(currentProjectObj)
    {
        
        currentProjectObj.projecttitle   = projecttitleTextField.text;
        currentProjectObj.duration       = duration;
        currentProjectObj.role           = roleTextField.text;
        currentProjectObj.teamsize       = teamsizeTextField.text;
        currentProjectObj.expertise      = expertiseTextField.text;
        currentProjectObj.ownby          = currRes;
        
    }
    else
    {
    Project *project = (Project *)[NSEntityDescription insertNewObjectForEntityForName:@"Project" inManagedObjectContext:context];
    
       
     project.projecttitle   = projecttitleTextField.text;
     project.duration       = duration;
     project.role           = roleTextField.text;
     project.teamsize       = teamsizeTextField.text;
     project.expertise      = expertiseTextField.text;
     project.insertedTime   = [NSDate date];
     project.ownby          = currRes;
        
    }
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding project data failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}


-(void) loadProjectdetails
{

    /*
    NSLog(@"loadProjectdetails");
    
    NSSet *projectSet  = currRes.projects;
    NSArray *projectsArray = [projectSet allObjects];
    
    for (Project *pro in projectsArray) {
        
        NSLog(@"projecttitle::%@",pro.projecttitle);
        NSLog(@"duration::%@",pro.duration);
        NSLog(@"role::%@",pro.role);
        NSLog(@"teamsize::%@",pro.teamsize);
        NSLog(@"expertise::%@",pro.expertise);
        
    }  */
    
    self.projecttitleTextField.text  =currentProjectObj.projecttitle ;
    self.durationtimeTextField.text  =currentProjectObj.duration;
    self.roleTextField.text          =currentProjectObj.role ;
    self.teamsizeTextField.text      =currentProjectObj.teamsize;
    self.expertiseTextField.text     =currentProjectObj.expertise ;

    

    
}

- (IBAction)durationTime:(id)sender
{
    NSLog(@"My method called ");
    UIButton *button = [[UIButton alloc]init];
    button.tag= 3;
    
    [keyBoardController toolbarButtonTap:button];
    
    [self projectDurationPicker];
}


-(void) projectDurationPicker
{
   

    myActionSheet = [[UIActionSheet alloc] init];
    myActionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    
    
    UIPickerView *myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
    myPickerView.dataSource = self;
    myPickerView.delegate   = self;
    myPickerView.showsSelectionIndicator = YES;
    [myActionSheet addSubview:myPickerView];
    
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,320,40)];
    tools.barStyle=UIBarStyleBlackOpaque;
    [myActionSheet addSubview:tools];
    
    
    
    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btnActinDoneClicked:)];
    doneButton.tag=1;
    doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
    
    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSArray *array = [[NSArray alloc]initWithObjects:flexSpace,flexSpace,doneButton,nil];
    [tools setItems:array];
    
    
    
    //picker title
    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(60,8, 200, 25)];
    lblPickerTitle.text=@"Select Project Duration";
    lblPickerTitle.backgroundColor=[UIColor clearColor];
    lblPickerTitle.textColor=[UIColor whiteColor];
    lblPickerTitle.textAlignment=UITextAlignmentCenter;
    lblPickerTitle.font=[UIFont boldSystemFontOfSize:15];
    [tools addSubview:lblPickerTitle];
    
    [myActionSheet showInView:[self.view superview]];
    // [myActionSheet showFromRect:CGRectMake(0,480, 320,215) inView:self.view animated:YES];
    [myActionSheet setBounds:CGRectMake(0,0, 320, 500)];
    
}

- (void) btnActinDoneClicked:(id)sender
{

         [myActionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
    
    NSLog(@"======>>%@",[NSString stringWithFormat:@"%@ %@ - %@ %@",startMonth,startYear,endMonth,endYear]);
    NSString *startDate = [NSString stringWithFormat:@"%@-%@-01",startYear,startMonth];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MMM-dd"];
    NSDate *currentDate1 =[NSDate date];

    NSString *endDate = [NSString stringWithFormat:@"%@-%@-01",endYear,endMonth];
    
    
    NSComparisonResult result;
    currentDate1 = [dateFormatter dateFromString:startDate];
    result = [[dateFormatter dateFromString:endDate] compare:currentDate1];
    NSLog(@"endDatewithformat is.. %@",[dateFormatter dateFromString:endDate]);
    NSLog(@"startDatewithformat is.. %@",currentDate1);
    NSLog(@"result is.. %d",result);
    
    switch (result)
    {
        case NSOrderedAscending:
        {
            NSLog(@"Ascending");
            
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Duration" message:@"End Date should be greater" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self.durationButton setTitle:@" " forState:UIControlStateNormal];
            
        }
            break;
        case NSOrderedDescending:
            NSLog(@"Descending");
            break;
            
        case NSOrderedSame:
            NSLog(@"same");
            break;
        default:
            NSLog(@"default");
            break;
            
    }
    
    
    
    
    
    
    
    
    
    
}

                      //========= UIPICKERVIEW DELEGATE METHODS ==============//
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

    if(component == 0)
        startMonth =  [months_Array objectAtIndex:row];
    
    else if (component == 2)
        startYear  =  [years_Array objectAtIndex:row];
    
    else if (component == 3)
        endMonth   =  [months_Array objectAtIndex:row];
    else
        endYear    =  [years_Array objectAtIndex:row];
    
     
    [self.durationButton setTitle:[NSString stringWithFormat:@"%@ %@ - %@ %@",startMonth,startYear,endMonth,endYear] forState:UIControlStateNormal];
    
}
-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component==0 || component==2)
        
        return [months_Array objectAtIndex:row];
    
    else  
        return [years_Array objectAtIndex:row];
    
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    
    if (component == 0 || component == 2)
        
        return [months_Array count];
    
    else 
        
        return [years_Array count];
    

}


-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 4;
}



//=======METHOD TO ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN =========//

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
        rect.origin.y -= kOFFSET_FOR_KEYBOARD+20;
        
    else
        rect.origin.y += kOFFSET_FOR_KEYBOARD+20;
        
    
    self.view.frame = rect;
    [UIView commitAnimations];
    
}
/*
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    
    [textField setInputAccessoryView:accessoryView];
    
    
    if( textField.tag == 2){
        [textField resignFirstResponder];
            [self projectDurationPicker];
    }
    
            
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
        
}
-(IBAction)doneButton_Pressed:(id)sender
{
    
    [currentTextField resignFirstResponder];
}
*/
//================================================================================================//
//=======================METHOD TO DONE BUTTON ====================//




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDurationButton:nil];
    [super viewDidUnload];
}
@end

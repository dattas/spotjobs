//
//  Others.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Resume;

@interface Others : NSManagedObject

@property (nonatomic, retain) NSString * drivingLicence;
@property (nonatomic, retain) NSString * panNo;
@property (nonatomic, retain) NSString * passportNo;
@property (nonatomic, retain) Resume *ownby;

@end

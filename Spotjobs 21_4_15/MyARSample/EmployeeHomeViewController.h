//
//  EmployeeHomeViewController.h
//  MyARSample
//
//  Created by vairat on 26/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobDetailsParser.h"
#import "JobDetails.h"

@interface EmployeeHomeViewController : UIViewController<JobDetailsXMLParserDelegate, UIAlertViewDelegate>{
    
     NSMutableData *_responseData;
    
}

@end

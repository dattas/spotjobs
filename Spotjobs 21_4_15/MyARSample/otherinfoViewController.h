//
//  otherinfoViewController.h
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#import "UIKeyboardViewController.h"
#import <UIKit/UIKit.h>
#import "Resume.h"
#import "Others.h"
@interface otherinfoViewController : UIViewController<UIKeyboardViewControllerDelegate>
{
    UIKeyboardViewController *keyBoardController;
    NSMutableArray *otherinfodetails;
}
@property (strong, nonatomic)NSMutableArray *otherinfodetails;
@property (strong, nonatomic) IBOutlet UIScrollView *otherinfoScrollView;
@property (strong, nonatomic) UITextField *currentTextField;
@property (strong, nonatomic) IBOutlet UITextField *drivinglicencenumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *passportnumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *pannumberTextField;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj;
@end

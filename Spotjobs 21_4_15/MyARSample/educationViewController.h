//
//  educationViewController.h
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//

#import "UIKeyboardViewController.h"
#import <UIKit/UIKit.h>
#import "Resume.h"
#import "Education.h"
@interface educationViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UIAlertViewDelegate,UIKeyboardViewControllerDelegate,UIActionSheetDelegate>
{
    UIKeyboardViewController *keyBoardController;
    NSMutableArray *educationdetails;
    
}
@property (nonatomic, retain) Education *currentEducationObj;

@property (strong, nonatomic) NSMutableArray *educationdetails;
@property (strong, nonatomic) IBOutlet UIScrollView *educationScrollView;
@property (strong, nonatomic) IBOutlet UITextField *degreecourseTextField;
@property (strong, nonatomic) IBOutlet UITextField *universityboardTextField;
@property (strong, nonatomic) IBOutlet UITextField *collegeschoolTextField;
@property (strong, nonatomic) IBOutlet UITextField *resultTextField;
@property (strong, nonatomic) IBOutlet UITextField *yearofpassingTextField;
@property (strong,nonatomic)  IBOutlet UITextField *currentTextField;
@property (strong, nonatomic) IBOutlet UIView *accessoryView;
@property (strong, nonatomic) IBOutlet UIButton *percentageButton;
@property (strong, nonatomic) IBOutlet UIButton *cgpaButtonTapped;
@property (strong, nonatomic) IBOutlet UILabel *resultLabel;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UIButton *YearofpassingButton;

- (IBAction)yop:(id)sender;
- (IBAction)percentageorcdpaTapped:(id)sender;
- (IBAction)doneButton_Pressed:(id)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj;
- (void)showCountryPicker;
- (void) projectDurationPicker;
@end

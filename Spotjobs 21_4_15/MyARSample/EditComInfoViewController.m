//
//  EditComInfoViewController.m
//  MyARSample
//
//  Created by vairat on 03/12/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "EditComInfoViewController.h"
#import "CompanyInfo.h"
#import "NSString+SBJSON.h"
#import "MerchantAddressCell.h"
#import "EmployeeHomeViewController.h"

#define Status_XML_tag @"status"
#define Status_success_message @"Success"

@interface EditComInfoViewController (){
    
    NSURLConnection *addCompanyRequest;
    NSURLConnection *getCompanyRequest;
    NSURLConnection *updateCompanyRequest;
    NSURLConnection *addAddressRequest;
    NSURLConnection *getAddressesRequest;
    NSURLConnection *deleteAddressRequest;
    
    CompanyInfoParser *CompanyInfoXMLParser;
    CompanyAddressParser *companyAddressXMLParser;
    CompanyAddress *editableAdress;
    NSMutableArray *address_Array;
    
    NSXMLParser *parser;
    NSString *userID;
    NSString *route;
    NSString *addressOperation;
}
@end

@implementation EditComInfoViewController

@synthesize segmentedControl;
@synthesize addAddressView;
@synthesize addressListView;
@synthesize editCompanyInfoView;

@synthesize editBusinessName_TextField;
@synthesize editContactName_TextField;
@synthesize editTelephone_TextField;
@synthesize editMobile_TextField;
@synthesize editEmail_TextField;
@synthesize editWebsite_TextField;
@synthesize editCompanyDec_TextView;

@synthesize address_TextField;
@synthesize street_TextField;
@synthesize suburb_TextField;
@synthesize state_TextField;
@synthesize country_TextField;
@synthesize postCode_TextField;
@synthesize adContctNo_TextField;
@synthesize adContctPer_TextField;
@synthesize adMobile_TextField;
@synthesize addressTable;
@synthesize noAddresslabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil fromView:(NSString *)viewName
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        route = viewName;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.title = @"Edit Company Info";
    
     keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
    [keyBoardController addToolbarToKeyboard];
    
    [self.segmentedControl setSelectedSegmentIndex:0];
    [self segmentedControlIndexChanged:self.segmentedControl];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    
}





- (IBAction)updateCompanyInfo_Action:(id)sender{
    
    
    NSString *businessName = self.editBusinessName_TextField.text;
    NSString *contactName  = self.editContactName_TextField.text;
    NSString *telphone     = self.editTelephone_TextField.text;
    NSString *mobile       = self.editMobile_TextField.text;
    NSString *email        = self.editEmail_TextField.text;
    NSString *website      = self.editEmail_TextField.text;
    NSString *companyDesc  = self.editCompanyDec_TextView.text;
    
    userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/editcompany.php?id=%@&cname=%@&contactname=%@&phno=%@&mobile=%@&email=%@&web=%@&desc=%@",userID,businessName,contactName,telphone,mobile,email,website,companyDesc]];
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    NSLog(@"EDIT COMPANY URL==>%@",aUrl);
    updateCompanyRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
    
    
    
    
}

- (IBAction)getCoordinates_Action:(id)sender{
    
    MapViewController *mapVC = [[MapViewController alloc]initWithNibName:@"MapViewController" bundle:Nil location:@""];
    mapVC.delegate = self;
    CLLocationCoordinate2D location;
    location.latitude = [self.lat_Label.text doubleValue];
    location.longitude = [self.lon_Label.text doubleValue];
    mapVC.currentCoordinates = location;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mapVC];
    navController.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    [self presentModalViewController:navController animated:YES];
    
}
- (IBAction)addAddress_Action:(id)sender{
    
    
    NSString *contactName = [self.adContctPer_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *contactNo   = [self.adContctNo_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *mobileNo    = [self.adMobile_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *addres      = [self.address_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *street      = [self.street_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *suburb      = [self.suburb_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *state       = [self.state_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *country     = [self.country_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *postCode    = [self.postCode_TextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *latt        = self.lat_Label.text;
    NSString *lon         = self.lon_Label.text;
    userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    
    NSURL *aUrl;
    if ([addressOperation isEqualToString:@"update"])
    {
        NSLog(@"Editing");
        aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/editaddress.php?aid=%@&contactname=%@&street=%@&address=%@&mobile=%@&phno=%@&city=%@&state=%@&postcode=%@&country=%@&lat=%@&long=%@&alt=%@",editableAdress.addressID,contactName,street,addres,contactNo,mobileNo,suburb,state,postCode,country,latt,lon,latt]];
    }
    
    else
    {
        NSLog(@"Adding");
        
        aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/addcompaddress.php?id=%@&contactname=%@&street=%@&address=%@&mobile=%@&phno=%@&city=%@&state=%@&postcode=%@&country=%@&lat=%@&long=%@&alt=%@",userID,contactName,street,addres,contactNo,mobileNo,suburb,state,postCode,country,latt,lon,latt]];
    }
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    
    
    NSLog(@"ADDRESS URL==>%@",aUrl);
    
    addAddressRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
}

-(void)getCompanydetails{
    
    userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/getcompany.php?id=%@",userID]];
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    
    
    NSLog(@"Get COMPANY URL==>%@",aUrl);
    
    getCompanyRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
}

-(void)getAddressesOfCompany{
    
    userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"];
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/getaddresses.php?id=%@",userID]];
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    
    
    NSLog(@"Get ADDERSSES URL==>%@",aUrl);
    
    getAddressesRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
}


-(IBAction) segmentedControlIndexChanged: (id)sender{
    
    //
    switch (self.segmentedControl.selectedSegmentIndex) {
            
        case 0:
            [self getAddressesOfCompany];
            [self.editCompanyInfoView removeFromSuperview];
            [self.addAddressView removeFromSuperview];
            
            
            break;
        case 1:{
            [self.editCompanyInfoView removeFromSuperview];
            self.addAddressView.frame = CGRectMake(6,45,306,436);
            [self.view addSubview:self.addAddressView];
            keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
            [keyBoardController addToolbarToKeyboard];
        }
            break;
            
        case 2:{
            [self getCompanydetails];
            [self.addAddressView removeFromSuperview];
            self.editCompanyInfoView.frame = CGRectMake(6,45,306,436);
            [self.view addSubview:self.editCompanyInfoView];
            keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
            [keyBoardController addToolbarToKeyboard];
        }
            break;
            
        default:
            break;
    }
    
}

-(void)configureCompanyDeatails:(CompanyInfo *) companyInfo{
    
    
    self.editBusinessName_TextField.text = companyInfo.businessName;
    self.editContactName_TextField.text  = companyInfo.contactName;
    self.editTelephone_TextField.text    = companyInfo.telphone;
    self.editMobile_TextField.text       = companyInfo.mobile;
    self.editEmail_TextField.text        = companyInfo.email;
    self.editWebsite_TextField.text      = companyInfo.website;
}

-(void)configureEditableAddress:(CompanyAddress *)currentEditableAdress{
    
    self.adContctPer_TextField.text   = currentEditableAdress.contactName;
    self.adContctNo_TextField.text    = currentEditableAdress.contactNumber;
    self.adMobile_TextField.text      = currentEditableAdress.mobileNumber;
    self.address_TextField.text       = currentEditableAdress.address;
    self.street_TextField.text        = currentEditableAdress.street;
    self.suburb_TextField.text        = currentEditableAdress.suburb;
    self.state_TextField.text         = currentEditableAdress.state;
    self.country_TextField.text       = currentEditableAdress.country;
    self.postCode_TextField.text      = currentEditableAdress.postCode;
    self.lat_Label.text               = currentEditableAdress.latitude;
    self.lon_Label.text               = currentEditableAdress.longitude;
    
}

-(void)clearAddressfields{
    
    self.adContctPer_TextField.text   = @"";
    self.adContctNo_TextField.text    = @"";
    self.adMobile_TextField.text      = @"";
    self.address_TextField.text       = @"";
    self.street_TextField.text        = @"";
    self.suburb_TextField.text        = @"";
    self.state_TextField.text         = @"";
    self.country_TextField.text       = @"";
    self.postCode_TextField.text      = @"";
    self.lat_Label.text               = @"Latitude";
    self.lon_Label.text               = @"Longitude";
    
    
    addressOperation = @"";
    
}


//=======================-----------NSURLCONNECTION METHODS---------================================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSLog(@"didReceiveResponse");
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
    
    if(connection == addCompanyRequest)
    {
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"addCompany Request::%@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate = self;
        [xmlParser parse];
        parser = xmlParser;
        
    }
    else if(connection == getCompanyRequest)
    {
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"getCompany Request::::%@",responseString);
        
        NSXMLParser *companyParser = [[NSXMLParser alloc] initWithData:_responseData];
        CompanyInfoXMLParser = [[CompanyInfoParser alloc] init];
        CompanyInfoXMLParser.delegate = self;
        companyParser.delegate = CompanyInfoXMLParser;
        [companyParser parse];
        
    }
    else if(connection == updateCompanyRequest)
    {
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"updateCompany Request::::%@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate = self;
        [xmlParser parse];
        parser = xmlParser;
        
    }
    
    else if(connection == addAddressRequest)
    {
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"addAddress Request::::%@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate = self;
        [xmlParser parse];
        parser = xmlParser;
        
    }
    else if(connection == getAddressesRequest){
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"GetAddresses Request::::%@",responseString);
        if([responseString isEqualToString:@""])
            [self refreshTableContents:NULL];
        
        else{
            NSXMLParser *companyAddrParser = [[NSXMLParser alloc] initWithData:_responseData];
            companyAddressXMLParser = [[CompanyAddressParser alloc] init];
            companyAddressXMLParser.delegate = self;
            companyAddrParser.delegate = companyAddressXMLParser;
            [companyAddrParser parse];
        }
    }
    else if(connection == deleteAddressRequest)
    {
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"delete AddressRequest::::%@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate = self;
        [xmlParser parse];
        parser = xmlParser;
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConnection");
    
}
//=============================================================================================//

#pragma mark -- NSXMLParser Delegate methods

NSMutableString *parserElementStr;



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if ([elementName isEqualToString:@"EditCompany"]) {
        parserElementStr = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"AddAddress"]) {
        parserElementStr = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"EditAddress"]) {
        parserElementStr = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"DeleteAddress"]) {
        parserElementStr = [[NSMutableString alloc] init];
    }
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [parserElementStr appendString:string];
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
        
    if ([elementName isEqualToString:@"EditCompany"])
    {
        if ([parserElementStr isEqualToString:Status_success_message]){
            
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"SUCCESS" message:@"Address is Successfully Updated" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [successAlert show];
            [self clearAddressfields];
        }
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Unable to Update Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
        }
    }
    
    else if ([elementName isEqualToString:@"EditAddress"])
    {
        if ([parserElementStr isEqualToString:Status_success_message]){
            
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"SUCCESS" message:@"Address is Successfully Updated" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [successAlert show];
            [self clearAddressfields];
        }
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Unable to Update Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
        }
    }
    
    else if ([elementName isEqualToString:@"DeleteAddress"])
    {
        if ([parserElementStr isEqualToString:@"Success"]){
            
            NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/getaddresses.php?id=%@",userID]];
            NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                timeoutInterval:60.0];
            NSLog(@"Get ADDERSSES URL==>%@",aUrl);
            
            getAddressesRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
        }
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Unable to Add Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
        }
    }
    
    else if ([elementName isEqualToString:@"AddAddress"])
    {
        if ([parserElementStr isEqualToString:@"Success"]){
            
            EmployeeHomeViewController *emp = [[EmployeeHomeViewController alloc]initWithNibName:@"EmployeeHomeViewController" bundle:Nil];
           
            
            
            
        }
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Unable to Add Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
        }
    }
    
    
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parserLocal {
    parser.delegate = nil;
    parser = nil;
}

- (void)parser:(NSXMLParser *)parserLocal parseErrorOccurred:(NSError *)parseError {
    parser.delegate = nil;
    parser = nil;
}

//=======

- (void) parsingCompanyDetailsFinished:(CompanyInfo *) companyDetails{
    
    
    NSLog(@"parsingCompanyDetailsFinished");
    
    [self configureCompanyDeatails:companyDetails];
    
}
- (void) companyDetailXMLparsingFailed{}
//===========================================

- (void) parsingCompanyAddressFinished:(NSArray *) companyAddresses_Array{
    
    NSLog(@"parsingCompanyAddressFinished");
    
    
    [self refreshTableContents:companyAddresses_Array];
    
}
-(void)refreshTableContents:(NSArray *)arr{
    
    if(!address_Array){
        
        address_Array = [[NSMutableArray alloc]init];
    }
    [address_Array removeAllObjects];
    [address_Array addObjectsFromArray:arr];
    [self.addressTable reloadData];
    
}
- (void) companyAddressXMLparsingFailed{
    
    NSLog(@"companyAddressXMLparsingFailed");
    
}


//==================----------------- UITABLEVIEW DELEGATE METHODS ------------======================//

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    
    if([address_Array count] > 0)
        self.noAddresslabel.hidden = YES;
    else
        self.noAddresslabel.hidden = NO;
    
    return [address_Array count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    
    
    
    
    MerchantAddressCell *cell = (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"MerchantAddressCell" owner:nil options:nil];
        
        for (UIView *cellview in views) {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (MerchantAddressCell*)cellview;
            }
        }
        
    }
    
    
    CompanyAddress *curCompanyAddr = [address_Array objectAtIndex:indexPath.row];
    
    
    cell.merchantAddressLabel.text = [NSString stringWithFormat:@"%@\n%@\n%@\n%@-%@",curCompanyAddr.contactName,curCompanyAddr.suburb,curCompanyAddr.state,curCompanyAddr.country,curCompanyAddr.postCode];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@,\n%@,%@,%@,%@-%@",curCompanyAddr.address,curCompanyAddr.street,curCompanyAddr.suburb,curCompanyAddr.state,curCompanyAddr.country,curCompanyAddr.postCode]);
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    addressOperation = @"update";
    editableAdress   = [address_Array objectAtIndex:indexPath.row];
    [self.segmentedControl setSelectedSegmentIndex:1];
    [self segmentedControlIndexChanged:self.segmentedControl];
    [self configureEditableAddress:editableAdress];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CompanyAddress *compAddr = [address_Array objectAtIndex:indexPath.row];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/deleteaddress.php?aid=%@",compAddr.addressID]];
        NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
        
        
        
        NSLog(@"Delete address URL==>%@",aUrl);
        
        deleteAddressRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
        
    }
}

-(void) mapViewControllerDismissed:(CLLocationCoordinate2D)coordinate{
    
    self.lat_Label.text = [NSString stringWithFormat:@"%f",coordinate.latitude];
    self.lon_Label.text = [NSString stringWithFormat:@"%f",coordinate.longitude];
    
}
-(void)backaction
{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                    completion:NULL];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

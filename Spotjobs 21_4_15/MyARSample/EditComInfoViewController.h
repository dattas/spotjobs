//
//  EditComInfoViewController.h
//  MyARSample
//
//  Created by vairat on 03/12/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"
#import "UIKeyboardViewController.h"
#import "CompanyInfoParser.h"
#import "CompanyAddressParser.h"

@interface EditComInfoViewController : UIViewController<UIKeyboardViewControllerDelegate,NSURLConnectionDelegate, NSXMLParserDelegate,CompanyInfoXMLParser,CompanyAddressXMLParserDelegate, MapDelegate>{
    
    UIKeyboardViewController *keyBoardController;
    NSMutableData *_responseData;
   
}

@property (strong, nonatomic) IBOutlet UILabel *noAddresslabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIView *addAddressView;
@property (strong, nonatomic) IBOutlet UIView *addressListView;
@property (strong, nonatomic) IBOutlet UIView *editCompanyInfoView;

@property (strong, nonatomic) IBOutlet UITextField *editBusinessName_TextField;
@property (strong, nonatomic) IBOutlet UITextField *editContactName_TextField;
@property (strong, nonatomic) IBOutlet UITextField *editTelephone_TextField;
@property (strong, nonatomic) IBOutlet UITextField *editMobile_TextField;
@property (strong, nonatomic) IBOutlet UITextField *editEmail_TextField;
@property (strong, nonatomic) IBOutlet UITextField *editWebsite_TextField;

@property (strong, nonatomic) IBOutlet UITextView *companyDec_TextView;
@property (strong, nonatomic) IBOutlet UITextView *editCompanyDec_TextView;

//add address Fields
@property (strong, nonatomic) IBOutlet UITextField *address_TextField;
@property (strong, nonatomic) IBOutlet UITextField *street_TextField;
@property (strong, nonatomic) IBOutlet UITextField *suburb_TextField;
@property (strong, nonatomic) IBOutlet UITextField *state_TextField;
@property (strong, nonatomic) IBOutlet UITextField *country_TextField;
@property (strong, nonatomic) IBOutlet UITextField *postCode_TextField;
@property (strong, nonatomic) IBOutlet UITextField *adContctNo_TextField;
@property (strong, nonatomic) IBOutlet UITextField *adMobile_TextField;
@property (strong, nonatomic) IBOutlet UITextField *adContctPer_TextField;

@property (strong, nonatomic) IBOutlet UILabel *lat_Label;
@property (strong, nonatomic) IBOutlet UILabel *lon_Label;
@property (strong, nonatomic) IBOutlet UITableView *addressTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil fromView:(NSString *)viewName;
- (IBAction)updateCompanyInfo_Action:(id)sender;
- (IBAction)segmentedControlIndexChanged: (id)sender;
- (IBAction)getCoordinates_Action:(id)sender;
- (IBAction)addAddress_Action:(id)sender;

@end

//
//  UIKeyboardViewController.m
// 
//
//  Created by  YFengchen on 13-1-4.
//  Copyright 2013 __zhongyan__. All rights reserved.
//

#import "UIKeyboardViewController.h"
#import "Category.h"
#import "AppDelegate.h"

static CGFloat kboardHeight = 254.0f;
static CGFloat keyBoardToolbarHeight = 38.0f;
static CGFloat spacerY = 10.0f;
static CGFloat viewFrameY = 0;

@interface UIKeyboardViewController () {
    AppDelegate *appDelegate;
    UIDatePicker *myDatePicker;
    
    NSMutableArray *months_Array;
    NSMutableArray *years_Array;
}

- (void)animateView:(BOOL)isShow textField:(id)textField heightforkeyboard:(CGFloat)kheight;
- (void)addKeyBoardNotification;
- (void)removeKeyBoardNotification;
- (void)checkBarButton:(id)textField;
- (id)firstResponder:(UIView *)navView;
- (NSArray *)allSubviews:(UIView *)theView;
- (void)resignKeyboard:(UIView *)resignView;

@end

@implementation UIKeyboardViewController
@synthesize currentScrollView;
@synthesize boardDelegate = _boardDelegate;

- (void)dealloc {
    _boardDelegate = nil;
	[self removeKeyBoardNotification];
	[super dealloc];
}

// Listen for keyboard hide and show events
- (void)addKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillHideNotification object:nil];
}

// Listen for events canceled
- (void)removeKeyBoardNotification {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Calculate the height of the current keyboard
-(void)keyboardWillShowOrHide:(NSNotification *)notification {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
#endif
		kboardHeight = 264.0f + keyBoardToolbarHeight;
	}
	NSValue *keyboardBoundsValue;
	if (IOS_VERSION >= 3.2) {
		keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
	}
	else {
		keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey];
	}
	[keyboardBoundsValue getValue:&keyboardBounds];
	BOOL isShow = [[notification name] isEqualToString:UIKeyboardWillShowNotification] ? YES : NO;
	if ([self firstResponder:objectView]) {
		[self animateView:isShow textField:[self firstResponder:objectView]
		heightforkeyboard:keyboardBounds.size.height];
	}
}

// Move to prevent keyboard input box cover
- (void)animateView:(BOOL)isShow textField:(id)textField heightforkeyboard:(CGFloat)kheight {
	kboardHeight = kheight;
	[self checkBarButton:textField];
    
if(currentScrollView)
     {
         if ([textField isKindOfClass:[UITextField class]]) {
             UITextField *newText = ((UITextField *)textField);
        
             switch (newText.tag) {
                 case 12:
                         [self.currentScrollView setContentOffset:CGPointMake(0, 280) animated:YES];
                          break;
                 case 13:
                         [self.currentScrollView setContentOffset:CGPointMake(0, 330) animated:YES];
                          break;
                 case 14:
                         [self.currentScrollView setContentOffset:CGPointMake(0, 390) animated:YES];
                          break;
                 case 15:
                         [self.currentScrollView setContentOffset:CGPointMake(0, 300) animated:YES];
                          break;
                 case 16:
                        [self.currentScrollView setContentOffset:CGPointMake(0, 340) animated:YES];
                         break;
                 case 17:
                        [self.currentScrollView setContentOffset:CGPointMake(0, 380) animated:YES];
                         break;
                 case 10:
                        [self.currentScrollView setContentOffset:CGPointMake(0, 420) animated:YES];
                         break;
                 case 6:
                        [self.currentScrollView setContentOffset:CGPointMake(0, 470) animated:YES];
                         break;
                 case 7:
                        [self.currentScrollView setContentOffset:CGPointMake(0, 510) animated:YES];
                         break;
                     
                 default:
                     break;
             }
             
         }
         else {
             NSLog(@"else isShow.....");
             UITextView *newView = ((UITextView *)textField);
             
             
             switch (newView.tag) {
                 case 100:
                     [self.currentScrollView setContentOffset:CGPointMake(0, 500) animated:YES];
                     break;
                 case 200:
                     [self.currentScrollView setContentOffset:CGPointMake(0, 380) animated:YES];
                     break;
                 case 300:
                     [self.currentScrollView setContentOffset:CGPointMake(0, 560) animated:YES];
                     break;
                 case 400:
                     [self.currentScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
                     break;
                 case 500:
                     [self.currentScrollView setContentOffset:CGPointMake(0, 240) animated:YES];
                     break;
                 default:
                     break;
             
                         }
         }}
else{
	   CGRect rect = objectView.frame;
	   [UIView beginAnimations:nil context:NULL];
	   [UIView setAnimationDuration:0.3];
	if (isShow) {
       
		if ([textField isKindOfClass:[UITextField class]]) {
			UITextField *newText = ((UITextField *)textField);
			CGPoint textPoint = [newText convertPoint:CGPointMake(0, newText.frame.size.height + spacerY) toView:objectView];
			if (rect.size.height - textPoint.y < kheight)
				rect.origin.y = rect.size.height - textPoint.y - kheight + viewFrameY;
			else rect.origin.y = viewFrameY;
		}
		else {
            
			UITextView *newView = ((UITextView *)textField);
			CGPoint textPoint = [newView convertPoint:CGPointMake(0, newView.frame.size.height + spacerY) toView:objectView];
			if (rect.size.height - textPoint.y < kheight) 
				rect.origin.y = rect.size.height - textPoint.y - kheight + viewFrameY;
			else rect.origin.y = viewFrameY;
		}
	  }
	else rect.origin.y = viewFrameY;
	objectView.frame = rect;
	[UIView commitAnimations];
    }
}

// Input box gets the focus
- (id)firstResponder:(UIView *)navView {
	for (id aview in [self allSubviews:navView]) {
		if ([aview isKindOfClass:[UITextField class]] && [(UITextField *)aview isFirstResponder]) {
			return (UITextField *)aview;
		}
		else if ([aview isKindOfClass:[UITextView class]] && [(UITextView *)aview isFirstResponder]) {
			return (UITextView *)aview;
		}
	}
	return NO;
}

// Find all subview
- (NSArray *)allSubviews:(UIView *)theView {
	NSArray *results = [theView subviews];
	for (UIView *eachView in [theView subviews]) {
		NSArray *riz = [self allSubviews:eachView];
		if (riz) {
			results = [results arrayByAddingObjectsFromArray:riz];
		}
	}
	return results;
}

// Input box loses focus, hide the keyboard
- (void)resignKeyboard:(UIView *)resignView {
	[[[UIApplication sharedApplication] keyWindow] endEditing:YES];
     [self.currentScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
}

// Set previousBarItem whether to allow or nextBarItem Click
- (void)checkBarButton:(id)textField {
	int i = 0,j = 0;
	UIBarButtonItem *previousBarItem = [keyboardToolbar itemForIndex:0];
    UIBarButtonItem *nextBarItem = [keyboardToolbar itemForIndex:1];
	for (id aview in [self allSubviews:objectView]) {
		if ([aview isKindOfClass:[UITextField class]] && ((UITextField*)aview).userInteractionEnabled && ((UITextField*)aview).enabled) {
			i++;
			if ([(UITextField *)aview isEqual:textField]) {
				j = i;
			}
		}
		else if ([aview isKindOfClass:[UITextView class]] && ((UITextView*)aview).userInteractionEnabled && ((UITextView*)aview).editable) {
			i++;
			if ([(UITextView *)aview isEqual:textField]) {
				j = i;
			}
		}
	}
	[previousBarItem setEnabled:j > 1 ? YES : NO];
	[nextBarItem setEnabled:j < i ? YES : NO];
}

//toolbar button Click on the event
#pragma mark - UIKeyboardView delegate methods
-(void)toolbarButtonTap:(UIButton *)button {
	NSInteger buttonTag = button.tag;
	NSMutableArray *textFieldArray=[NSMutableArray arrayWithCapacity:15];
	for (id aview in [self allSubviews:objectView]) {
		if ([aview isKindOfClass:[UITextField class]] && ((UITextField*)aview).userInteractionEnabled && ((UITextField*)aview).enabled) {
			[textFieldArray addObject:(UITextField *)aview];
		}
		else if ([aview isKindOfClass:[UITextView class]] && ((UITextView*)aview).userInteractionEnabled && ((UITextView*)aview).editable) {
			[textFieldArray addObject:(UITextView *)aview];
		}
	}
	for (int i = 0; i < [textFieldArray count]; i++) {
		id textField = [textFieldArray objectAtIndex:i];
		if ([textField isKindOfClass:[UITextField class]]) {
			textField = ((UITextField *)textField);
		}
		else {
			textField = ((UITextView *)textField);
		}
		if ([textField isFirstResponder]) {
			if (buttonTag == 1) {
				if (i > 0) {
					[[textFieldArray objectAtIndex:--i] becomeFirstResponder];
					[self animateView:YES textField:[textFieldArray objectAtIndex:i] heightforkeyboard:kboardHeight];
				}
			}
			else if (buttonTag == 2) {
				if (i < [textFieldArray count] - 1) {
					[[textFieldArray objectAtIndex:++i] becomeFirstResponder];
					[self animateView:YES textField:[textFieldArray objectAtIndex:i] heightforkeyboard:kboardHeight];
				}
			}
		}
	}
	if (buttonTag == 3)
		[self resignKeyboard:objectView];
}


#pragma mark - TextField delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
	[self checkBarButton:textField];
    
    NSLog(@"textFieldDidBeginEditing:%d",textField.tag);
    if(textField.tag == 11 ||textField.tag == 22 ||textField.tag == 33){
        
        months_Array = [[NSMutableArray alloc]initWithCapacity:12];
        [months_Array addObjectsFromArray:[[NSArray alloc]initWithObjects:@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec",nil]];
        
        years_Array = [[NSMutableArray alloc]initWithCapacity:130];
        for(int i = 1970;i<=2100;i++)
            [years_Array addObject:[NSString stringWithFormat:@"%d",i]];
        
        UIPickerView *myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
        myPickerView.dataSource = self;
        myPickerView.delegate   = self;
        myPickerView.tag        = textField.tag;
        myPickerView.showsSelectionIndicator = YES;
        textField.inputView     = myPickerView;
    
    }
    else if(textField.tag == 6 || textField.tag == 7){
        myDatePicker = [[UIDatePicker alloc]init];
        myDatePicker.date = [NSDate date];
        myDatePicker.datePickerMode = UIDatePickerModeDate;
        [myDatePicker addTarget:self action:@selector(dateSelected:) forControlEvents:UIControlEventValueChanged];
       
        if(textField.tag == 6 )
            myDatePicker.tag = 1;
        else
            myDatePicker.tag = 2;
        
        textField.inputView = myDatePicker;
        
        
    }
    else if(textField.tag == 10){
        UIPickerView *myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
        myPickerView.dataSource = self;
        myPickerView.delegate   = self;
        myPickerView.tag        = textField.tag;
        myPickerView.showsSelectionIndicator = YES;
        textField.inputView     = myPickerView;
    }
    
}

-(void)dateSelected:(UIDatePicker *)currentDatePicker{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    //[dateFormatter setDateFormat:@"dd-MM-yyyy"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    if(currentDatePicker.tag == 1)
       appDelegate.startDate = [dateFormatter stringFromDate:[myDatePicker date]];
    else
       appDelegate.endDate = [dateFormatter stringFromDate:[myDatePicker date]];
    
    
    NSLog(@"appDelegate.startDate ::%@",appDelegate.startDate);
    NSLog(@"appDelegate.endDate ::%@",appDelegate.endDate);
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"================++++++++++++++++++++++++=======================");
	[textField resignFirstResponder];
    [self.currentScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	if ([self.boardDelegate respondsToSelector:@selector(alttextFieldDidEndEditing:)]) {
		[self.boardDelegate alttextFieldDidEndEditing:textField];
	}
}

#pragma mark - UITextView delegate methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {    
	if ([text isEqualToString:@"\n"]) {    
		//[textView resignFirstResponder];
		//return NO;
	}
	return YES;    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	if ([self.boardDelegate respondsToSelector:@selector(alttextViewDidEndEditing:)]) {
		[self.boardDelegate alttextViewDidEndEditing:textView];
	}
}

@end

@implementation UIKeyboardViewController (UIKeyboardViewControllerCreation)


- (id)initWithControllerDelegate:(id <UIKeyboardViewControllerDelegate>)delegateObject {
	if (self = [super init]) {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		self.boardDelegate = delegateObject;
        if ([self.boardDelegate isKindOfClass:[UIViewController class]]) {
			objectView = [(UIViewController *)[self boardDelegate] view];
            //[self addKeyBoardNotification];
		}
		else if ([self.boardDelegate isKindOfClass:[UIView class]]) {
			objectView = (UIView *)[self boardDelegate];
            //[self addKeyBoardNotification];
		}
        viewFrameY = objectView.frame.origin.y;
		[self addKeyBoardNotification];
	}
	return self;
}

@end

@implementation UIKeyboardViewController (UIKeyboardViewControllerAction)

//Coupled to the keyboard toolbar
- (void)addToolbarToKeyboard {
	keyboardToolbar = [[UIKeyboardView alloc] initWithFrame:CGRectMake(0, 0, objectView.frame.size.width, keyBoardToolbarHeight)];
	keyboardToolbar.delegate = self;
	for (id aview in [self allSubviews:objectView]) {
		if ([aview isKindOfClass:[UITextField class]]) {
			((UITextField *)aview).inputAccessoryView = keyboardToolbar;
			((UITextField *)aview).delegate = self;
		}
		else if ([aview isKindOfClass:[UITextView class]]) {
			((UITextView *)aview).inputAccessoryView = keyboardToolbar;
			((UITextView *)aview).delegate = self;
		}
	}
	[keyboardToolbar release];
}

//================
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 11) {
    
    
    }
    else{
    Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
    appDelegate.selectdCatID   = catObj.catId;
    appDelegate.selectdCatName = catObj.catName;
    }
}
//-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
//    return catObj.catName;
//}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 45)];
    
    [label setTextAlignment:UITextAlignmentCenter];
    label.opaque=NO;
    label.backgroundColor=[UIColor clearColor];
    label.textColor = [UIColor blackColor];
    UIFont* textFont = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    
    label.font = textFont;
    if (pickerView.tag == 11) {
        if(component == 0)
            label.text = [months_Array objectAtIndex:row];
        else
            label.text = [years_Array objectAtIndex:row];
    }
    else if(pickerView.tag == 22 || pickerView.tag == 33){
        
        if(component == 0 || component == 2)
            label.text = [months_Array objectAtIndex:row];
        else
            label.text = [years_Array objectAtIndex:row];
            
    }
    else{
    Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
    label.text = catObj.catName;
    }
    
    return label;
    
}
-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 11) {
        if(component == 0)
           return [months_Array count];
        else
           return [years_Array count];
    }
    else if(pickerView.tag == 22 || pickerView.tag == 33){
        
        if(component == 0 || component == 2)
            return [months_Array count];
        else
            return [years_Array count];
    }
    else
    return [appDelegate.categoryList_Array count];
    
}


-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (pickerView.tag == 11)
        return 2;
    else if(pickerView.tag == 22 || pickerView.tag == 33)
        return 4;
    else
        return 1;
}
@end

//============== TAGS ALLOCATION

 // PICKER TAGS      1,2,3..................
 //TEXTFIELD TAGS    11,12,13,14............
 //TEXTVIEW TAGS     100,200,300............
















//
//  ProductCell.h
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell
{
    
}

@property (nonatomic, strong) IBOutlet UIImageView *logo_ImageView;
@property (nonatomic, strong) IBOutlet UILabel *jobTitle_Label;
@property (nonatomic, strong) IBOutlet UILabel *jobDesc_Label;
@property (nonatomic, strong) IBOutlet UIView *headerContainerView;
@end

//
//  Product.m
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize productImgLink;
@synthesize productName;
@synthesize productId;
@synthesize productDesciption;
@synthesize productOffer;
@synthesize productText;
@synthesize productImage;
@synthesize pin_type;
@synthesize termsAndConditions;
@synthesize contactMerchantName,contactSuburb,contactState,contactPostcode,contactlastName,contactFirstName,contactCountry;

@synthesize imageDoesntExist;

@synthesize displayImagePrefix;
@synthesize imageExtension;

@synthesize phone;
@synthesize mobile;
@synthesize websiteLink;

@synthesize productDetailedDesciption;
@synthesize productTermsAndConditions;

@synthesize coordinate;
@synthesize hotoffer_extension;


@synthesize merchantId;
@synthesize merchantLogoExtention;

@synthesize product_AvailableCount;
@synthesize product_CategoryId;
@synthesize product_HoursCountDown;
@synthesize product_StartDate;
@synthesize product_EndDate;
@synthesize product_FbStatus;
@synthesize product_FbUrl;
@synthesize product_TwitterStatus;
@synthesize product_TwitterUrl;
@synthesize product_EmailStatus;
@synthesize product_EmailAddress;
@synthesize product_PhoneNoStatus;
@synthesize product_PhoneNo1;
@synthesize product_PhoneNo2;
@synthesize product_BookNowStatus;
@synthesize product_BookNowText;
@synthesize product_WebsiteStatus;
@synthesize product_WebsiteUrl;
@synthesize product_CommentStatus;
@synthesize product_CommentText;
@synthesize product_ScoreStatus;
@synthesize product_ScoreText;
@synthesize product_ScanStatus;
@synthesize product_ScanText;
@synthesize product_RedeemStatus;
@synthesize product_RedeemText;
@synthesize product_Active;
@synthesize StoreId;
@synthesize categoryId;


- (id)init {
    self = [super init];
    
    if (self) {
        imageDoesntExist = NO;
    }
    
    return self;
}

@end



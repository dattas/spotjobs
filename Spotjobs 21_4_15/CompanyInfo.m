//
//  CompanyInfo.m
//  MyARSample
//
//  Created by vairat on 22/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "CompanyInfo.h"

@implementation CompanyInfo


@synthesize businessName;
@synthesize contactName;
@synthesize telphone;
@synthesize mobile;
@synthesize email;
@synthesize website;
@synthesize companyDesc;

@end

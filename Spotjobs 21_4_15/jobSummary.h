//
//  jobSummary.h
//  MyARSample
//
//  Created by vairat on 06/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface jobSummary : NSObject
{
    NSString *companyProfile;
    NSString *jobDescription;
    NSString *jobRequirements;
}
@property (nonatomic ,strong) NSString *companyProfile;
@property (nonatomic ,strong) NSString *jobDescription;
@property (nonatomic ,strong) NSString *jobRequirements;
@end

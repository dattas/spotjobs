//
//  ProductDataParser.m
//  GrabItNow
//
//  Created by MyRewards on 12/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductDataParser.h"

@interface ProductDataParser()
{
    NSMutableString *charString;
    Product *currProduct;
}

@end


@implementation ProductDataParser
@synthesize delegate;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    NSLog(@"parserDidStartDocument");
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    NSLog(@"didStartElement");
    if ([elementName isEqualToString:@"deal"]) {
    //if ([elementName isEqualToString:@"product"]) {
        currProduct = [[Product alloc] init];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    NSLog(@"foundCharacters");
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"details"]) {
        currProduct.productDesciption = finalString;
    }
    else if ([elementName isEqualToString:@"text"]) {
        currProduct.productText = finalString;
    }
    else if ([elementName isEqualToString:@"terms_and_conditions"]) {
        currProduct.productTermsAndConditions = finalString;
    }
    else if ([elementName isEqualToString:@"image_extension"]) {
        currProduct.imageExtension = finalString;
    }
    else if ([elementName isEqualToString:@"logo_extension"]) {
        currProduct.merchantLogoExtention = finalString;
    }
    else if ([elementName isEqualToString:@"display_image"]) {
        if ([finalString isEqualToString:@"Merchant Logo"]) {
            currProduct.displayImagePrefix = Image_URL_Prefix;
        }
        else {
            currProduct.displayImagePrefix = Product_Image_URL_Prefix;
        }
    }
    else if ([elementName isEqualToString:@"id"]) {
        currProduct.productId = finalString;
        
        // construct image link
        NSString *imgLink = [NSString stringWithFormat:@"%@",finalString];
        currProduct.productImgLink = imgLink;
        
    }
    else if ([elementName isEqualToString:@"merchant_id"]) {
        currProduct.merchantId = finalString;
    }
    else if ([elementName isEqualToString:@"hotoffer_extension"]) {
        currProduct.hotoffer_extension = finalString;
    }
    
    
    
    else if ([elementName isEqualToString:@"name"]) {
        currProduct.productName = finalString;
    }
    else if ([elementName isEqualToString:@"highlight"]) {
        currProduct.productOffer = finalString;
    }
    else if ([elementName isEqualToString:@"mname"]) {
        currProduct.contactMerchantName = finalString;
    }
    else if ([elementName isEqualToString:@"contact_first_name"]) {
        currProduct.contactFirstName = finalString;
    }
    else if ([elementName isEqualToString:@"contact_last_name"]) {
        currProduct.contactlastName = finalString;
    }
    else if ([elementName isEqualToString:@"mail_suburb"]) {
        currProduct.contactSuburb = finalString;
    }
    else if ([elementName isEqualToString:@"mail_state"]) {
        currProduct.contactState = finalString;
    }
    else if ([elementName isEqualToString:@"mail_postcode"]) {
        currProduct.contactPostcode = finalString;
    }
    else if ([elementName isEqualToString:@"mail_country"]) {
        currProduct.contactCountry = finalString;
    }
    else if ([elementName isEqualToString:@"phone"]) {
        currProduct.phone = finalString;
    }
    else if ([elementName isEqualToString:@"mobile"]) {
        currProduct.mobile = finalString;
    }
    else if ([elementName isEqualToString:@"link1"]) {
        currProduct.websiteLink= finalString;
    }
    else if ([elementName isEqualToString:@"count"]) {
        currProduct.product_AvailableCount = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"category_id"]) {
        currProduct.categoryId = finalString;
    }
    else if ([elementName isEqualToString:@"hours_countdown"]) {
        currProduct.product_HoursCountDown = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"start_date"]) {
        currProduct.product_StartDate = finalString;
    }
    else if ([elementName isEqualToString:@"end_date"]) {
        currProduct.product_EndDate = finalString;
    }
    else if ([elementName isEqualToString:@"offer_details"]) {
        currProduct.productDetailedDesciption = finalString;
    }
    else if ([elementName isEqualToString:@"terms_conditions"]) {
        currProduct.termsAndConditions = finalString;
    }
    else if ([elementName isEqualToString:@"stores"]) {
        currProduct.StoreId = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"facebook_status"]) {
        currProduct.product_FbStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"facebook_url"]) {
        currProduct.product_FbUrl = finalString;
    }
    else if ([elementName isEqualToString:@"twitter_status"]) {
        currProduct.product_TwitterStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"twitter_url"]) {
        currProduct.product_TwitterUrl = finalString;
    }
    else if ([elementName isEqualToString:@"email_status"]) {
        currProduct.product_EmailStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"email_address"]) {
        currProduct.product_EmailAddress = finalString;
    }
    else if ([elementName isEqualToString:@"phone_num_status"]) {
        currProduct.product_PhoneNoStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"phone_num1"]) {
        currProduct.product_PhoneNo1 = finalString;
    }
    else if ([elementName isEqualToString:@"phone_num2"]) {
        currProduct.product_PhoneNo2 = finalString;
    }
    else if ([elementName isEqualToString:@"phone_num1"]) {
        currProduct.product_PhoneNo1 = finalString;
    }
    else if ([elementName isEqualToString:@"booknow_status"]) {
        currProduct.product_BookNowStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"booknow_text"]) {
        currProduct.product_BookNowText = finalString;
    }
    else if ([elementName isEqualToString:@"website_status"]) {
        currProduct.product_WebsiteStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"website_url"]) {
        currProduct.product_WebsiteUrl = finalString;
    }
    else if ([elementName isEqualToString:@"comment_status"]) {
        currProduct.product_CommentStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"comments_text"]) {
        currProduct.product_CommentText = finalString;
    }
    else if ([elementName isEqualToString:@"score_status"]) {
        currProduct.product_ScoreStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"score_text"]) {
        currProduct.product_ScoreText = finalString;
    }
    else if ([elementName isEqualToString:@"scan_status"]) {
        currProduct.product_ScanStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"scan_text"]) {
        currProduct.product_ScanText = finalString;
    }
    else if ([elementName isEqualToString:@"redeem_status"]) {
        currProduct.product_RedeemStatus = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"redeem_text"]) {
        currProduct.product_RedeemText = finalString;
    }
    
    else if ([elementName isEqualToString:@"root"]) {
       
       /*
        // ** Calculating image name
        if ([currProduct.displayImagePrefix isEqualToString:Image_URL_Prefix]) {
            // construct merchant image link
            NSString *imgLink = [NSString stringWithFormat:@"%@",currProduct.merchantId];
            currProduct.productImgLink = imgLink;
            
            // ** Update Extension param
            currProduct.imageExtension = currProduct.merchantLogoExtention;
        }
        else {
            // construct product image link
            NSString *imgLink = [NSString stringWithFormat:@"%@",currProduct.productId];
            currProduct.productImgLink = imgLink;
        }
        
        // Update image link (prefix+image_name+.+extention )

        if (currProduct.displayImagePrefix && currProduct.productImgLink) {
            currProduct.productImgLink = [NSString stringWithFormat:@"%@%@.%@",currProduct.displayImagePrefix,currProduct.productImgLink,currProduct.imageExtension];
        }  */
        
        NSLog(@"Product image link: %@",currProduct.productImgLink);
        
        [parser abortParsing];
        [delegate parsingProductDataFinished:currProduct];
        
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
     NSLog(@"Error: %@", parseError);
    [delegate parsingProductDataXMLFailed];
}

@end

/*
 
 <mname>Metung Holiday Villas </mname>
 <contact_first_name>Garry</contact_first_name>
 <contact_last_name>Avage</contact_last_name>
 <contact_title>Mr</contact_title>
 <contact_position>Manager</contact_position>
 <mail_address1>Cnr Mairburn &amp; Stirling Road</mail_address1>
 <mail_address2></mail_address2>
 <mail_suburb>Metung</mail_suburb>
 <mail_state>VIC</mail_state>
 <mail_postcode>3904</mail_postcode>
 <mail_country>Australia</mail_country>
 <email></email>
 <phone>03 5156 2306</phone>
 <mobile></mobile>
 <fax></fax>
 <latitude>-37.8833330</latitude>
 <longitude>147.8500000</longitude>
 
 */



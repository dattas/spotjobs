//
//  JobDetails.h
//  MyARSample
//
//  Created by vairat on 06/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JobDetails : NSObject
{
    NSString *companyName;
    NSString *websiteaddress;
    NSString *eligiblity;
    NSString *experience;
    NSString *location;
    NSString *jobrole;

}

@property (nonatomic ,strong)NSString *companyName;
@property (nonatomic ,strong)NSString *websiteaddress;
@property (nonatomic ,strong)NSString *eligiblity;
@property (nonatomic ,strong)NSString *experience;
@property (nonatomic ,strong)NSString *location;
@property (nonatomic ,strong)NSString *jobrole;

@end

//
//  Education.m
//  MyARSample
//
//  Created by vairat on 15/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "Education.h"
#import "Resume.h"


@implementation Education

@dynamic collegeschool;
@dynamic degreecourse;
@dynamic perorcgpa;
@dynamic result;
@dynamic universityboard;
@dynamic yearofpassing;
@dynamic insertedTime;
@dynamic ownby;

@end

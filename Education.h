//
//  Education.h
//  MyARSample
//
//  Created by vairat on 15/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Resume;

@interface Education : NSManagedObject

@property (nonatomic, retain) NSString * collegeschool;
@property (nonatomic, retain) NSString * degreecourse;
@property (nonatomic, retain) NSString * perorcgpa;
@property (nonatomic, retain) NSString * result;
@property (nonatomic, retain) NSString * universityboard;
@property (nonatomic, retain) NSString * yearofpassing;
@property (nonatomic, retain) NSDate * insertedTime;
@property (nonatomic, retain) Resume *ownby;

@end

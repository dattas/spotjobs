//
//  EmployeeViewController.h
//  MyARSample
//
//  Created by vairat on 26/10/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostAJobViewController.h"
#import "FillComInfoViewController.h"

@interface EmployeeViewController : UIViewController<UITextFieldDelegate>{
    
    IBOutlet PostAJobViewController *postjobViewController;
	IBOutlet FillComInfoViewController *addressViewController;
	IBOutlet UIViewController *viewController;
    
}
@property(nonatomic, retain) PostAJobViewController *postjobViewController;
@property(nonatomic, retain) FillComInfoViewController *addressViewController;

@property (nonatomic, retain) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) IBOutlet UIView *menuView;


- (IBAction)submit_Tapped:(id)sender;
- (IBAction)postResume:(id)sender;
- (IBAction)PostaJob:(id)sender;
- (IBAction)bottomMenuButtonTapped:(id)sender;
@end

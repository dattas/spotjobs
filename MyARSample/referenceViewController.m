//
//  referenceViewController.m
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#define kOFFSET_FOR_KEYBOARD 135
#import "referenceViewController.h"
#import "AppDelegate.h"
#import "Reference.h"
#import "MediatorViewController.h"
@interface referenceViewController ()
{

    AppDelegate *appDelegate;
    Resume *currRes;
    
    NSMutableDictionary *ref_Dictionary;
}

@end

@implementation  referenceViewController

@synthesize currentReferenceObj;
@synthesize referenceScrollView;
@synthesize currentTextField;
@synthesize referencenameTextField;
@synthesize companyTextField;
@synthesize phoneTextField;
@synthesize emailTextField;
@synthesize managedObjectContext;
@synthesize referencedetails;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        currRes = resumeObj;
        referencedetails =[[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.title=@"References";
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"123 version is %0.2f",version);
    
    if (version>=5.00)
    {
        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
        // [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    }
    
	else
    {
        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    }
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    UIImage *myImage2 = [UIImage imageNamed:@"Savebutton.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(saveData) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    if(currentReferenceObj)
        [self loadReferenceDetails];
}
-(void)backaction
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}
-(void)viewWillAppear:(BOOL)animated{

    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
	[keyBoardController addToolbarToKeyboard];

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([appDelegate isIphone5]) {
        referenceScrollView.frame=CGRectMake(0,   0, 320, 500);
        [referenceScrollView setContentSize:CGSizeMake(320, 678)];
        
    }
    else
    {
        referenceScrollView.frame=CGRectMake(0,  0, 320, 460);
        [referenceScrollView setContentSize:CGSizeMake(320, 678)];
    }
    
[self performSelector:@selector(loadReferenceDetails) withObject:nil afterDelay:0.01];

}
-(void)saveData
{
        
    BOOL success = [self addReferenceinfo];
   
    if(success)
    {
        ref_Dictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SortDic"] mutableCopy];
        if(!ref_Dictionary){
            ref_Dictionary = [[NSMutableDictionary alloc]init];
        }
        NSMutableArray *arr = [[ref_Dictionary objectForKey:@"Reference"]mutableCopy];
        [arr addObject:self.companyTextField.text];
        [ref_Dictionary setValue:arr forKey:@"Reference"];
        
        [[NSUserDefaults standardUserDefaults] setValue:ref_Dictionary forKey:@"SortDic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Saved" message:@"data saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"data not  saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    MediatorViewController *mVc = [[MediatorViewController alloc]initWithNibName:@"MediatorViewController" bundle:Nil resume:currRes listType:@"reference"];
    mVc.managedObjectContext = self.managedObjectContext;
    [self.navigationController pushViewController:mVc animated:NO];
    
    
}


-(BOOL) addReferenceinfo
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if(currentReferenceObj){
        
        currentReferenceObj.referencename = self.referencenameTextField.text;
        currentReferenceObj.company       = self.companyTextField.text;
        currentReferenceObj.phone         = self.phoneTextField.text;
        currentReferenceObj.email         = self.emailTextField.text;
        currentReferenceObj.ownby         = currRes;
        
    }
    else{
    Reference *reference = (Reference *)[NSEntityDescription insertNewObjectForEntityForName:@"Reference" inManagedObjectContext:context];
    
      
      reference.referencename = self.referencenameTextField.text;
      reference.company       = self.companyTextField.text;
      reference.phone         = self.phoneTextField.text;
      reference.email         = self.emailTextField.text;
      reference.ownby         = currRes;
    }
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding Refernce data failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}



-(void)loadReferenceDetails
{
   /*
    NSSet *referenceSet  = currRes.reference;
    NSArray *referenceArray = [referenceSet allObjects];
    
    for (Reference *ref in referenceArray) {
        
        NSLog(@"referencename::%@",ref.referencename);
        NSLog(@"company::%@",ref.company);
        NSLog(@"phone::%@",ref.phone);
        NSLog(@"email::%@",ref.email);
        
        
    }*/
    self.referencenameTextField.text  = currentReferenceObj.referencename ;
    self.companyTextField.text        = currentReferenceObj.company;
    self.phoneTextField.text          = currentReferenceObj.phone ;
    self.emailTextField.text          = currentReferenceObj.email;
    

}
//=======METHOD TO ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN =========//

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        NSLog(@"INSIDE MOVEDUP AND SCROLL VIEW Y IS::%f",rect.origin.y);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD+20;
        
        
        
    }
    else
    {
        
        rect.origin.y += kOFFSET_FOR_KEYBOARD+20;
        
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    
    //[textField setInputAccessoryView:accessoryView];
    
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=3  )
        {
            
            [self setViewMovedUp:YES];
        }
    }
    else
    {
        
        if( textField.tag<=3   )
        {
            
            [self setViewMovedUp:YES];
        }
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=3  )
        {
            [self setViewMovedUp:NO];
            
        }
        
    } else
    {
        
        if( textField.tag<=3 )
        {
            
            [self setViewMovedUp:NO];
        }
        
    }
    
}
//================================================================================================//

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [currentTextField resignFirstResponder];
        return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  AppDelegate.h
//  MyARSample
//
//  Created by vairat on 26/06/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RevealController.h"
#import "Product.h"
#import "User.h"
#import "Personal.h"
#import "Project.h"
#import "Education.h"
#import "Experience.h"
//#import "OtherInfo.h"
#import "Reference.h"
#import "Resume.h"
#import "jobSummary.h"
#import "JobDetails.h"

@class HomeViewController;
@class SearchCriteriaViewController;
@class HomePageViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) HomeViewController *homeViewController;
@property (strong, nonatomic) SearchCriteriaViewController *searchCriteriaViewController;
@property (strong, nonatomic) HomePageViewController *homePageViewController;
@property (strong, nonatomic) NSMutableArray *productsList_Array;
@property (strong, nonatomic) NSMutableArray *categoryList_Array;
@property (strong, nonatomic) NSMutableArray *resumeObjects_Array;
@property (nonatomic, strong) User *sessionUser;
//@property (strong, nonatomic) Resume *currentResumeObj;
@property (nonatomic, strong) UIImage *signImage;
- (BOOL) isIphone5;
- (void) loginSuccessfulWithUserdetails:(User *) userDetails password:(NSString *) pWord;

// My Favorite methods
- (BOOL) addProductToFavorites:(Product *) product;
- (BOOL) removeProductFromfavorites:(Product *) product;
- (BOOL) productExistsInFavorites:(Product *) product;
- (NSArray *) myFavoriteProducts;




//Job Details
-(BOOL) addJobDetails:(JobDetails *) jobdetails;
-(NSArray *)jobdetails;

//Job Summary
-(BOOL) addjobSummary:(jobSummary *) jobsummary;
-(NSArray *)jobsummary;

//
-(NSMutableArray *)sorting:(NSMutableArray *)insertOrder_Array Source:(NSMutableArray *)source_Array listType:(int)type;




// methods to store resume data
-(NSManagedObjectContext *)getManagedObject;
// **** Core data properties
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end

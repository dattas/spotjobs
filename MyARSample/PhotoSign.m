//
//  PhotoSign.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "PhotoSign.h"
#import "Resume.h"


@implementation PhotoSign

@dynamic objective;
@dynamic photo;
@dynamic place;
@dynamic sign;
@dynamic ownby;

@end

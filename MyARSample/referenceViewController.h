//
//  referenceViewController.h
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//

#import "UIKeyboardViewController.h"
#import <UIKit/UIKit.h>
#import "Resume.h"
#import "Reference.h"
@interface referenceViewController : UIViewController<UIKeyboardViewControllerDelegate>
{
    UIKeyboardViewController *keyBoardController;
    NSMutableArray *referencedetails;

}
@property (nonatomic, retain)Reference *currentReferenceObj;

@property(strong, nonatomic)NSMutableArray *referencedetails;
@property (strong, nonatomic) IBOutlet UIScrollView *referenceScrollView;
@property (strong, nonatomic) IBOutlet UITextField *referencenameTextField;
@property (strong, nonatomic) IBOutlet UITextField *companyTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property(strong, nonatomic) UITextField *currentTextField;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj;
@end

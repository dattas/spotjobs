//
//  ListViewController.m
//  MyARSample
//
//  Created by vairat on 06/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "ListViewController.h"
#import "AppDelegate.h" 
#import "ResumeViewController.h"
#import "Resume.h"
#import "PhotoSign.h"
#import <CoreData/CoreData.h>
@interface ListViewController (){
    
    AppDelegate *appDelegate;
    NSMutableArray *resumes_Array;
    
}

@end

@implementation ListViewController
@synthesize myTableView;
@synthesize alertTextField;
@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = @"Resume List";
    
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(addButtonTapped) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 45,45);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    
    self.managedObjectContext = [appDelegate getManagedObject];
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self refreshTableContents];
    [self.myTableView reloadData];
}

-(void)refreshTableContents{
    
    if(resumes_Array)
    {
        [resumes_Array removeAllObjects];
    }
    resumes_Array = [[NSMutableArray alloc]initWithArray:[self getResumes]];
    
}



-(void)addButtonTapped{
    
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Enter Resume Title" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
    
    alertTextField = [[UITextField alloc] initWithFrame:CGRectMake(22.0, 45.0, 240.0, 25.0)];
    alertTextField.placeholder = @"Enter Title";
    alertTextField.font = [UIFont systemFontOfSize:14];
    alertTextField.borderStyle = UITextBorderStyleRoundedRect;
    [alertTextField setBackgroundColor:[UIColor whiteColor]];
    [myAlert addSubview:alertTextField];
    [myAlert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.cancelButtonIndex == buttonIndex) {}
    else
    {
        
        if([alertTextField.text length]>0){
            
            if([self addNewResume:alertTextField.text]){
                [self refreshTableContents];
                [self.myTableView reloadData];
                
            }
            else{
            
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Resume with this name already Exits" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [myAlert show];
            }
            
            
                
        }
        else{
            
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Resume should have valid Title" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [myAlert show];
            }
        
        
    }
    
}





#pragma mark -- UITableView Datasource & Delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [resumes_Array count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    Resume *resume = [resumes_Array objectAtIndex:indexPath.row];
    cell.textLabel.text = resume.title;
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.managedObjectContext deleteObject:[resumes_Array objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        else{
            NSLog(@"deleted sucessfully");
            [resumes_Array removeObjectAtIndex:indexPath.row];
            [self.myTableView reloadData];
        }
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Resume *resumeObj = [resumes_Array objectAtIndex:indexPath.row];
   
   ResumeViewController *resumeVC=[[ResumeViewController alloc]initWithNibName:@"ResumeViewController" bundle:nil resume:resumeObj];
    resumeVC.managedObjectContext = self.managedObjectContext;
      [self.navigationController pushViewController:resumeVC animated:YES];
    
    
    
    
}


//=========+++++++++++

-(BOOL) addNewResume:(NSString *)title{
    
    NSManagedObjectContext *context = self.managedObjectContext;
    
    if([self isResumeExits:title]){
        
        return NO;
        
    }
    
    
    NSManagedObject *newResume = [NSEntityDescription insertNewObjectForEntityForName:@"Resume" inManagedObjectContext:context];
    [newResume setValue:@"1" forKey:@"resumeId"];
    [newResume setValue:title forKey:@"title"];
    
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Adding Resume failed %@", [error localizedDescription]);
        return NO;
    }
    
    
    return YES;
}


-(BOOL) isResumeExits:(NSString *)name{
    
    
    NSManagedObjectContext *context = self.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *resDesc = [NSEntityDescription entityForName:@"Resume" inManagedObjectContext:context];
    [fetchRequest setEntity:resDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title==%@",name];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0) {
        return YES;
    }
    
    return NO;
    
}

-(NSArray *)getResumes{
    
    NSManagedObjectContext *context = self.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *resumeDesc = [NSEntityDescription entityForName:@"Resume" inManagedObjectContext:context];
    [fetchRequest setEntity:resumeDesc];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];

    for (Resume *resume in fetchedObjects)
    {
        
        NSLog(@"ID::%@",resume.resumeId);
        NSLog(@"Title::%@",resume.title);
        
        Personal *personalInfo = resume.personal;
        
        NSLog(@"=================PERSONAL INFO");
        NSLog(@"FullName::%@",personalInfo.fullName);
        NSLog(@"Gender::%@",personalInfo.gender);
        NSLog(@"DOB::%@",personalInfo.dob);
        NSLog(@"Address::%@",personalInfo.address);
        NSLog(@"Phone::%@",personalInfo.phNo);
        NSLog(@"Email::%@",personalInfo.emailId);
        
        
        NSSet *eduSet = resume.education;
        
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"insertedTime" ascending:YES]];
        
        NSArray *sortedArr = [[eduSet allObjects] sortedArrayUsingDescriptors:sortDescriptors];
        
        
        
       // NSArray *arr = [eduSet allObjects];
        
       // NSLog(@"=================EDUCATION");
        for (Education *edu in sortedArr) {
             NSLog(@"course::%@",edu.degreecourse);
            NSLog(@"collegeschool::%@",edu.collegeschool);
            NSLog(@"universityboard::%@",edu.universityboard);
            NSLog(@"perorcgpa::%@",edu.perorcgpa);
            NSLog(@"universityboard::%@",edu.universityboard);
            NSLog(@"Result::%@",edu.result);
            NSLog(@"yearofpassing::%@",edu.yearofpassing);
            NSLog(@"------------------------------");
        
        }
        NSLog(@"=================PROJECTS");
        NSSet *projectSet  = resume.projects;
        NSArray *projectsArray = [projectSet allObjects];
        
        for (Project *pro in projectsArray) {
            
            NSLog(@"projecttitle::%@",pro.projecttitle);
            NSLog(@"duration::%@",pro.duration);
            NSLog(@"role::%@",pro.role);
            NSLog(@"teamsize::%@",pro.teamsize);
            NSLog(@"expertise::%@",pro.expertise);
            NSLog(@"------------------------------");
        }
        NSLog(@"=================EXPERIENCE");
        NSSet *experienceSet  = resume.experience;
        NSArray *experienceArray = [experienceSet allObjects];
        
        for (Experience *exp in experienceArray) {
            
            NSLog(@"projecttitle::%@",exp.company);
            NSLog(@"duration::%@",exp.position);
            NSLog(@"role::%@",exp.period);
            NSLog(@"teamsize::%@",exp.location);
            NSLog(@"expertise::%@",exp.jobresponse);
            NSLog(@"------------------------------");
        }
        NSLog(@"=================REFERENCE");
        NSSet *referenceSet  = resume.reference;
        NSArray *referenceArray = [referenceSet allObjects];
        
        for (Reference *ref in referenceArray) {
            
            NSLog(@"referencename::%@",ref.referencename);
            NSLog(@"company::%@",ref.company);
            NSLog(@"phone::%@",ref.phone);
            NSLog(@"email::%@",ref.email);
            NSLog(@"------------------------------");
            
        }

        NSLog(@"=================OTHER INFO");
        Others *otherInfo = resume.other;
        
        NSLog(@"drivingLicence::%@",otherInfo.drivingLicence);
        NSLog(@"panNo::%@",otherInfo.panNo);
        NSLog(@"passportNo::%@",otherInfo.passportNo);

        PhotoSign *photosign = resume.photosign;
        
        NSLog(@"place::%@",photosign.place);
        NSLog(@"objective::%@",photosign.objective);
        
        
        NSLog(@"=============================================");
        
    }

    
    
    
    
    
    return fetchedObjects;
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSLog(@"willShowViewController");
    [self viewWillAppear:animated];
}




//==========================+++++++++++















- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

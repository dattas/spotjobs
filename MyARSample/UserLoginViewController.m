//
//  UserLoginViewController.m
//  MyARSample
//
//  Created by vairat on 19/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "UserLoginViewController.h"
#import "UserSignUpViewController.h"
#import "UserViewController.h"
#import "EmployeeViewController.h"
#import "JSON.h"
#import "JobDetailViewController.h"
#import "FillComInfoViewController.h"

#define Status_XML_tag @"status"
#define Status_success_message @"SUCCESS"
#define Status_failure_message @"FAILURE"
@interface UserLoginViewController (){
    
     NSString *userType;
     NSURLConnection *loginRequest;
    
     NSURLConnection *fbConnection;
     NSXMLParser *parser;
     UserDataXMLParser *parserDelegate;

}

@end

@implementation UserLoginViewController
@synthesize userName_Textfield;
@synthesize pwd_Textfield;
@synthesize checkAvailability_Button;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil userType:(NSString *)user
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        userType = user;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([userType isEqualToString:@"jobSeeker"])
        self.title = @"User Login";
    else
        self.title = @"Employee Login";
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
}


-(void)backaction
{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                    completion:NULL];
    
}

- (IBAction)signIn_Action:(id)sender {
    
      NSString *type = @"";
       if([userType isEqualToString:@"jobSeeker"])
           type = @"member";
       else
           type = @"employer";
    
        
        NSString *uName = self.userName_Textfield.text;
        NSString *pWord = self.pwd_Textfield.text;
        
        NSLog(@"%@ %@",uName,pWord);
        
        
        
        NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://184.107.152.53/newapp/spotjobs/login.php?username=%@&password=%@&type=%@",uName,pWord,type]];
        NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
        
        
        NSLog(@"=======>>%@",aUrl);
        
        loginRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
        
        

 
   
    
    
}

//=====================------------------ PARSING METHODS --------------=============================//
#pragma mark -- NSXMLParser Delegate methods

NSMutableString *parserEleString;

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    //if ([elementName isEqualToString:@"status"]) {
    parserEleString = [[NSMutableString alloc] init];
    NSLog(@"%@",parserEleString);
    
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [parserEleString appendString:string];
    
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:Status_XML_tag]) {
        
        if ([parserEleString isEqualToString:Status_success_message])
        {
            
            NSLog(@"Prashanth");
            
        }
        else
        {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Please provide valid details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
        }
        
    }
    else{
        NSLog(@"elementName::%@",elementName);
    }
    
    
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parserLocal {
    parser.delegate = nil;
    parser = nil;
}

- (void)parser:(NSXMLParser *)parserLocal parseErrorOccurred:(NSError *)parseError {
    parser.delegate = nil;
    parser = nil;
}


- (IBAction)signUp_Action:(id)sender {
 
    
    if([userType isEqualToString:@"jobSeeker"])
    {
        UserSignUpViewController *userSignVC = [[UserSignUpViewController alloc]initWithNibName:@"UserSignUpViewController" bundle:Nil userType:@"jobSeeker"];
       [self presentNewViewController:userSignVC];
    }
    else{
        
        UserSignUpViewController *userSignVC = [[UserSignUpViewController alloc]initWithNibName:@"UserSignUpViewController" bundle:Nil userType:@"jobPoster"];
        [self presentNewViewController:userSignVC];
        
       //  EmployeeViewController *employee = [[EmployeeViewController alloc]initWithNibName:@"EmployeeViewController" bundle:Nil];
        // [self presentNewViewController:employee];
    }
    
  
}

- (IBAction)loginWithFB_Action:(id)sender {
    
    [self performSelector:@selector(publishPostWithDelegate:) withObject:nil afterDelay:0.2];
}

#pragma mark facebook start
//---------------------------------------------------------
- (void) publishPostWithDelegate:(id) _delegate
{
    
	//store the delegate incase the user needs to login
	//self.delegate = _delegate;
    
	BOOL loggedIn = [[FBRequestWrapper defaultManager] isLoggedIn];
    
	if (!loggedIn) {
        
        NSLog(@"Logged In");
        //isFbLoggedIn=NO;
		[[FBRequestWrapper defaultManager] FBSessionBegin:self];
        
	}
	else {
        
        NSLog(@"Logged Out");
        //isFbLoggedIn=YES;
        [self performSelector:@selector(getFacebookProfile)];
        
	}
    //[self performSelector:@selector(getFacebookProfile)];
}

- (void)getFacebookProfile {
    
    NSLog(@"facebook.accessToken = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"]);
    NSString *urlString = [NSString
                           stringWithFormat:@"https://graph.facebook.com/me?access_token=%@",
                           [[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *url = [NSURL URLWithString:urlString];

    
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    fbConnection = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    //[fbConnection setDidFinishSelector:@selector(getFacebookProfileFinished:)];
    //[fbConnection ]
    
}

- (void)fbDidLogin {
    
    
    
  	//after the user is logged in try to publish the post
    
    //https://graph.facebook.com/100000838671085/picture/
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"facebook_user"])
    {
        [self performSelector:@selector(getFacebookProfile)];
        
        
    }
    else
    {
        
    }
    
}
//@"access_token"
//@"exp_date"

-(void)fbDidExtendToken:(NSString *)accessTokens expiresAt:(NSDate *)expiresAt {
    NSLog(@"token extended");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accessTokens forKey:@"access_token"];
    [defaults setObject:expiresAt forKey:@"exp_date"];
    
    NSLog(@"access_token  -----> %@",[defaults objectForKey:@"access_token"]);
    NSLog(@"exp_date  -----> %@",[defaults objectForKey:@"exp_date"]);
    
    [defaults synchronize];
}




- (void)request:(FBRequest *)request didLoad:(id)result
{
    NSLog(@"ViewController's request:didLoad: method executing %@",result);
    
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"[appDelegate request:(FBRequest *) didFailWithError:]");
}

- (void)fbDidNotLogin:(BOOL)cancelled
{
    
    NSLog(@"fbDidNotLogin/failedToPublishPost");
    //    [spinner stopAnimating];
    //
    //    if (fbPost) {
    //        fbPost.delegate = nil;
    //        fbPost = nil;
    //    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)fbDidLogout
{
    [[FBRequestWrapper defaultManager] setIsLoggedIn:NO];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // [facebook invalidateSession];
    //[defaults removeObjectForKey:@"FBAccessTokenKey"];
    //[defaults removeObjectForKey:@"FBExpirationDateKey"];
    [defaults removeObjectForKey:@"access_token"];
    [defaults removeObjectForKey:@"facebook_user"];
    [defaults removeObjectForKey:@"facebook_userid"];
    [defaults removeObjectForKey:@"exp_date"];
    
    NSLog(@"access_token/fbdidlogout is %@",[defaults objectForKey:@"access_token"]);
    NSLog(@"facebook_user/fbdidlogout is %@",[defaults objectForKey:@"facebook_user"]);
    NSLog(@"facebook_userid/fbdidlogout is %@",[defaults objectForKey:@"facebook_userid"]);
    NSLog(@"exp_date/fbdidlogout is %@",[defaults objectForKey:@"exp_date"]);
    
    [defaults synchronize];
    
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSLog(@"didReceiveResponse");
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
    
    if(connection == fbConnection)
    {
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"loginRequest...%@",responseString);
        
        NSDictionary *detailsDict = [[NSDictionary alloc]init];
        detailsDict = [responseString JSONValue];
        
        for(NSString *key in [detailsDict allKeys]) {
            NSLog(@"%@",[detailsDict objectForKey:key]);
        }
        
        
        NSLog(@"================================================================");
        NSString *name = [detailsDict objectForKey:@"first_name"];
        NSLog(@"name::%@",name);
        NSString *gender = [detailsDict objectForKey:@"gender"];
        NSLog(@"gender::%@",gender);
        
        NSArray * arr = [detailsDict objectForKey:@"languages"];
        
        NSDictionary *dict = [[NSDictionary alloc]init];
        dict = [arr objectAtIndex:0];
        NSString *lan1 = [dict objectForKey:@"name"];
        dict = [arr objectAtIndex:1];
        NSString *lan2 = [dict objectForKey:@"name"];
        NSLog(@"languages::%@,%@",lan1,lan2);
        
        
        dict = [detailsDict objectForKey:@"location"];
        NSString *location = [dict objectForKey:@"name"];
        NSLog(@"location::%@",location);
        
        arr = [detailsDict objectForKey:@"work"];
        // NSLog(@"arr count::%d",[arr count]);
        dict = [arr objectAtIndex:0];
        
        //    for(NSString *key in [dict allKeys]) {
        //        NSLog(@"%@",[dict objectForKey:key]);
        //    }
        NSArray *arr1 = [[NSArray alloc]init];
        arr1 = [dict allValues];
        //NSLog(@"arr1 count::%d",[arr1 count]);
        
        
        [dict setValue:[arr1 objectAtIndex:0] forKey:@"employer"];
        [dict setValue:[arr1 objectAtIndex:2] forKey:@"position"];
        
        NSDictionary *dict1 = [dict objectForKey:@"employer"];
        NSString *emplyee = [dict1 objectForKey:@"name"];
        NSLog(@"Company::%@",emplyee);
        dict1 = [dict objectForKey:@"position"];
        NSString *position = [dict1 objectForKey:@"name"];
        NSLog(@"Designation::%@",position);
    }
    
    
    
    
    else{
        
            NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
            NSLog(@"loginRequest...%@",responseString);
            
//            NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
//            xmlParser.delegate =self;
//            [xmlParser parse];
//            parser = xmlParser;
        
        NSXMLParser *userDetailsParser = [[NSXMLParser alloc] initWithData:_responseData];
        parserDelegate = [[UserDataXMLParser alloc] init];
        parserDelegate.delegate = self;
        userDetailsParser.delegate = parserDelegate;
        [userDetailsParser parse];
            
        

    }
    
    
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConnection");
    
}
//=====================------------------ PARSING METHODS --------------=============================//


- (void)parsingUserDetailsFinished:(User *)userDetails {
    
    if([userDetails.result isEqualToString:Status_success_message]){
        
        [[NSUserDefaults standardUserDefaults] setValue:userDetails.userId forKey:@"USERID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"USERID::::%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"]);
        
         if([userType isEqualToString:@"jobSeeker"])
         {
           // UserViewController *userVC = [[UserViewController alloc]initWithNibName:@"UserViewController" bundle:Nil];
           // [self presentNewViewController:userVC];
             
             EmployeeViewController *employee = [[EmployeeViewController alloc]initWithNibName:@"EmployeeViewController" bundle:Nil];
              [self presentNewViewController:employee];
         }
         else
         {
        
          // EmployeeViewController *employee = [[EmployeeViewController alloc]initWithNibName:@"EmployeeViewController" bundle:Nil];
          // [self presentNewViewController:employee];
            FillComInfoViewController *addressVC =  [[FillComInfoViewController alloc]initWithNibName:@"FillComInfoViewController" bundle:nil];
             [self.navigationController pushViewController:addressVC animated:YES];
         }
        
        
    }
    else{
        UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Please provide valid details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [failureAlert show];

        }
    
}

//=====================------------------END PARSING METHODS --------------=============================//

-(void)presentNewViewController:(UIViewController *) controller
{
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController pushViewController:controller animated:NO];}
     completion:NULL];
    
}







-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
   
    
   
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setCheckAvailability_Button:nil];
    [super viewDidUnload];
}
@end

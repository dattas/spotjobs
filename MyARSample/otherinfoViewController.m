//
//  otherinfoViewController.m
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//
#define kOFFSET_FOR_KEYBOARD 135
#import "otherinfoViewController.h"
#import "AppDelegate.h"
#import "Others.h"
@interface otherinfoViewController ()
{
    AppDelegate *appDelegate;
    Resume *currRes;
}

@end

@implementation otherinfoViewController
@synthesize otherinfoScrollView;
@synthesize currentTextField;
@synthesize drivinglicencenumberTextField;
@synthesize pannumberTextField;
@synthesize passportnumberTextField;
@synthesize otherinfodetails;
@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currRes = resumeObj;
        otherinfodetails=[[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.title=@"Other Information";
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"123 version is %0.2f",version);
    
    if (version>=5.00)
    {
        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
        // [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    }
    
	else
    {
        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    }
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    UIImage *myImage2 = [UIImage imageNamed:@"Savebutton.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(saveData) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{

    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
	[keyBoardController addToolbarToKeyboard];

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([appDelegate isIphone5]) {
        otherinfoScrollView.frame=CGRectMake(0,   0, 320, 500);
        [otherinfoScrollView setContentSize:CGSizeMake(320, 678)];
        
    }
    else
    {
        otherinfoScrollView.frame=CGRectMake(0,  0, 320, 460);
        [otherinfoScrollView setContentSize:CGSizeMake(320, 678)];
    }
   [self performSelector:@selector(loadOtherInfo) withObject:nil afterDelay:0.01];
    
}
-(void)backaction
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}
-(void)saveData
{
        
    BOOL success = [self addOtherInfo];
    
    if(success)
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Saved" message:@"data saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"data not  saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

    
}


-(BOOL) addOtherInfo
{
    NSManagedObjectContext *context = self.managedObjectContext;
    
    if(currRes.other){
        currRes.other.drivingLicence = self.drivinglicencenumberTextField.text;
        currRes.other.passportNo     = self.passportnumberTextField.text;
        currRes.other.panNo          = self.pannumberTextField.text;
        currRes.other.ownby          = currRes;
        
    }
    else{
    Others *otherInfo = (Others *)[NSEntityDescription insertNewObjectForEntityForName:@"Others" inManagedObjectContext:context];
    
    otherInfo.drivingLicence = self.drivinglicencenumberTextField.text;
    otherInfo.passportNo     = self.passportnumberTextField.text;
    otherInfo.panNo          = self.pannumberTextField.text;
    otherInfo.ownby          = currRes;
    }
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding OtherInfo failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
    
}

-(void) loadOtherInfo
{
    /*
    Others *otherInfo = currRes.other;
   
        NSLog(@"drivingLicence::%@",otherInfo.drivingLicence);
        NSLog(@"panNo::%@",otherInfo.panNo);
        NSLog(@"passportNo::%@",otherInfo.passportNo);  */
    
    
    Others *otherInfo = currRes.other;
    if(otherInfo)
    {
        self.drivinglicencenumberTextField.text = otherInfo.drivingLicence ;
        self.pannumberTextField.text            = otherInfo.panNo;
        self.passportnumberTextField.text       = otherInfo.passportNo;
    }
    
}
//=======METHOD TO ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN =========//

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        NSLog(@"INSIDE MOVEDUP AND SCROLL VIEW Y IS::%f",rect.origin.y);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD+20;
        
        
        
    }
    else
    {
        
        rect.origin.y += kOFFSET_FOR_KEYBOARD+20;
        
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    
    //[textField setInputAccessoryView:accessoryView];
    
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=3  )
        {
            
            [self setViewMovedUp:YES];
        }
    }
    else
    {
        
        if( textField.tag<=3   )
        {
            
            [self setViewMovedUp:YES];
        }
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=3  )
        {
            [self setViewMovedUp:NO];
            
        }
        
    } else
    {
        
        if( textField.tag<=3 )
        {
            
            [self setViewMovedUp:NO];
        }
        
    }
    
}
//================================================================================================//

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [currentTextField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  MediatorViewController.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "MediatorViewController.h"
#import "AppDelegate.h"
#import "Education.h"
#import "Project.h"
#import "Experience.h"
#import "Reference.h"

#import "educationViewController.h"
#import "projectViewController.h"
#import "experienceViewController.h"
#import "referenceViewController.h"


@interface MediatorViewController (){
    
    NSString *loadingListType;
    NSMutableArray *content_Array;
    NSMutableDictionary *content_Dictionary;
    Resume *currRes;
    int listTypeNumber;
    
    AppDelegate *appDelegate;
    
    
    
}
@property(nonatomic, strong)NSMutableArray *content_Array;
@property(nonatomic, strong)NSMutableDictionary *content_Dictionary;
@end

@implementation MediatorViewController
@synthesize myTableView;
@synthesize content_Array;
@synthesize content_Dictionary;
@synthesize managedObjectContext;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj listType:(NSString *)type
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        loadingListType = type;
        currRes = resumeObj;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"viewDidLoad");
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(addButtonTapped) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 45,45);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	//self.navigationItem.rightBarButtonItem = rightButton;
    
    UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[editButton setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    [editButton setTintColor:[UIColor blackColor]];
    [editButton addTarget:self action:@selector(editButtonTapped) forControlEvents:UIControlEventTouchUpInside];
	editButton.showsTouchWhenHighlighted = YES;
    editButton.frame = CGRectMake(30.0, 30.0, 45,45);
    UIBarButtonItem *rightButton2 = [[UIBarButtonItem alloc] initWithCustomView:editButton];
	self.navigationItem.rightBarButtonItems = @[rightButton,rightButton2];

    
    
    
    
    NSString *titleStr = @"";
    
    if([loadingListType isEqualToString:@"education"])
    {
        NSSet *eduSet  = currRes.education;
        NSArray *eduArray = [eduSet allObjects];
        self.content_Array = [[NSMutableArray alloc]initWithArray:eduArray];
        listTypeNumber = 1;
        titleStr = @"EducationList";
    }
    else if([loadingListType isEqualToString:@"project"])
    {
        NSSet *projectSet  = currRes.projects;
        NSArray *projectsArray = [projectSet allObjects];
        self.content_Array = [[NSMutableArray alloc]initWithArray:projectsArray];
        listTypeNumber = 2;
        titleStr = @"ProjectList";
    }
    else if([loadingListType isEqualToString:@"experience"])
    {
        
        NSSet *experienceSet  = currRes.experience;
        NSArray *experienceArray = [experienceSet allObjects];
        self.content_Array = [[NSMutableArray alloc]initWithArray:experienceArray];
        listTypeNumber = 3;
        titleStr = @"ExperienceList";
    }
    else{   //reference
        
        NSSet *referenceSet  = currRes.reference;
        NSArray *referenceArray = [referenceSet allObjects];
        self.content_Array = [[NSMutableArray alloc]initWithArray:referenceArray];
        listTypeNumber = 4;
        titleStr = @"ReferenceList";
    }
   
    self.title = titleStr;  
    
    self.content_Dictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SortDic"] mutableCopy];
    if(!self.content_Dictionary)
        self.content_Dictionary = [[NSMutableDictionary alloc]init];
    else
        [self sortContentsInStoredOrder:listTypeNumber];
    
    
}

-(void)backaction
{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:4] animated:NO];
                    }
                    completion:NULL];
    
}

-(void)addButtonTapped{
    
    switch (listTypeNumber)
    {
        case 1:{
            educationViewController *eduVc = [[educationViewController alloc]initWithNibName:@"educationViewController" bundle:nil resume:currRes];
            eduVc.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:eduVc animated:YES];
        }
            break;
        case 2:{
            projectViewController *proVc = [[projectViewController alloc]initWithNibName:@"projectViewController" bundle:nil resume:currRes];
            proVc.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:proVc animated:YES];
        }
            break;
        case 3:{
            experienceViewController *expVc = [[experienceViewController alloc]initWithNibName:@"experienceViewController" bundle:nil resume:currRes];
            expVc.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:expVc animated:YES];
        }
            break;
        case 4:{
            referenceViewController *refVc = [[referenceViewController alloc]initWithNibName:@"referenceViewController" bundle:nil resume:currRes];
            refVc.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:refVc animated:YES];
        }
            break;
        default:
            break;
    }

    
    
}


-(void)editButtonTapped{
    if(self.editing)
	{
        
		[super setEditing:NO animated:NO];
		[myTableView setEditing:NO animated:NO];
		[myTableView reloadData];
		[self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
        
        switch (listTypeNumber)
        {
            case 1:{
                     NSMutableArray *arr = [[NSMutableArray alloc]init];
                    for (Education *edu in self.content_Array)
                         [arr addObject:edu.degreecourse];
                
                    [self.content_Dictionary setValue:arr forKey:@"Education"];
                   }
                   break;
            case 2:{
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    for (Project *pro in self.content_Array)
                         [arr addObject:pro.projecttitle];
                
                    [self.content_Dictionary setValue:arr forKey:@"Project"];
                   }
                   break;
            case 3:{
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    for (Experience *exp in self.content_Array)
                       [arr addObject:exp.company];
                
                    [self.content_Dictionary setValue:arr forKey:@"Experience"];
                   }
                  break;
            case 4:{
                     NSMutableArray *arr = [[NSMutableArray alloc]init];
                     for (Reference *ref in self.content_Array)
                          [arr addObject:ref.referencename];
                
                      [self.content_Dictionary setValue:arr forKey:@"Reference"];
                   }
                  break;
            default:
                  break;
        }

        [[NSUserDefaults standardUserDefaults] setValue:self.content_Dictionary forKey:@"SortDic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
	}
	else
	{
        
		[super setEditing:YES animated:YES];
		[myTableView setEditing:YES animated:YES];
		[myTableView reloadData];
		[self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStyleDone];
        
    }




}

-(void)sortContentsInStoredOrder:(int)listType{
    
    switch (listType)
    {
        case 1:
                   
            
            self.content_Array = [appDelegate sorting:[self.content_Dictionary objectForKey:@"Education"] Source:self.content_Array listType:1];
            break;
        case 2:
            self.content_Array = [appDelegate sorting:[self.content_Dictionary objectForKey:@"Project"] Source:self.content_Array listType:2];
            break;
        case 3:
            self.content_Array = [appDelegate sorting:[self.content_Dictionary objectForKey:@"Experience"] Source:self.content_Array listType:3];
            break;
        case 4:
            self.content_Array = [appDelegate sorting:[self.content_Dictionary objectForKey:@"Reference"] Source:self.content_Array listType:4];
            break;
        default:
            break;
    }

    
}

-(void)sorting:(NSMutableArray *)tempArr{
    
    NSLog(@"===BEFORE SORTING  CONTENT ARRAY");
    for (Education *e in self.content_Array) {
        NSLog(@"%@",e.degreecourse);
    }
    
    NSLog(@"====SORTING TEMP ARRAY");
    for (NSString *str in tempArr) {
        NSLog(@"%@",str);
    }
    
    for(int i =0;i<[tempArr count];i++){
        
        for (int j=i;j<[self.content_Array count]; j++) {
            
            Education *edu = [self.content_Array objectAtIndex:j];
            
            if([[tempArr objectAtIndex:i]isEqualToString:edu.degreecourse])
            {
                [self.content_Array removeObject:edu];
                [self.content_Array insertObject:edu atIndex:i];
                
            }
            
            
        }
    }
    NSLog(@"AFTER SORTING CONTENT ARRAY");
    for (Education *e in self.content_Array) {
        NSLog(@"%@",e.degreecourse);
    }
    
    
}


#pragma mark -- UITableView Datasource & Delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [self.content_Array count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
        
    }
    
    switch (listTypeNumber)
    {
        case 1:{
               Education *edu = [self.content_Array objectAtIndex:indexPath.row];
               cell.textLabel.text = edu.degreecourse;
               }
               break;
        case 2:{
                Project *pro = [self.content_Array objectAtIndex:indexPath.row];
                cell.textLabel.text = pro.projecttitle;
               }
            break;
        case 3:{
                Experience *exp = [self.content_Array objectAtIndex:indexPath.row];
                cell.textLabel.text = exp.company;
               }
            break;
        case 4:{
               Reference *ref = [self.content_Array objectAtIndex:indexPath.row];
               cell.textLabel.text = ref.referencename;
              }
                break;
        default:
            break;
    }
    
    cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrow.png"]];
    return cell;
}



- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.managedObjectContext deleteObject:[self.content_Array objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        else{
            NSLog(@"deleted sucessfully");
            [self.content_Array removeObjectAtIndex:indexPath.row];
            [self.myTableView reloadData];
        }
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    switch (listTypeNumber)
    {
        case 1:{
            educationViewController *eduVc = [[educationViewController alloc]initWithNibName:@"educationViewController" bundle:nil resume:currRes];
            eduVc.managedObjectContext = self.managedObjectContext;
            eduVc.currentEducationObj = [self.content_Array objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:eduVc animated:YES];
               }
            break;
        case 2:{
            projectViewController *proVc = [[projectViewController alloc]initWithNibName:@"projectViewController" bundle:nil resume:currRes];
            proVc.managedObjectContext = self.managedObjectContext;
            proVc.currentProjectObj = [self.content_Array objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:proVc animated:YES];
            }
            break;
        case 3:{
        experienceViewController *expVc = [[experienceViewController alloc]initWithNibName:@"experienceViewController" bundle:nil resume:currRes];
            expVc.managedObjectContext = self.managedObjectContext;
            expVc.currentExperienceObj = [self.content_Array objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:expVc animated:YES];
        }
            break;
        case 4:{
            referenceViewController *refVc = [[referenceViewController alloc]initWithNibName:@"referenceViewController" bundle:nil resume:currRes];
            refVc.managedObjectContext = self.managedObjectContext;
            refVc.currentReferenceObj = [self.content_Array objectAtIndex:indexPath.row];
              [self.navigationController pushViewController:refVc animated:YES];
        }
             break;
        default:
            break;
    }
    
}

#pragma mark Row reordering
// Determine whether a given row is eligible for reordering or not.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
// Process the row move. This means updating the data model to correct the item indices.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath
	  toIndexPath:(NSIndexPath *)toIndexPath {
    
    NSLog(@"listTypeNumber::%d",listTypeNumber);
    
    switch (listTypeNumber)
    {
        case 1:{
	           Education *item = [self.content_Array objectAtIndex:fromIndexPath.row];
	           [self.content_Array removeObject:item];
               [self.content_Array insertObject:item atIndex:toIndexPath.row];}
               break;
            
        case 2:{
               Project *item = [self.content_Array objectAtIndex:fromIndexPath.row];
               [self.content_Array removeObject:item];
               [self.content_Array insertObject:item atIndex:toIndexPath.row];}
               break;
        case 3:{
               Experience *item = [self.content_Array objectAtIndex:fromIndexPath.row];
               [self.content_Array removeObject:item];
               [self.content_Array insertObject:item atIndex:toIndexPath.row];}
               break;
        case 4:{
               Reference *item = [self.content_Array objectAtIndex:fromIndexPath.row];
               [self.content_Array removeObject:item];
               [self.content_Array insertObject:item atIndex:toIndexPath.row];}
               break;
        default:
            break;
	
    }
    

}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

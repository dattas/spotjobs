//
//  signViewController.m
//  MyARSample
//
//  Created by vairat on 02/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "SignViewController.h"
#import <OpenGLES/EAGL.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>
//#import "SecondViewController.h"
#import "AppDelegate.h"

@interface SignViewController ()
{
 BOOL createSnapshot;
AppDelegate *appDelegate;
}

@end

@implementation SignViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     createSnapshot = YES;
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.navigationItem.title=@"Signature";
    
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"123 version is %0.2f",version);
    
    if (version>=5.00)
    {
        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
        // [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    }
    
	else
    {
        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    }

    UIImage *myImage2 = [UIImage imageNamed:@"Use.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(useButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    
    [super viewDidUnload];
}
- (IBAction)useButtonTapped:(id)sender {
    CGRect rect = [self.view bounds];   //use your signature view's Rect means Frame;
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.view.layer renderInContext:context];   //this line is also important for you
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
   // UIImage *imgFinal = [self cropImage:img rect:CGRectMake(0,100,150,150)];
    
    appDelegate.signImage=[self cropImage:img rect:CGRectMake(0,100,320,250)];
    
    [self dismissModalViewControllerAnimated:YES];
    
}

-(UIImage *)cropImage:(UIImage *)image rect:(CGRect)cropRect {
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return img;
}

@end

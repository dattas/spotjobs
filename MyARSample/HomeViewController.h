//
//  HomeViewController.h
//  MyARSample
//
//  Created by vairat on 22/08/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKMapview.h>
#import <CoreLocation/CoreLocation.h>
#import "JobDetailViewController.h"
#import "ProductDataParser.h"

@interface HomeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,MKMapViewDelegate, CLLocationManagerDelegate,ProductXMLParserDelegate, JobDetailViewDelegate> {
     NSMutableData *_responseData;
}
@property(nonatomic, retain)IBOutlet UIView *map_View;
@property(nonatomic, retain)IBOutlet UIView *listView;
@property(nonatomic, retain)IBOutlet UITableView *tblView;

@property(nonatomic, retain)IBOutlet UIView *test;

@property(nonatomic,strong)IBOutlet MKMapView *mkView;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, readwrite) CLLocationCoordinate2D currentLocation;

@property(nonatomic,strong)IBOutlet UIView *bottom_Menu;
- (IBAction)mapButton_Tapped:(id)sender;
- (IBAction)arButton_Tapped:(id)sender;
- (IBAction)listButton_Tapped:(id)sender;

- (IBAction)bottomMenuButton_Tapped:(id)sender;
@end

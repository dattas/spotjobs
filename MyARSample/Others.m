//
//  Others.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "Others.h"
#import "Resume.h"


@implementation Others

@dynamic drivingLicence;
@dynamic panNo;
@dynamic passportNo;
@dynamic ownby;

@end

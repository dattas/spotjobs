//
//  personalinfoViewController.m
//  ResumeBuilder
//
//  Created by vairat on 25/10/13.
//  Copyright (c) 2013 Vairat. All rights reserved.
//

#define kOFFSET_FOR_KEYBOARD 135

#import "personalinfoViewController.h"
#import "AppDelegate.h"
#import "Personal.h"
#import <CoreData/CoreData.h>
@interface personalinfoViewController ()
{
    AppDelegate *appDelegate;
     NSString *textView_Text;
     UILabel *mylabel;
    
     UIDatePicker *datePicker;
     UIActionSheet *asheet;
     NSString *currentResumeTitle;
     Resume *currRes;
    
}
@property(nonatomic,strong) NSString *textView_Text;
@property(nonatomic,retain) UIDatePicker *datePicker;
@property(nonatomic,strong)UIActionSheet *asheet;
@property(nonatomic,strong)UILabel *mylabel;
@end

@implementation personalinfoViewController

@synthesize personaldetails;
@synthesize personalinfoScrollView;
@synthesize textView_Text;

@synthesize fullnameTextField;
@synthesize languagesKnownTextField;
@synthesize phonenumberTextField;
@synthesize emailIdTextField;
@synthesize currentTextField;

@synthesize addressTextView;
@synthesize accessoryView;

@synthesize mylabel;
@synthesize datelabel;
@synthesize genderLabel;

@synthesize Male_Button;
@synthesize FemaleButton;

@synthesize datePicker;
@synthesize asheet;
@synthesize managedObjectContext;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
         currRes = resumeObj;
         personaldetails = [[NSMutableArray alloc] init];
         currentResumeTitle = resumeObj.title;
    }
    return self;
}

- (void)viewDidLoad
{  appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [super viewDidLoad];
    self.navigationItem.title=@"Personal Info";
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"123 version is %0.2f",version);
   
    if (version>=5.00)
    {
         self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
       // [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    }
    
	else
    {
        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    }
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    UIImage *myImage2 = [UIImage imageNamed:@"Savebutton.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(saveData) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    

    if ([appDelegate isIphone5]) {
     
        mylabel = [[UILabel alloc]initWithFrame:CGRectMake(14, -85, 280, 250)];
       // mylabel.text = @"Enter Valid Address.";
        mylabel.backgroundColor = [UIColor clearColor];
        mylabel.font = [UIFont systemFontOfSize:14];
        mylabel.numberOfLines=4;
        mylabel.textColor = [UIColor lightGrayColor];
        [self.addressTextView addSubview:mylabel];
    }
    else
    {
        mylabel = [[UILabel alloc]initWithFrame:CGRectMake(14, -80, 280, 250)];
       // mylabel.text = @"Enter Valid Address.";
        mylabel.backgroundColor = [UIColor clearColor];
        mylabel.numberOfLines=4;
        mylabel.font = [UIFont systemFontOfSize:14];
        mylabel.textColor = [UIColor lightGrayColor];
        [self.addressTextView addSubview:mylabel];
        
    }

    [self loadPersonalData];
}
-(void)backaction
{
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

-(void)loadPersonalData
{

    Personal *personal = currRes.personal;
    if(personal)
    {

    self.fullnameTextField.text  =  personal.fullName;
    self.datelabel.text          =  personal.dob;
    self.addressTextView.text    =  personal.address;
    if (self.addressTextView.text.length!=0)
        mylabel.hidden = YES;
    
    self.languagesKnownTextField.text = personal.language;
    self.phonenumberTextField.text    = personal.phNo ;
    self.emailIdTextField.text        = personal.emailId;
    
    if ([personal.gender isEqualToString:@"Male"])
        [self.Male_Button setImage:[UIImage imageNamed:@"circle_hl.png"] forState:UIControlStateNormal];
    
    else
        [self.FemaleButton setImage:[UIImage imageNamed:@"circle_hl.png"] forState:UIControlStateNormal];
    

    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
	[keyBoardController addToolbarToKeyboard];

    
   /* self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveData)] ;*/
    
    if ([appDelegate isIphone5]) {
        personalinfoScrollView.frame=CGRectMake(0,   0, 320, 545);
        [personalinfoScrollView setContentSize:CGSizeMake(320,900)];
        
    }
    else
    {
        personalinfoScrollView.frame=CGRectMake(0,  0, 320, 460);
        [personalinfoScrollView setContentSize:CGSizeMake(320, 678)];
    }

    }

-(void)saveData
{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailTest evaluateWithObject:emailIdTextField.text] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Invalid EmailId" message:@"Email should be in proper format" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    else
    {
        
         

       BOOL success = [self addPersonalinfo];
   
      if(success)
        {
        NSLog(@"Added Sucessfully");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Saved" message:@"data saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
       }
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [textField resignFirstResponder];
    
    return YES;
}
//=======METHOD TO ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN =========//

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        NSLog(@"INSIDE MOVEDUP AND SCROLL VIEW Y IS::%f",rect.origin.y);
        
        rect.origin.y -=0; //kOFFSET_FOR_KEYBOARD+20;
        
        if (currentTextField.tag==4)
        {
            rect.origin.y -= 0;//kOFFSET_FOR_KEYBOARD+40;
        }
        
    }
    else
    {
        
        rect.origin.y += 0;//kOFFSET_FOR_KEYBOARD+20;
        
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    
    
   // [textField setInputAccessoryView:accessoryView];
    
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=3  )
        {
            
           // [self setViewMovedUp:YES];
        }
    }
    else
    {
        
        if( textField.tag<=3   )
        {
            
           // [self setViewMovedUp:YES];
        }
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=3  )
        {
           // [self setViewMovedUp:NO];
            
        }
        
    } else
    {
        
        if( textField.tag<=3 )
        {
            
           // [self setViewMovedUp:NO];
        }
        
    }
    
}


//=======================METHOD TO DONE BUTTON ====================//

-(IBAction)doneButton_Pressed:(id)sender
{
    if(addressTextView.text.length == 0)
    {
        mylabel.hidden = NO;
        textView_Text = @"";
        
        
    }
    [addressTextView resignFirstResponder];
    [currentTextField resignFirstResponder];
}




//================UITEXTVIEW DELEGATE METHODS======================//

- (BOOL)textViewShouldBeginEditing:(UITextView *)aTextView
{
    NSLog(@"textfieldbegin");
    //[self setViewMovedUp:YES];
    mylabel.hidden = YES;
    
    //[addressTextView setInputAccessoryView:accessoryView];
    
    return YES;
    
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if ([appDelegate isIphone5]) {
        
        
        if(addressTextView.text.length == 0){
            mylabel.hidden = NO;
            [addressTextView resignFirstResponder];
        }
        textView_Text = textView.text;
    }else
    {
        NSLog(@"textfieldchange");
        if(addressTextView.text.length == 0){
            mylabel.hidden = NO;
            [addressTextView resignFirstResponder];
        }
        textView_Text = textView.text;
        
    }
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{  NSLog(@"textfieldend");
    //[self setViewMovedUp:NO];
    if(addressTextView.text.length == 0){
        mylabel.hidden = NO;
       // [addressTextView resignFirstResponder];
    }
}
//=================================================================
- (IBAction)genderButton_Tapped:(id)sender
{
    switch ([sender tag]) {
        case 0:
           
           
            [self.FemaleButton setImage:[UIImage imageNamed:@"circle_hl.png"] forState:UIControlStateNormal];
            [self.Male_Button setImage:[UIImage imageNamed:@"circle.png"]  forState:UIControlStateNormal];
           genderLabel.text=@"Female";
            break;
        case 1:
            
            [self.Male_Button setImage:[UIImage imageNamed:@"circle_hl.png"]  forState:UIControlStateNormal];
            [self.FemaleButton setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
            genderLabel.text=@"Male";
            break;
            
        default:
            break;
    }

    
}
- (IBAction)selectdateButtonTapped:(id)sender
{
    
   
    asheet = [[UIActionSheet alloc] init];
    asheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
    [datePicker addTarget:self action:@selector(LabelChange:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    [asheet addSubview:datePicker];
    
    
    
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,320,40)];
    tools.barStyle=UIBarStyleBlackOpaque;
    [asheet addSubview:tools];
    
    
    
    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btnActinDoneClicked:)];
    doneButton.tag=1;
    doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
    
    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *array = [[NSArray alloc]initWithObjects:flexSpace,flexSpace,doneButton,nil];
    
    [tools setItems:array];
    
    
    
    //picker title
    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(60,8, 200, 25)];
    lblPickerTitle.text=@"Select Date";
    lblPickerTitle.backgroundColor=[UIColor clearColor];
    lblPickerTitle.textColor=[UIColor whiteColor];
    lblPickerTitle.textAlignment=UITextAlignmentCenter;
    lblPickerTitle.font=[UIFont boldSystemFontOfSize:15];
    [tools addSubview:lblPickerTitle];
    
    
    [asheet showFromRect:CGRectMake(0,480, 320,215) inView:self.view animated:YES];
    [asheet setBounds:CGRectMake(0,0, 320, 500)];
    
    
}
- (IBAction) btnActinDoneClicked:(id)sender
{
    
    [asheet dismissWithClickedButtonIndex:0 animated:YES];
    if (self.datelabel.text.length==0)
    {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        df.dateStyle = NSDateFormatterMediumStyle;
        datelabel.text = [NSString stringWithFormat:@"%@",
                          [df stringFromDate:datePicker.date]];
        
        
    }

    
}

- (void)LabelChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    datelabel.text = [NSString stringWithFormat:@"%@",
                      [df stringFromDate:datePicker.date]];
}

//==============++++++++++++++++++

-(BOOL) addPersonalinfo
{
    
    NSManagedObjectContext *context = self.managedObjectContext;
    
    if(currRes.personal){
        
        currRes.personal.fullName = fullnameTextField.text;
        currRes.personal.dob      = datelabel.text;
        currRes.personal.gender   = genderLabel.text;
        currRes.personal.phNo     = phonenumberTextField.text;
        currRes.personal.emailId  = emailIdTextField.text;
        currRes.personal.language = languagesKnownTextField.text;
        currRes.personal.address  = addressTextView.text;
        currRes.personal.ownby    = currRes;
        
    }
    else{
    Personal *newPersonal = (Personal *)[NSEntityDescription insertNewObjectForEntityForName:@"Personal" inManagedObjectContext:context];
    
    
    
    newPersonal.fullName   = fullnameTextField.text;
    newPersonal.dob        = datelabel.text;
    newPersonal.gender     = genderLabel.text;
    newPersonal.phNo       = phonenumberTextField.text;
    newPersonal.emailId    = emailIdTextField.text;
    newPersonal.language   = languagesKnownTextField.text;
    newPersonal.address    = addressTextView.text;
    newPersonal.ownby      = currRes;
    }
     
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding Personal Details failed %@", [error localizedDescription]);
        return NO;
    }
 
       
    return YES;
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

//
//  MediatorViewController.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Resume.h"

@interface MediatorViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic)IBOutlet UITableView *myTableView;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj listType:(NSString *)type;
@end

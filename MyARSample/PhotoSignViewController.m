//
//  PhotoSignViewController.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "PhotoSignViewController.h"
#import "SignViewController.h"
#import "AppDelegate.h"
#import "PhotoSign.h"
@interface PhotoSignViewController (){
    
    AppDelegate *appDelegate;
    UIActionSheet *asheet;
    UIDatePicker *datepicker ;
    Resume *currRes;
    
}

@end

@implementation PhotoSignViewController
@synthesize photoImageView;
@synthesize placeTextField;
@synthesize objectiveTextView;
@synthesize currentTextField;
@synthesize accessoryView;
@synthesize dateLabel;
@synthesize signImageView;
@synthesize managedObjectContext;
@synthesize numLines;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currRes = resumeObj;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    self.navigationItem.title=@"Photo & Signature";
    
    numLines=0;
  
    
    self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    UIImage *myImage2 = [UIImage imageNamed:@"Savebutton.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(saveData) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    [self loadPhotoSign];

}
-(void)viewWillAppear:(BOOL)animated
{
    
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
	[keyBoardController addToolbarToKeyboard];
    self.photoImageView.frame=CGRectMake(187, 11, 115, 115);

    if (appDelegate.signImage) 
        self.signImageView.image = appDelegate.signImage;
    
}

-(void) loadPhotoSign
{
  
     PhotoSign *ref = currRes.photosign;
    
    
    if(ref)
    {
    self.photoImageView.image   = [UIImage imageWithData:ref.photo];
    self.signImageView.image    = [UIImage imageWithData:ref.sign];
    self.objectiveTextView.text = ref.objective;
    self.placeTextField.text    = ref.place;
    }



}

-(void)backaction
{
    
    [UIView transitionWithView:self.navigationController.view
            duration:0.15f
            options:UIViewAnimationOptionTransitionCrossDissolve
            animations:^{
                            [self.navigationController popViewControllerAnimated:NO];
                        }
                      completion:NULL];
    
}


-(void)saveData
{
    
        BOOL success = [self addPhotoAndSign];
        
        if(success)
        {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Saved" message:@"data saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    
    
}


-(BOOL)addPhotoAndSign{
    
    
    NSManagedObjectContext *context = self.managedObjectContext;
    
    if(currRes.photosign){
        
        if(photoImageView.image){
            NSData *photoImageData = UIImageJPEGRepresentation(self.photoImageView.image, 1);
            currRes.photosign.photo = photoImageData;
        }
        if(signImageView.image){
            NSData *signImageData = UIImageJPEGRepresentation(self.signImageView.image, 1);
            currRes.photosign.sign = signImageData;
        }
        
        
        currRes.photosign.place = self.placeTextField.text;
        currRes.photosign.objective = self.objectiveTextView.text;
        currRes.photosign.ownby = currRes;
        
    }
    else{
    PhotoSign *photoSign = (PhotoSign *)[NSEntityDescription insertNewObjectForEntityForName:@"PhotoSign" inManagedObjectContext:context];
    
    
    if(photoImageView.image){
        NSData *photoImageData = UIImageJPEGRepresentation(self.photoImageView.image, 1);
        photoSign.photo = photoImageData;
    }
    if(signImageView.image){
        NSData *signImageData = UIImageJPEGRepresentation(self.signImageView.image, 1);
        photoSign.sign = signImageData;
    }
    
    
    photoSign.place = self.placeTextField.text;
    photoSign.objective = self.objectiveTextView.text;
    photoSign.ownby = currRes;
    }
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding PhotoSign Details failed %@", [error localizedDescription]);
        return NO;
    }

    return YES;
}

- (IBAction)DigitalSignature:(id)sender{
    SignViewController *signVC = [[SignViewController alloc]init];
    
    UINavigationController *navcont = [[UINavigationController alloc] initWithRootViewController:signVC];
    [self presentModalViewController:navcont animated:YES];
    
}
- (IBAction)dateAction:(id)sender{

    asheet = [[UIActionSheet alloc] init];
    asheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    
    
    datepicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
    [datepicker addTarget:self action:@selector(LabelChange:) forControlEvents:UIControlEventValueChanged];
    datepicker.datePickerMode = UIDatePickerModeDate;
    
        
    [asheet addSubview:datepicker];
    
    
    
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,320,40)];
    tools.barStyle=UIBarStyleBlackOpaque;
    [asheet addSubview:tools];
    
    
    
    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btnActinDoneClicked:)];
    doneButton.tag=1;
    doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
    
    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *array = [[NSArray alloc]initWithObjects:flexSpace,flexSpace,doneButton,nil];
    
    [tools setItems:array];
    
    
    
    //picker title
    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(60,8, 200, 25)];
    lblPickerTitle.text=@"Select Date";
    lblPickerTitle.backgroundColor=[UIColor clearColor];
    lblPickerTitle.textColor=[UIColor whiteColor];
    lblPickerTitle.textAlignment=UITextAlignmentCenter;
    lblPickerTitle.font=[UIFont boldSystemFontOfSize:15];
    [tools addSubview:lblPickerTitle];
    
    
    [asheet showFromRect:CGRectMake(0,480, 320,215) inView:self.view animated:YES];
    [asheet setBounds:CGRectMake(0,0, 320, 500)];







}
- (IBAction)didClickOpenPDF:(id)sender{NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfPath = [documentsDirectory stringByAppendingPathComponent:@"Resume.pdf"];
    NSLog(@"pdfPath is %@",pdfPath);
    if([[NSFileManager defaultManager] fileExistsAtPath:pdfPath]) {
        
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:pdfPath password:nil];
        
        if (document != nil)
        { NSLog(@"document is.not nil");
            ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
            readerViewController.delegate = self;
            
            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [self  presentModalViewController:readerViewController animated:YES];
        }
    }
}
- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    [self dismissModalViewControllerAnimated:YES];
}


- (IBAction)addPhoto:(id)sender
{
    UIActionSheet *alsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Done" destructiveButtonTitle:nil otherButtonTitles:@"Take A Photo", @"Photo Library",nil];
    [alsheet showInView:[self.view superview]];
}
//================================ UIIMAGEPICKERCONTROLLER DELEGATES METHODS ============================//

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissModalViewControllerAnimated:YES];
    self.photoImageView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Take a Photo");
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Unable to Capture image" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
        }
        else
        {
            // if capture image is not supported by the device or Stimaulator
            UIImagePickerController* imagePickerController = [[UIImagePickerController alloc]init];
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePickerController.delegate = self;
            imagePickerController.allowsEditing = YES;
            [self presentModalViewController:imagePickerController animated:YES];
        }
        
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"From");
        // Create image picker controller
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        // Set source to the camera
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate = self;
        
        
        // Allow editing of image ?
        imagePicker.allowsEditing = NO;
        // Show image picker
        [self presentViewController:imagePicker animated:YES completion:nil];
        
        
    }
    
}

- (IBAction) btnActinDoneClicked:(id)sender
{
    
 numLines = objectiveTextView.contentSize.height/objectiveTextView.font.lineHeight;
    NSLog(@"numLines %d",numLines);
    [asheet dismissWithClickedButtonIndex:0 animated:YES];
     NSLog(@"date is...%d ",self.dateLabel.text.length);
    
    if (self.dateLabel.text.length==0)
    {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        df.dateStyle = NSDateFormatterMediumStyle;
        dateLabel.text = [NSString stringWithFormat:@"%@",
                          [df stringFromDate:datepicker.date]];
        
        NSLog(@"date is...%@ ",dateLabel.text);
        
        
    }
    
    
}

- (void)LabelChange:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    dateLabel.text = [NSString stringWithFormat:@"%@",
                      [df stringFromDate:datepicker.date]];
}


//===================   UITEXTFIELD DELEGATE METHODS =================//
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   
    [textField resignFirstResponder];
    
    return YES;
}
-(IBAction)doneButton_Pressed:(id)sender
{
    if(objectiveTextView.text.length == 0)
    {
        //mylabel.hidden = NO;
        objectiveTextView.text = @"";
        
        
    }
    [objectiveTextView resignFirstResponder];
    [currentTextField resignFirstResponder];
}

//================UITEXTVIEW DELEGATE METHODS======================//

- (BOOL)textViewShouldBeginEditing:(UITextView *)aTextView
{
    
    [objectiveTextView setInputAccessoryView:accessoryView];
    
    return YES;
    
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if ([appDelegate isIphone5]) {
        
        
        if(objectiveTextView.text.length == 0){
            // mylabel.hidden = NO;
            [objectiveTextView resignFirstResponder];
        }
        
    }else
    {
        
        if(objectiveTextView.text.length == 0){
            //mylabel.hidden = NO;
            [objectiveTextView resignFirstResponder];
        }
       
        
    }
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{  
    
    if(objectiveTextView.text.length == 0){
            }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

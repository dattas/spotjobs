//
//  PhotoSign.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Resume;

@interface PhotoSign : NSManagedObject

@property (nonatomic, retain) NSString * objective;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSData * sign;
@property (nonatomic, retain) Resume *ownby;

@end

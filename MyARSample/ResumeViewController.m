//
//  ResumeViewController.m
//  MyARSample
//
//  Created by vairat on 01/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "ResumeViewController.h"
#import "MediatorViewController.h"
#import "CoreText/CoreText.h"
#import "Personal.h"
#import "PhotoSign.h"
#import "Resume.h"
@interface ResumeViewController ()
{
    
    CGSize pagesize;
    NSString *currentResumeTitle;
    Resume *currentResume;
    
    //int endPoint;
    CGFloat currentPageY;
    //CGContextRef context;
    
    
    NSMutableArray *eduTble_RowPoints;
    NSMutableArray *proTble_RowPoints;
}

@end

@implementation ResumeViewController
@synthesize personalinfoView;
@synthesize educationView;
@synthesize referenceView;
@synthesize projectView;
@synthesize otherinfoView;
@synthesize experienceView;
@synthesize myTextField;
@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resume:(Resume *)resumeObj
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        currentResume = resumeObj;
        currentResumeTitle = resumeObj.title;
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    //endPoint = 0;
    currentPageY = 0;
    self.navigationItem.title=[NSString stringWithFormat:@"%@ Resume",currentResumeTitle];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;

    
}
-(void)viewWillAppear:(BOOL)animated
{
}


-(void)backaction
{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController popViewControllerAnimated:TRUE];
                    }
                    completion:NULL];
    
}


/*
-(void)share
{
    //[self.navigationItem.rightBarButtonItem
   
    pagesize=CGSizeMake(595,842);
    NSString *filename=@"Resume.pdf";
    NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory =[path objectAtIndex:0];
    NSString *pdfpathwithfilename=[documentDirectory stringByAppendingPathComponent:filename];
    if ( [appDelegate getProjectinfo].count > 0 ||[appDelegate getEducationinfo].count > 0|| [appDelegate getExperienceinfo].count > 0|| [appDelegate getReferenceinfo].count > 0|| [appDelegate getReferenceinfo].count > 0) {
         [self GeneratePDF:pdfpathwithfilename];
         [self.view addSubview:preView];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Failed to Create Pdf" message:@"please fill all sections in post resume" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    
    }
   
    
}
 */

-(void)GeneratePDFButtonTapped{
    
    pagesize=CGSizeMake(595,842);
    NSString *filename=@"Resume.pdf";
    NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory =[path objectAtIndex:0];
    NSString *pdfpathwithfilename=[documentDirectory stringByAppendingPathComponent:filename];
    
    [self GeneratePDF:pdfpathwithfilename];
}


-(void)GeneratePDF:(NSString *)filepath
{
    UIGraphicsBeginPDFContextToFile(filepath, CGRectZero, nil);
    
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 595,842), nil);
    //context = UIGraphicsGetCurrentContext();
    //[self drawBackground];   //background
    [self drawPDFData];        
    //[self drawText];         //text in pdf
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 595,842), nil);
  //  [self drawImage1];      // second page
    UIGraphicsEndPDFContext();
    
    [self openPDFView];
    
    
}

/*
-(void)drawBackground
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect=CGRectMake(0, 0, 595,842);
    CGContextSetFillColorWithColor(context,[[UIColor whiteColor]CGColor]);
    CGContextFillRect(context,rect);
}
*/
-(void)drawPDFData
{
    
    UIFont* headerFont = [UIFont fontWithName:@"Helvetica-Bold" size:14.0];
    UIFont* textFont = [UIFont fontWithName:@"Helvetica" size:14.0];
    
    Personal *person = currentResume.personal;
    NSString *personalHeaderText=[[NSString alloc]initWithFormat:@"Name: %@ \nMobile No: %@ \nEmailId: %@",person.fullName,person.phNo,person.emailId];
   
    [personalHeaderText drawInRect:CGRectMake(35, 40, 300, 60) withFont:headerFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
    
    //to add Photo
    PhotoSign *photosign = currentResume.photosign;
    UIImage *photo       = [UIImage imageWithData:photosign.photo];
    CGRect photoRect     = CGRectMake(480, 20, 85, 65);
    [photo drawInRect:photoRect];

    
    //to add Line
    CGRect lineRect  = CGRectMake(35, 95, 530, 1);
    UIImage *line  = [UIImage imageNamed:@"line"];
    [line drawInRect:lineRect];
    
    //to add objective Image
    CGRect objectiveImageRect = CGRectMake(35, 100, 530, 20);
    UIImage *objectiveImage   = [UIImage imageNamed:@"OBJECTIVE"];
    [objectiveImage drawInRect:objectiveImageRect];
    
    //to add Photo
    [photosign.objective drawInRect:CGRectMake(35, 120, 530, 40) withFont:[UIFont fontWithName:@"Helvetica" size:11.0] lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
    
    //to add Education Image, Table and Education details
    CGRect imageRect2=CGRectMake(35, 160, 530, 20);
    UIImage *image2=[UIImage imageNamed:@"EDUCATIONAL"];
    [image2 drawInRect:imageRect2];
    
    NSSet *eduSet = currentResume.education;
    NSArray *eduArray = [eduSet allObjects];
    
    CGRect eduImageRect = CGRectMake(35,185,530, 20);
    UIImage *eduImage = [UIImage imageNamed:@"Course"];
    [eduImage drawInRect:eduImageRect];
    
    
    
    int xOrigin = 35;
    int yOrigin = currentPageY = 207;
    
    int rowHeight = 30;
    int columnWidth = 106;
    
    int numberOfRows = [eduArray count];
    int numberOfColumns = 5;

    [self drawTableAt:CGPointMake(xOrigin, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns tableType:1];
    
    
    [self drawTableDataAt:CGPointMake(xOrigin, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns];
    
 //to add Project Image, Table and Project details
    
    int index = [eduTble_RowPoints count]-1;
    currentPageY = [[eduTble_RowPoints objectAtIndex:index]intValue];
    
    CGRect proImageRect   = CGRectMake(35, currentPageY+10, 530, 20);
    UIImage *proImage     = [UIImage imageNamed:@"PROJECTDETAILS"];
    [proImage drawInRect:proImageRect];
    
    CGRect projectImageRect = CGRectMake(35, currentPageY+35, 530, 20);
    UIImage *projectImage   = [UIImage imageNamed:@"ProjectTitle"];
    [projectImage drawInRect:projectImageRect];
    
    NSSet *proSet = currentResume.projects;
    NSArray *proArray = [proSet allObjects];
    numberOfRows = [proArray count];
    yOrigin =  currentPageY+58;
    NSLog(@" Project Table yOrigin %d",yOrigin);   
    [self drawTableAt:CGPointMake(xOrigin, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns tableType:2];
    
    [self drawProTableDataAt:CGPointMake(xOrigin, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns];
    
 //to add Experience Image, Table and Experience details
    
    index = [proTble_RowPoints count]-1;
    currentPageY = [[proTble_RowPoints objectAtIndex:index]intValue];
    
     CGRect imageRect3=CGRectMake(35, currentPageY+10, 530, 20);
    UIImage *image3=[UIImage imageNamed:@"EXPERIENCEDETAILS"];
    [image3 drawInRect:imageRect3];
    
    NSSet *expSet = currentResume.experience;
    NSArray *expArray = [expSet allObjects];

    currentPageY += 40;
    
 for (int i = 0; i< [expArray count]; i++)
  {
      
    // to add Experience header
    NSString *expHeaderText = [[NSString alloc]initWithFormat:@"Company Name:\nPostion:\nPeriod:\nLocation:\nJob Responsibility:"];
      
     CGSize size = [expHeaderText sizeWithFont:headerFont forWidth:150 lineBreakMode:UILineBreakModeWordWrap];
      
      if (842 - currentPageY < size.height) {
          // create a new page and reset the current page's Y value
          //UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, kDefaultPageWidth, kDefaultPageHeight), nil);
         // currentPageY = kMargin;
          UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 595,842), nil);
          currentPageY = 0;
      }
      
     [expHeaderText drawInRect:CGRectMake(35, currentPageY, 150, 95) withFont:headerFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
        

    Experience *exp    = [expArray objectAtIndex:i];
      
     // to add Experience text 
    NSString *expText  = [[NSString alloc]initWithFormat:@" %@ \n %@ \n %@ \n %@",exp.company,exp.position,exp.period,exp.location];
      [expText drawInRect:CGRectMake(35+150+10, currentPageY, 150, 95) withFont:textFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
      
      
    // to add Experience responsibilty 
        int lines = 1;
        int jobResHight = 20;
        if ([exp.jobresponse length]>58)
        {
             lines = ceil([exp.jobresponse length]/58);
             jobResHight = lines*20;

        }
        
    [exp.jobresponse drawInRect:CGRectMake(35+150+15,currentPageY+70, 380, jobResHight) withFont:textFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
    
    currentPageY = currentPageY+95+jobResHight;
    }
    
    
    
    if (currentPageY >= 842) {
        
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 595,842), nil);
        currentPageY = 0;
    }
    CGRect refRect = CGRectMake(35,  currentPageY+15, 530, 20);
    UIImage *image5=[UIImage imageNamed:@"REFERENCES1"];
    [image5 drawInRect:refRect];
    currentPageY += 15+20;
    
    NSSet *refSet = currentResume.reference;
    NSArray *refArray = [refSet allObjects];
    
    for (int i = 0; i< [refArray count]; i++)
    {
        
        // to add Reference header
        NSString *refHeaderText = [[NSString alloc]initWithFormat:@"Reference Name:\nCompany:\nPhone:\nEmail:"];
        
        //CGSize size = [refHeaderText sizeWithFont:headerFont forWidth:150 lineBreakMode:UILineBreakModeWordWrap];
        
        [refHeaderText drawInRect:CGRectMake(35, currentPageY+10, 150, 75) withFont:headerFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
        
        Reference *ref    = [refArray objectAtIndex:i];
        
        // to add Experience text
        NSString *refText  = [[NSString alloc]initWithFormat:@" %@ \n %@ \n %@ \n %@",ref.referencename,ref.company,ref.phone,ref.email];
        [refText drawInRect:CGRectMake(35+150+10, currentPageY+10, 150, 95) withFont:textFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
        
        
        currentPageY += 75;
    }
    
    CGRect persoanalRect = CGRectMake(35, currentPageY+15, 530, 20);
    UIImage *perImage = [UIImage imageNamed:@"PERSONALINFORMATION"];
    [perImage drawInRect:persoanalRect];
    
    currentPageY += 35;
    NSString *personalText = [[NSString alloc]initWithFormat:@"Languages Known:\nContact Address:"];
    
    [personalText drawInRect:CGRectMake(35, currentPageY+10, 150, 75) withFont:headerFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
    
    NSString *str = [NSString stringWithFormat:@"%@\n%@",person.language,person.address];
    
    [str drawInRect:CGRectMake(35+200, currentPageY+10, 150, 75) withFont:textFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
    
    currentPageY += 75;
    
    NSString *iHereText=@"I, hereby declare that the information furnished above is true to the best of my knowledge.";

    [iHereText drawInRect:CGRectMake(35, currentPageY+10, 530, 75) withFont:textFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
    
    currentPageY += 75;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *today = [NSDate date];
    NSString *dateString = [dateFormatter stringFromDate:today];
    
     NSString *placeDatetext=[[NSString alloc]initWithFormat:@"Place :%@\nDate :%@",photosign.place,dateString];
    
    [placeDatetext drawInRect:CGRectMake(35, currentPageY+10, 150, 75) withFont:textFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
    
    NSString *yoursincertext=[[NSString alloc]initWithFormat:@"Yours sincerely"];
    [yoursincertext drawInRect:CGRectMake(350, currentPageY+10, 150, 20) withFont:textFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
    
    UIImage *signImage = [UIImage imageWithData:photosign.sign];
    
    CGRect signImageRect = CGRectMake(370, currentPageY+30, 100, 60);//CGRectMake(49,191 , 724, 28);
    [signImage drawInRect:signImageRect];
    
//    CGRect expImageRect = CGRectMake(35,endPoint+35,530, 20);
//    UIImage *expImage = [UIImage imageNamed:@"company"];
//    [expImage drawInRect:expImageRect];
    
    
   // numberOfRows = [expArray count];
   // yOrigin = endPoint+57;
   /*  
    [self drawTableAt:CGPointMake(xOrigin, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns tableType:2];
  
    [self drawExpTableDataAt:CGPointMake(xOrigin, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns];
   
    CGRect imageRect4=CGRectMake(35,  470, 530, 20);
    UIImage *image4=[UIImage imageNamed:@"PROJECTDETAILS"];
    [image4 drawInRect:imageRect4];
    CGRect imageRect5=CGRectMake(35, 495, 530, 20);
    UIImage *image5=[UIImage imageNamed:@"REFERENCES1"];
    [image5 drawInRect:imageRect5];
    CGRect imageRect6=CGRectMake(35, 610, 530, 20);
    UIImage *image6=[UIImage imageNamed:@"PERSONALINFORMATION"];
    [image6 drawInRect:imageRect6];
    CGRect imageRect7=CGRectMake(-13, -13, 620, 802);
    UIImage *image7=[UIImage imageNamed:@"Page"];
    [image7 drawInRect:imageRect7];
    */
    
}

-(void)drawTableAt:(CGPoint)origin
     withRowHeight:(int)rowHeight
    andColumnWidth:(int)columnWidth
       andRowCount:(int)numberOfRows
    andColumnCount:(int)numberOfColumns tableType:(int)table

{
    
    switch (table)
    {
        case 1:{
            
            eduTble_RowPoints = [[NSMutableArray alloc]init];
            NSSet *eduSet = currentResume.education;
            NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"insertedTime" ascending:YES]];
            NSArray *myArray = [[eduSet allObjects] sortedArrayUsingDescriptors:sortDescriptors];
            
            int colHeight;
            int newOrigin = origin.y;
            CGPoint from = CGPointMake(origin.x, newOrigin);
            CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
            [eduTble_RowPoints addObject:@(newOrigin)];
            [self drawLineFromPoint:from toPoint:to];

            for (int i = 0; i <= numberOfRows; i++)
            {
                
                if([myArray count]>i)
                {
                   Education *edu = [myArray objectAtIndex:i];
                   // NSLog(@"========%@=======",edu.collegeschool);
                    if([edu.collegeschool length] >28 || [edu.universityboard length] >14)
                       newOrigin =  newOrigin +rowHeight+15;
                      // NSLog(@"%@  length >28",edu.collegeschool);
                   else
                       newOrigin =  newOrigin +(rowHeight);
                    //NSLog(@"newOrigin:::%d",newOrigin);
                    [eduTble_RowPoints addObject:@(newOrigin)];
                CGPoint from = CGPointMake(origin.x, newOrigin);
                CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
                [self drawLineFromPoint:from toPoint:to];
                }
                
            }
            colHeight = newOrigin;
            for (int i = 0; i <= numberOfColumns; i++)
            {
                int newOrigin = 0;
                if( i == 3)
                    newOrigin = origin.x + (columnWidth*i)+110;
                else if(i ==4)
                    newOrigin = origin.x + (columnWidth*i)+58;
                else
                    newOrigin =  origin.x + (columnWidth*i);
                
                CGPoint from = CGPointMake(newOrigin, origin.y);
                CGPoint to = CGPointMake(newOrigin, colHeight);
                [self drawLineFromPoint:from toPoint:to];
                
            }
            }
            break;
            
        case 2:{
            
            proTble_RowPoints = [[NSMutableArray alloc]init];
            NSSet *proSet = currentResume.projects;
            NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"insertedTime" ascending:YES]];
            NSArray *myArray = [[proSet allObjects] sortedArrayUsingDescriptors:sortDescriptors];
            
            int colHeight;
            int newOrigin = origin.y;
            CGPoint from = CGPointMake(origin.x, newOrigin);
            CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
            [proTble_RowPoints addObject:@(newOrigin)];
            [self drawLineFromPoint:from toPoint:to];
            
            for (int i = 0; i <= numberOfRows; i++)
            {
                
                if([myArray count]>i)
                {
                    Project *pro = [myArray objectAtIndex:i];
                    if([pro.projecttitle length] >28 || [pro.role length] >15 || [pro.duration length] >15)
                        newOrigin =  newOrigin +rowHeight+15;
                    else
                        newOrigin =  newOrigin +(rowHeight);

                    [proTble_RowPoints addObject:@(newOrigin)];
                    CGPoint from = CGPointMake(origin.x, newOrigin);
                    CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
                    [self drawLineFromPoint:from toPoint:to];
                }
                
            }
            colHeight = newOrigin;
            for (int i = 0; i <= numberOfColumns; i++)
            {
                int newOrigin = 0;
                if( i == 1)
                    newOrigin = origin.x + (columnWidth*i)+74;
                else if(i == 2)
                    newOrigin = origin.x + (columnWidth*i)+67;
                else if(i == 3)
                    newOrigin = origin.x + (columnWidth*i)+25;
                else if(i == 4)
                    newOrigin = origin.x + (columnWidth*i)+21;
                else
                    newOrigin = origin.x + (columnWidth*i);
                CGPoint from = CGPointMake(newOrigin, origin.y);
                CGPoint to = CGPointMake(newOrigin, colHeight);
                [self drawLineFromPoint:from toPoint:to];
                
            }
        }
            break;

            
                 default:
            break;
    }
    
    for (NSString *value in proTble_RowPoints) {
        NSLog(@"proTble_RowPoints::%@",value);
    }
    
    
    }


-(void)drawLineFromPoint:(CGPoint)from toPoint:(CGPoint)to
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 2.0);
    
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat components[] = {0.2, 0.2, 0.2, 0.3};
    
    CGColorRef color = CGColorCreate(colorspace, components);
    
    CGContextSetStrokeColorWithColor(context, color);
    
    
    CGContextMoveToPoint(context, from.x, from.y);
    CGContextAddLineToPoint(context, to.x, to.y);
    
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
    
}

-(void)drawTableDataAt:(CGPoint)origin
         withRowHeight:(int)rowHeight
        andColumnWidth:(int)columnWidth
           andRowCount:(int)numberOfRows
        andColumnCount:(int)numberOfColumns
{
    int padding = 10;
    NSSet *eduSet = currentResume.education;
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"insertedTime" ascending:YES]];
    
    NSArray *myArray = [[eduSet allObjects] sortedArrayUsingDescriptors:sortDescriptors];

    for(int i = 0; i < [myArray count]; i++)
    {
       Education *infoToDraw = [myArray objectAtIndex:i];

        for (int j = 0; j < numberOfColumns; j++)
        {
            
            int newOriginX = 0;
            //to Set  X point to start data
            if(j == 3)
               newOriginX = origin.x + (j*columnWidth)+110;
            else if(j == 4)
               newOriginX = origin.x + (j*columnWidth)+60;
            else
               newOriginX = origin.x + (j*columnWidth)-5;
            
            
            //to Set Y point to start data
            int newOriginY = [[eduTble_RowPoints objectAtIndex:i]intValue];
              
            
            CGRect frame = CGRectMake(newOriginX + padding, newOriginY+35, columnWidth, rowHeight);
            
            switch (j) {
                case 0:
                    [self drawText:infoToDraw.degreecourse inFrame:frame];
                    break;
                case 1:
                    [self drawText:infoToDraw.universityboard inFrame:frame];
                    break;
                case 2:
                      {
                    int cWidth = columnWidth+100;
                    frame = CGRectMake(newOriginX + padding, newOriginY+35, cWidth, rowHeight);
                    [self drawText:infoToDraw.collegeschool inFrame:frame];
                      }
                    break;
                case 3:
                    [self drawText:infoToDraw.yearofpassing inFrame:frame];
                    break;
                case 4:
                    [self drawText:infoToDraw.result inFrame:frame];
                    break;
                    
                default:
                    break;
                   }
                }
      
    }
    
}

-(void)drawProTableDataAt:(CGPoint)origin
         withRowHeight:(int)rowHeight
        andColumnWidth:(int)columnWidth
           andRowCount:(int)numberOfRows
        andColumnCount:(int)numberOfColumns
{
    
     int padding = 10;
    NSSet *proSet = currentResume.projects;
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"insertedTime" ascending:YES]];
    
    NSArray *myArray = [[proSet allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    for(int i = 0; i < [myArray count]; i++)
    {
        Project *infoToDraw = [myArray objectAtIndex:i];
        
        for (int j = 0; j < numberOfColumns; j++)
        {
            
            int newOriginX = 0;
            //to Set  X point to start data
            if(j == 1)
                newOriginX = origin.x + (j*columnWidth)+70;
            else if(j == 2)
                newOriginX = origin.x + (j*columnWidth)+90;
            else if(j == 3)
                newOriginX = origin.x + (j*columnWidth)+30;
            else if(j == 4)
                newOriginX = origin.x + (j*columnWidth)+20;
            else 
                newOriginX = origin.x + (j*columnWidth);
            
            
            //to Set Y point to start data
            int newOriginY = [[proTble_RowPoints objectAtIndex:i]intValue];
            
            
            CGRect frame = CGRectMake(newOriginX + padding, newOriginY+35, columnWidth, rowHeight);
            
            switch (j) {
                case 0:{
                    int cWidth = columnWidth+70;
                    frame = CGRectMake(newOriginX + padding, newOriginY+35, cWidth, rowHeight);
                    [self drawText:infoToDraw.projecttitle inFrame:frame];}
                    break;
                case 1:
                    [self drawText:infoToDraw.role inFrame:frame];
                    break;
                case 2:
                    [self drawText:infoToDraw.teamsize inFrame:frame];
                    break;
                case 3:
                    [self drawText:infoToDraw.duration inFrame:frame];
                    break;
                case 4:
                    [self drawText:infoToDraw.expertise inFrame:frame];
                    break;
                    
                default:
                    break;
            }
        }
        
    }
    
}

-(void)drawText:(NSString*)textToDraw inFrame:(CGRect)frameRect
{
    
    CFStringRef stringRef = (__bridge CFStringRef)textToDraw;
    // Prepare the text using a Core Text Framesetter
    CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, stringRef, NULL);
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
    
    
    CGMutablePathRef framePath = CGPathCreateMutable();
    CGPathAddRect(framePath, NULL, frameRect);
    
    // Get the frame that will do the rendering.
    CFRange currentRange = CFRangeMake(0, 0);
    CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
    CGPathRelease(framePath);
    
    // Get the graphics context.
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    
    // Put the text matrix into a known state. This ensures
    // that no old scaling factors are left in place.
    CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
    
    
    // Core Text draws from the bottom-left corner up, so flip
    // the current transform prior to drawing.
    CGContextTranslateCTM(currentContext, 0, frameRect.origin.y*2);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    // Draw the frame.
    CTFrameDraw(frameRef, currentContext);
    
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    CGContextTranslateCTM(currentContext, 0, (-1)*frameRect.origin.y*2);
    
    
    CFRelease(frameRef);
    CFRelease(stringRef);
    CFRelease(framesetter);
}




-(void)openPDFView{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfPath = [documentsDirectory stringByAppendingPathComponent:@"Resume.pdf"];
   // NSLog(@"pdfPath is %@",pdfPath);
    if([[NSFileManager defaultManager] fileExistsAtPath:pdfPath]) {
        
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:pdfPath password:nil];
        
        if (document != nil)
        { //NSLog(@"document is.not nil");
            ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
            readerViewController.delegate = self;
            
            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [self  presentModalViewController:readerViewController animated:YES];
        }
    }
}
- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)menuButton_Tapped:(id)sender
{
    switch ([sender tag]) {
        case 0:
        {
            personalinfoView=[[personalinfoViewController alloc]initWithNibName:@"personalinfoViewController" bundle:nil  resume:currentResume];
            personalinfoView.managedObjectContext = self.managedObjectContext;
            
            [self.navigationController pushViewController:personalinfoView animated:YES];
        }
            break;
            
        case 1:
        {
            NSSet *proSet = currentResume.projects;
            NSArray *arr = [proSet allObjects];
            
            if ([arr count] <= 0) {
            projectView=[[projectViewController alloc]initWithNibName:@"projectViewController" bundle:nil resume:currentResume];
            projectView.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:projectView animated:YES];
            }
            else
                [self presentMediatorViewController:@"project"];
        }
            break;
            
        case 2:
        {
            
            NSSet *eduSet = currentResume.education;
            NSArray *arr = [eduSet allObjects];
            
            if ([arr count] <= 0) {
                educationView=[[educationViewController alloc]initWithNibName:@"educationViewController" bundle:nil resume: currentResume];
                educationView.managedObjectContext = self.managedObjectContext;
                [self.navigationController pushViewController:educationView animated:YES];
            }
            else
                [self presentMediatorViewController:@"education"];
                
        }
            break;
            
        case 3:
        {
            NSSet *expSet = currentResume.experience;
            NSArray *arr = [expSet allObjects];
            
            if ([arr count] <= 0) {
            experienceView=[[experienceViewController alloc]initWithNibName:@"experienceViewController" bundle:nil resume: currentResume];
            experienceView.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:experienceView animated:YES];
            }
            else
                [self presentMediatorViewController:@"experience"];
        }
            break;
            
        case 4:
        {
            NSSet *refSet = currentResume.reference;
            NSArray *arr = [refSet allObjects];
            
            if ([arr count] <= 0) {
            referenceView=[[referenceViewController alloc]initWithNibName:@"referenceViewController" bundle:nil resume: currentResume];
            referenceView.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:referenceView animated:YES];
            }
                else
                    [self presentMediatorViewController:@"reference"];
            
        }
            break;
            
        case 5:
        {
            otherinfoView=[[otherinfoViewController alloc]initWithNibName:@"otherinfoViewController" bundle:nil resume: currentResume];
            otherinfoView.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:otherinfoView animated:YES];
        }
            break;
            
        case 6:
        {
            PhotoSignViewController *pvc =[[PhotoSignViewController alloc]initWithNibName:@"PhotoSignViewController" bundle:nil resume: currentResume];
            pvc.managedObjectContext = self.managedObjectContext;
            [self.navigationController pushViewController:pvc animated:YES];
        }
            break;
            
        case 7:
              [self GeneratePDFButtonTapped];
              break;
            
        case 8:
              [self CreateResumeReplica];
            break;
        default:
            break;
    }
    
    
}


-(void)CreateResumeReplica{
    
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Enter Resume Title" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
    
    myTextField = [[UITextField alloc] initWithFrame:CGRectMake(22.0, 45.0, 240.0, 25.0)];
    myTextField.placeholder = @"Enter Title";
    myTextField.font = [UIFont systemFontOfSize:14];
    myTextField.borderStyle = UITextBorderStyleRoundedRect;
    [myTextField setBackgroundColor:[UIColor whiteColor]];
    [myAlert addSubview:myTextField];
    [myAlert show];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.cancelButtonIndex == buttonIndex) {}
    else
    {
        
        if([myTextField.text length]>0){
            
            if([self addNewResume:myTextField.text]){
                
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Resume has replicated Successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [myAlert show];

                
            }
            else{
                
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Resume with this name already Exits" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [myAlert show];
            }
            
            
            
        }
        else{
            
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Resume should have valid Title" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [myAlert show];
        }
        
        
    }
    
}
-(BOOL) addNewResume:(NSString *)title{
    
    NSManagedObjectContext *context = self.managedObjectContext;
    
    if([self isResumeExits:title]){
        
        return NO;
        
    }
    
    
    Resume *newResume = (Resume *)[NSEntityDescription insertNewObjectForEntityForName:@"Resume" inManagedObjectContext:context];
    [newResume setValue:@"2" forKey:@"resumeId"];
    [newResume setValue:title forKey:@"title"];
    
    
    Personal *newPersonal = (Personal *)[NSEntityDescription insertNewObjectForEntityForName:@"Personal" inManagedObjectContext:context];
    
    newPersonal.title      = currentResume.personal.title;
    newPersonal.fullName   = currentResume.personal.fullName;
    newPersonal.dob        = currentResume.personal.dob;
    newPersonal.gender     = currentResume.personal.gender;
    newPersonal.phNo       = currentResume.personal.phNo;
    newPersonal.emailId    = currentResume.personal.emailId;
    newPersonal.language   = currentResume.personal.language;
    newPersonal.address    = currentResume.personal.address;
    newPersonal.ownby      = newResume;
    
    
    
        NSSet *eduSet     = currentResume.education;
        NSArray *eduArray = [eduSet allObjects];
    
    
    for (Education *edu in eduArray) {
        Education *education = (Education *)[NSEntityDescription insertNewObjectForEntityForName:@"Education" inManagedObjectContext:context];
        
        education.degreecourse        = edu.degreecourse;
        education.universityboard     = edu.universityboard;
        education.collegeschool       = edu.collegeschool;
        education.result              = edu.result;
        education.yearofpassing       = edu.yearofpassing;
        education.perorcgpa           = edu.perorcgpa;
        education.insertedTime        = [NSDate date];
        education.ownby               = newResume;

    }
    
    NSSet *proSet     = currentResume.projects;
    NSArray *proArray = [proSet allObjects];
    
    for (Project *pro in proArray) {
        Project *project = (Project *)[NSEntityDescription insertNewObjectForEntityForName:@"Project" inManagedObjectContext:context];
        
        project.projecttitle   = pro.projecttitle;
        project.duration       = pro.duration ;
        project.role           = pro.role;
        project.teamsize       = pro.teamsize;
        project.expertise      = pro.expertise;
        project.insertedTime   = [NSDate date];
        project.ownby          = newResume;
        
    }
    
    
    NSSet *expSet     = currentResume.experience;
    NSArray *expArray = [expSet allObjects];
    
    for(Experience *exp in expArray) {
        
        Experience *experience = (Experience *)[NSEntityDescription insertNewObjectForEntityForName:@"Experience" inManagedObjectContext:context];
        
          experience.company      = exp.company;
          experience.position     = exp.position;
          experience.period       = exp.period;
          experience.location     = exp.location;
          experience.jobresponse  = exp.jobresponse;
          experience.insertedTime = [NSDate date];
          experience.ownby        = newResume;
    }
    
    NSSet *refSet     = currentResume.reference;
    NSArray *refArray = [refSet allObjects];
    
    for(Reference *ref in refArray) {
        
        Reference *reference = (Reference *)[NSEntityDescription insertNewObjectForEntityForName:@"Reference" inManagedObjectContext:context];
        
        
        reference.referencename = ref.referencename;
        reference.company       = ref.company;
        reference.phone         = ref.phone;
        reference.email         = ref.email;
        reference.ownby         = newResume;
        
        
    }
    
    Others *otherInfo = (Others *)[NSEntityDescription insertNewObjectForEntityForName:@"Others" inManagedObjectContext:context];
    
    otherInfo.drivingLicence = currentResume.other.drivingLicence;
    otherInfo.passportNo     = currentResume.other.passportNo;
    otherInfo.panNo          = currentResume.other.panNo;
    otherInfo.ownby          = newResume;
    
    
    PhotoSign *photoSign = (PhotoSign *)[NSEntityDescription insertNewObjectForEntityForName:@"PhotoSign" inManagedObjectContext:context];

        photoSign.photo     = currentResume.photosign.photo;
        photoSign.sign      = currentResume.photosign.sign;
        photoSign.place     = currentResume.photosign.place;
        photoSign.objective = currentResume.photosign.objective;
        photoSign.ownby     = newResume;
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Adding Resume failed %@ ,%@", [error localizedDescription],[error userInfo]);
        return NO;
    }
    
    
    return YES;
}


-(BOOL) isResumeExits:(NSString *)name{
    
    
    NSManagedObjectContext *context = self.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *resDesc = [NSEntityDescription entityForName:@"Resume" inManagedObjectContext:context];
    [fetchRequest setEntity:resDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title==%@",name];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0) {
        return YES;
    }
    
    return NO;
    
}


-(void)presentMediatorViewController:(NSString *)ofType{
    
    
    MediatorViewController *mVc = [[MediatorViewController alloc]initWithNibName:@"MediatorViewController" bundle:Nil resume:currentResume listType:ofType];
    mVc.managedObjectContext = self.managedObjectContext;

    [self.navigationController pushViewController:mVc animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidUnload {
    
    [super viewDidUnload];
}
/*
 case 2:
 {
 expTble_RowPoints = [[NSMutableArray alloc]init];
 NSSet *eduSet = currentResume.experience;
 NSArray *myArray = [eduSet allObjects];
 int colHeight;
 int newOrigin = origin.y;
 CGPoint from = CGPointMake(origin.x, newOrigin);
 CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
 [expTble_RowPoints addObject:@(newOrigin)];
 [self drawLineFromPoint:from toPoint:to];
 
 
 for (int i = 0; i <= numberOfRows; i++)
 {
 
 if([myArray count]>i)
 {
 Experience *exp = [myArray objectAtIndex:i];
 NSLog(@"========%@=======",exp.jobresponse);
 if([exp.jobresponse length] >14)
 {
 newOrigin =  newOrigin +rowHeight+15;
 NSLog(@"%@  length >14",exp.jobresponse);
 }
 else
 newOrigin =  newOrigin +(rowHeight);
 
 NSLog(@"newOrigin:::%d",newOrigin);
 [expTble_RowPoints addObject:@(newOrigin)];
 
 CGPoint from = CGPointMake(origin.x, newOrigin);
 CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
 [self drawLineFromPoint:from toPoint:to];
 
 }
 }
 colHeight = newOrigin;
 for (int i = 0; i <= numberOfColumns; i++)
 {
 int newOrigin = origin.x + (columnWidth*i);
 
 CGPoint from = CGPointMake(newOrigin, origin.y);
 CGPoint to = CGPointMake(newOrigin, colHeight);
 
 [self drawLineFromPoint:from toPoint:to];
 
 }
 }
 break;
 */

@end

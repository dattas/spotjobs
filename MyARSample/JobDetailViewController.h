//
//  JobDetailViewController.h
//  MyARSample
//
//  Created by vairat on 21/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol JobDetailViewDelegate;
@interface JobDetailViewController : UIViewController{
    
}
@property (unsafe_unretained) id <JobDetailViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *mainContainerHolder;
@property (strong, nonatomic) IBOutlet UIView *titleContainerHolder;
@property (strong, nonatomic) IBOutlet UIImageView *titleBg_ImageView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *mySegmentedControl;

@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;







@end

@protocol JobDetailViewDelegate <NSObject>

@end

//
//  JobDetailViewController.m
//  MyARSample
//
//  Created by vairat on 21/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "JobDetailViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface JobDetailViewController ()

@end

@implementation JobDetailViewController
@synthesize mainContainerHolder;
@synthesize titleBg_ImageView;
@synthesize titleContainerHolder;
@synthesize mySegmentedControl;
@synthesize myScrollView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    titleBg_ImageView.layer.borderWidth = 2.0;
    titleBg_ImageView.layer.borderColor = [UIColor orangeColor].CGColor;
    titleBg_ImageView.layer.cornerRadius = 4.0;
    */
    
    self.myScrollView.contentSize = CGSizeMake(320, 800);
    
    titleContainerHolder.layer.borderWidth = 2.0;
    titleContainerHolder.layer.borderColor = [UIColor orangeColor].CGColor;
    titleContainerHolder.layer.cornerRadius = 4.0;
    
    mainContainerHolder.layer.borderWidth = 2.0;
    mainContainerHolder.layer.borderColor = [UIColor orangeColor].CGColor;
    mainContainerHolder.layer.cornerRadius = 4.0;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMainContainerHolder:nil];
    [self setTitleContainerHolder:nil];
    [self setTitleBg_ImageView:nil];
    [self setMySegmentedControl:nil];
    [super viewDidUnload];
}
@end

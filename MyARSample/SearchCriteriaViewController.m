//
//  SearchCriteriaViewController.m
//  MyARSample
//
//  Created by vairat on 22/08/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "SearchCriteriaViewController.h"
#import "HomeViewController.h"
//#import "MyDealsViewController.h"
#import "HomePageViewController.h"
//#import "PostADealViewController.h"
//#import "PostADealMenuViewController.h"
#import "MyAccountViewController.h"
//#import "ShareAppViewController.h"
#import "HelpViewController.h"
#import "AppDelegate.h"
#import "SearchCell.h"
#import "Category.h"

@interface SearchCriteriaViewController (){
    
    NSArray *resultSet_Array;
    NSArray *subUrbs_Array;
   // NSMutableArray *categoryArray;
    
    int selectedCategory;
    
    ProductListParser *productsXMLParser;
    CategoryXMLParser *categoryParserDelegate;
    AppDelegate *appDelegate;
    
    UIActionSheet *searchCriteriaActionSheet;
    UIPickerView  *searchCriteriaPicker;
    NSArray *searchCriteriaItems;
    
    NSURLConnection *searchCriteria;
    NSURLConnection *dealsFetchRequest;
    UIView *pickerContainerView;

    
    
}
@property(nonatomic,retain)NSArray *searchCriteriaItems;
-(void)presentSearchCriteriaPicker;
- (void) fetchCategoryList;
- (void) searchDealsOnKeywordBasis;

@end

@implementation SearchCriteriaViewController

@synthesize suburb_TextField;
@synthesize searchCriteria_TextField;
@synthesize keyword_TextField;
@synthesize searchCriteriaItems;
@synthesize current_TextField;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
           // categoryArray = [[NSMutableArray alloc] init];
            selectedCategory = -1;
            }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"searchViewController");
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // self.searchCriteriaItems = [[NSArray alloc]initWithObjects:@"Automotive",@"Dining & FastFood",@"Golf Courses",@"Healthy & Beauty",@"Home & lifestyle",@"Leisure & Entertainment",@"Shopping & Vochers",@"Travel & Accommodation", nil];
    
    [self fetchCategoryList];
[self presentSearchCriteriaPicker];
   // [self configureCountryPicker];
}

-(void)fetchCategoryList{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_cat.php?cid=1406&country=Australia",URL_Prefix]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    searchCriteria = [[NSURLConnection alloc] initWithRequest:request delegate:self];

}

//================================---------------UIPICKERVIEW METHODS----------------====================================//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
        return [appDelegate.categoryList_Array count];

}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
           Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
           return catObj.catName;
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
        
        selectedCategory = row;
        Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
        self.searchCriteria_TextField.text = catObj.catName;
    
    
    
}


//======================------------------TEXTFIELD DELEGATE METHODS-------------------===========================//

- (void)textFieldDidBeginEditing:(UITextField *)textField {
   
    current_TextField = textField;
    
  
    
    if (textField== searchCriteria_TextField)
    {
     textField.inputView = pickerContainerView;
    }
   
    
    
    
    
}


-(void)configureCountryPicker
{
    // AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    UIPickerView  *myPickerView = [[ UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator=YES;
    myPickerView.backgroundColor=[UIColor whiteColor];
    
    
    
    
    [myPickerView setFrame:CGRectMake(0, 50, 320, 250)];
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
    [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    pickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 300, 320, 266)];
    pickerContainerView.backgroundColor = [UIColor whiteColor];
    [pickerContainerView addSubview:myPickerView];
    [pickerContainerView addSubview:button];
    
}
-(void)doneButtonPressed
{
    
    NSLog(@"doneButtonPressed");
    if (self.searchCriteria_TextField.text.length==0)
    {
        self.searchCriteria_TextField.text = @"Automotive";

    }
  //  [self bringTheFirstTimeLoginViewDown];
    //[pickerContainerView removeFromSuperview];
    [current_TextField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
        [textField resignFirstResponder];
        return YES;
}

//======================-------------------CUSTOM METHODS------------------===========================//

-(void)presentSearchCriteriaPicker
{
    // AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    UIPickerView  *myPickerView = [[ UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator=YES;
    myPickerView.backgroundColor=[UIColor whiteColor];
    
    
    
    
    [myPickerView setFrame:CGRectMake(0, 50, 320, 250)];
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
    [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    pickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 300, 320, 266)];
    pickerContainerView.backgroundColor = [UIColor whiteColor];
    [pickerContainerView addSubview:myPickerView];
    [pickerContainerView addSubview:button];
    
}
-(void)doneButtonClicked{
       if(selectedCategory == -1)
           self.searchCriteria_TextField.text = @"Automotive";
       [searchCriteriaActionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)cancelButtonClicked{
    [searchCriteriaActionSheet dismissWithClickedButtonIndex:0 animated:YES];
}
- (IBAction)resetButtonTapped:(id)sender{
    selectedCategory = -1;
    self.searchCriteria_TextField.text = @"";
    self.keyword_TextField.text = @"";
    self.suburb_TextField.text = @"";
}
- (IBAction)homeButtonTapped:(id)sender{
    
    RevealController *revealController = [self.parentViewController isKindOfClass:[RevealController class]] ? (RevealController *)self.parentViewController : nil;
    
        HomePageViewController *home = [[HomePageViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:home];
        navigationController.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
        [revealController setFrontViewController:navigationController animated:NO];
    
}

- (IBAction)findMeDeal_Action:(id)sender {
   
    /*
   // http://www.myrewards.com.au/app/webroot/newapp/search.php?cat_id=10&cid=24&country=Australia&start=0&limit=30
    
   // NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.myrewards.com.au/app/webroot/newapp/search.php?cat_id=10&cid=24&country=Australia&start=0&limit=30"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.myrewards.com.au/app/webroot/newapp/search_with_map.php?cat_id=10&cid=24&country=Australia"]];
    NSLog(@"findMeJob_Action");

     NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];   */
    
 if ((selectedCategory == -1 && suburb_TextField.text.length == 0) && keyword_TextField.text.length == 0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a search criteria" message:@"Please select a category or enter a keyword or a location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
        return;
    }
    if (selectedCategory == -1 && keyword_TextField.text.length != 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a search criteria" message:@"Please select a category " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
        
    }
    [current_TextField resignFirstResponder];
    [self searchDealsOnKeywordBasis];
    
    
}

-(void)searchDealsOnKeywordBasis{
    
    NSLog(@"searchDealsOnKeywordBasis");
    Category *cat;
    NSString *categoryID = @"-1";
    if(selectedCategory != -1)
    {
        cat = [appDelegate.categoryList_Array objectAtIndex:selectedCategory];
        categoryID = cat.catId;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@search_with_map.php?",URL_Prefix];
    
    
    if(selectedCategory != -1)
    {
        urlString = [NSString stringWithFormat:@"%@cat_id=%@",urlString,categoryID];
        
    }
    if([self.suburb_TextField.text length]>0)
    {
        
        if(selectedCategory != -1)
        {
            
            urlString = [NSString stringWithFormat:@"%@&",urlString];
            
        }
        urlString = [NSString stringWithFormat:@"%@p=%@",urlString,self.suburb_TextField.text];
    }
    if([keyword_TextField.text length]>0)
    {
        
        if(selectedCategory != -1 || ([self.suburb_TextField.text length]!= 0))
        {
            
            urlString = [NSString stringWithFormat:@"%@&",urlString];
            
        }
        urlString = [NSString stringWithFormat:@"%@q=%@",urlString,keyword_TextField.text];
    }
    
    urlString = [NSString stringWithFormat:@"%@&cid=24&country=Australia&start=0&limit=30",urlString];
    
    NSLog(@"Search URL::%@",urlString);
    NSString *searchURL = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:searchURL]];
    dealsFetchRequest = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
}
//=======================-----------NSURLCONNECTION METHODS---------================================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
   
    [_responseData appendData:data];
    
    if(connection == searchCriteria)
    {
        NSLog(@"searchCriteria Request...");
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"RESPONSE::::%@",responseString);
        
        NSXMLParser *categoryParser = [[NSXMLParser alloc] initWithData:_responseData];
        categoryParserDelegate = [[CategoryXMLParser alloc] init];
        categoryParserDelegate.delegate = self;
        categoryParser.delegate = categoryParserDelegate;
        [categoryParser parse];
        
    }
    else
    {
       NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:_responseData];
       productsXMLParser = [[ProductListParser alloc] init];
       productsXMLParser.delegate = self;
       productsParser.delegate = productsXMLParser;
       [productsParser parse];
    }
}


//=====================------------------ PARSING METHODS --------------=============================//

#pragma mark -- Category Parser Delegate methods

- (void)parsingCategoriesFinished:(NSArray *)categoryList
{
    
    for (Category *cat in categoryList)
        [appDelegate.categoryList_Array addObject:cat];

}

- (void)categoryXMLparsingFailed
{
    
}


#pragma mark -- Product Parser Delegate methods

- (void)parsingProductListFinished:(NSArray *)productsList {
    
   // NSLog(@".......... productsListLocal: %d",[productsList count]);
    
    // Here update products list
//    if (!productsList) {
//        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
//    }
    //else {
        [appDelegate.productsList_Array removeAllObjects];
        [appDelegate.productsList_Array addObjectsFromArray:productsList];
   // }
    NSLog(@"Products Count %d",[appDelegate.productsList_Array count]);
    //[self performSelector:@selector(loadContent) withObject:nil afterDelay:1];
    
//    for(int i=0;i<[productsList count];i++){
//        Product *pro = [productsList objectAtIndex:i];
//        NSLog(@"PRODUCT LATTITUDE:%f ,LONGITUDE::%f ",pro.coordinate.latitude,pro.coordinate.longitude);
//    }
    
    RevealController *revealController = [self.parentViewController isKindOfClass:[RevealController class]] ? (RevealController *)self.parentViewController : nil;
    
    //[revealController revealToggle:self];
    HomeViewController *home = [[HomeViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:home];
    navigationController.navigationBar.tintColor = [UIColor colorWithRed:213/255.f green:56/255.f blue:56/255.f alpha:1.0f];
    [revealController setFrontViewController:navigationController animated:NO];
    
}
- (void)parsingProductListXMLFailed {
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

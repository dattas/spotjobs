//
//  UserViewController.h
//  MyARSample
//
//  Created by vairat on 26/10/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserViewController : UIViewController<UITextFieldDelegate>{
    
}


@property (strong, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) IBOutlet UIView *menuView;

- (IBAction)postResume:(id)sender;


- (IBAction)submit_Tapped:(id)sender;

@end

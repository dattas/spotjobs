//
//  ListViewController.h
//  MyARSample
//
//  Created by vairat on 06/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UINavigationControllerDelegate>
{
    
}
@property (strong, nonatomic)IBOutlet UITableView *myTableView;
@property (strong, nonatomic)UITextField *alertTextField;

@property (nonatomic, retain) NSManagedObjectContext        *managedObjectContext;
@end

//
//  HomePageViewController.m
//  MyARSample
//
//  Created by vairat on 11/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "HomePageViewController.h"
#import "HelpViewController.h"
#import "UserViewController.h"
#import "UserLoginViewController.h"
#import "DMLazyScrollView.h"

@interface HomePageViewController ()<DMLazyScrollViewDelegate>{
    
    DMLazyScrollView* lazyScrollView;
    NSMutableArray*    viewControllerArray;
    NSArray *pageImages_Array;
}
@property(nonatomic,strong)NSArray *pageImages_Array;
@end

@implementation HomePageViewController
@synthesize pageImages_Array;
@synthesize myPage_Control;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Spot Jobs";
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
	{
		UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
		[self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self.navigationController.parentViewController action:@selector(revealToggle:)];

       
    }
    
    self.pageImages_Array = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"photo1.png"],[UIImage imageNamed:@"photo2.png"],[UIImage imageNamed:@"photo3.png"],[UIImage imageNamed:@"photo4.png"], nil];
    
    NSUInteger numberOfPages = 4;
    viewControllerArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSUInteger k = 0; k < numberOfPages; k++) {
        [viewControllerArray addObject:[NSNull null]];
    }
    
    CGRect rect = CGRectMake(0, 35, self.view.frame.size.width, 520.0f);
    lazyScrollView = [[DMLazyScrollView alloc] initWithFrame:rect];
    
    
    lazyScrollView.dataSource = ^(NSUInteger index) {
        return [self controllerAtIndex:index];
    };
    lazyScrollView.numberOfPages = numberOfPages;
    lazyScrollView.controlDelegate = self;
    [self.view addSubview:lazyScrollView];
    [self.view bringSubviewToFront:self.myPage_Control];

}

- (UIViewController *) controllerAtIndex:(NSInteger) index {
    
    UIImageView   *myImageView;
    
    
    if (index > viewControllerArray.count || index < 0) return nil;
    
    id res = [viewControllerArray objectAtIndex:index];
    if (res == [NSNull null]) {
        UIViewController *contr = [[UIViewController alloc] init];
        contr.view.backgroundColor = [UIColor clearColor];
        
        myImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,0.0f,320, 566.0f)];
        myImageView.image = [self.pageImages_Array  objectAtIndex:index];
        [contr.view addSubview:myImageView];
        
        [viewControllerArray replaceObjectAtIndex:index withObject:contr];
        return contr;
    }
    return res;
}
- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex
{
    NSLog(@"currentPage %d",currentPageIndex);
    
    
    self.myPage_Control.currentPage = currentPageIndex;
    
   
    
    
}















- (IBAction)MenuButtonTapped:(id)sender {
    
    switch ([sender tag])
    {
        case 1:
              [self presentUserLoginViewController];
            break;
        case 2:
              [self presentEmployeeViewController];
            break;
        case 3:
              [self presentHelpViewController];
            break;
            
        default:
            break;
    }

    
}


-(void)presentUserLoginViewController{
    
    UserLoginViewController *userVC = [[UserLoginViewController alloc]initWithNibName:@"UserLoginViewController" bundle:Nil userType:@"jobSeeker"];
    [self presentNewViewController:userVC];

}
-(void)presentEmployeeViewController{
    
//EmployeeViewController *employee = [[EmployeeViewController alloc]initWithNibName:@"EmployeeViewController" bundle:Nil];
   // [self presentNewViewController:employee];
    UserLoginViewController *userVC = [[UserLoginViewController alloc]initWithNibName:@"UserLoginViewController" bundle:Nil userType:@"jobPoster"];
    [self presentNewViewController:userVC];

}
-(void)presentHelpViewController{
    
    HelpViewController *help = [[HelpViewController alloc]initWithNibName:@"HelpViewController" bundle:Nil];
    [self presentNewViewController:help];

}


-(void)presentNewViewController:(UIViewController *) controller
{
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController pushViewController:controller animated:NO];}
     completion:NULL];
    
}













- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  Resume.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "Resume.h"
#import "Education.h"
#import "Personal.h"


@implementation Resume

@dynamic resumeId;
@dynamic title;
@dynamic education;
@dynamic experience;
@dynamic other;
@dynamic personal;
@dynamic photosign;
@dynamic projects;
@dynamic reference;

@end

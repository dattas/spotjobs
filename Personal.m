//
//  Personal.m
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "Personal.h"
#import "Resume.h"


@implementation Personal

@dynamic address;
@dynamic dob;
@dynamic emailId;
@dynamic fullName;
@dynamic gender;
@dynamic language;
@dynamic phNo;
@dynamic title;
@dynamic ownby;

@end

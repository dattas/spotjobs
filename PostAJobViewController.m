//
//  PostAJobViewController.m
//  MyARSample
//
//  Created by vairat on 21/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "PostAJobViewController.h"
#import "AppDelegate.h"
#import "HelpViewController.h"
@interface PostAJobViewController (){
    AppDelegate *appDelegate;
    
    BOOL isMorning;
    BOOL isAftrNoon;
    BOOL isEvening;
    BOOL isWeekDays;
    BOOL isWeekEnds;
    BOOL isCustomerFacing;
    BOOL isIndoor;
    BOOL isOutdoor;
    
}

@end

@implementation PostAJobViewController
@synthesize jobSummaryArray;
@synthesize jobDetailsArray;

@synthesize jobDescripitionTextView;
@synthesize jobRequirementTextView;

@synthesize eligibilityTextField;
@synthesize experienceTextField;
@synthesize jobRoleTextField;
@synthesize customerFacing;

@synthesize weekDaysButton;
@synthesize weekEndsButton;
@synthesize indoorsButton;
@synthesize outdoorsButton;
@synthesize moringButton;
@synthesize afternoonButton;
@synthesize eveningButton;

@synthesize postajobScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationItem.title=@"Post A Job";
    
    /*
    UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
    self.navigationItem.rightBarButtonItem = leftButton;
    UIImage *myImage2 = [UIImage imageNamed:@"Share.png"];
    
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(postJob) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    self.navigationItem.rightBarButtonItem = leftButton; */
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    
    isMorning        = NO;
    isAftrNoon       = NO;
    isEvening        = NO;
    isWeekDays       = NO;
    isWeekEnds       = NO;
    isCustomerFacing = NO;
    isIndoor         = NO;
    isOutdoor        = NO;
    
    
    
    [postajobScrollView setContentSize:CGSizeMake(320, 1000)];
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
	[keyBoardController addToolbarToKeyboard];
   
}
-(void)backaction
{
    
    [UIView transitionWithView:self.navigationController.view
                      duration:0.15f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                    completion:NULL];
    
}

- (IBAction)weeksButtonTapped:(id)sender{

    switch ([sender tag]) {
        case 2:
                  if(isWeekDays)
                  {
                      [self.weekDaysButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                      isWeekDays = NO;
                  }
                  else
                  {
                      
                      [self.weekDaysButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                      isWeekDays = YES;
                      
                  }
            
                  break;
            
        case 1:
                 if(isWeekEnds)
                {
                   
                    [self.weekEndsButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                   isWeekEnds = NO;
                 }
                else
                {
                   [self.weekEndsButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                  isWeekEnds = YES;
                
                }
            
            break;
        default:
            break;
    }

}
- (IBAction)inOutButtonTapped:(id)sender{

    switch ([sender tag]) {
        case 1:
            if(isIndoor)
            {
                
                [self.indoorsButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isIndoor = NO;
            }
            else
            {
                [self.indoorsButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isIndoor = YES;
                
            }
            
            break;
            
        case 2:
            if(isOutdoor)
            {
                
                [self.outdoorsButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isOutdoor = NO;
            }
            else
            {
                [self.outdoorsButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isOutdoor = YES;
                
            }
            
            break;
        default:
            break;
    }
}
- (IBAction)customerButtonTapped:(id)sender
{
   
            if(isCustomerFacing)
            {
                
                [self.customerFacing setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isCustomerFacing = NO;
            }
            else
            {
                [self.customerFacing setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isCustomerFacing = YES;
                
            }
            
}
- (IBAction)timeOfDayButtonTapped:(id)sender{

    switch ([sender tag]) {
        case 1:
            if(isMorning)
            {
                
                [self.moringButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isMorning = NO;
            }
            else
            {
                [self.moringButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isMorning = YES;
                
            }
            break;
            
        case 2:
            if(isAftrNoon)
            {
                
                [self.afternoonButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isAftrNoon = NO;
            }
            else
            {
                [self.afternoonButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isAftrNoon = YES;
                
            }
            break;
            
        case 3:
            if(isEvening)
            {
                
                [self.eveningButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
                isEvening = NO;
            }
            else
            {
                [self.eveningButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
                isEvening = YES;
                
            }
            break;
        default:
            break;
    }








}


- (IBAction)test_Action:(id)sender {
    
    NSLog(@"test_Action");
    HelpViewController *help =[[HelpViewController alloc]initWithNibName:@"HelpViewController" bundle:nil];
    [self.navigationController pushViewController:help animated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCustomerFacing:nil];
    [self setMoringButton:nil];
    [self setAfternoonButton:nil];
    [self setEveningButton:nil];
    [super viewDidUnload];
}

@end

//
//  UserDataXMLParser.m
//  GrabItNow
//
//  Created by MyRewards on 12/2/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "UserDataXMLParser.h"
#import "User.h"

@interface UserDataXMLParser()
{
    NSMutableString *charString;
    User *userDetails;
}
@property (nonatomic, strong) User *userDetails;

@end

@implementation UserDataXMLParser

@synthesize userDetails;
@synthesize delegate;

-(void)dealloc {
    userDetails = nil;
    delegate = nil;
}


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    
    if ([elementName isEqualToString:@"status"]) {
        userDetails = [[User alloc] init];
    }
    else if ([elementName isEqualToString:@"result"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"id"]) {
        charString = nil;
    }
    /*
    else if ([elementName isEqualToString:@"domain_id"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"type"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"username"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"email"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"first_name"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"last_name"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"state"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"country"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"mobile"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"card_ext"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"client_name"]) {
        charString = nil;
    }
    else if ([elementName isEqualToString:@"newsletter"]) {
        charString = nil;
    }*/

}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"status"]) {
        [parser abortParsing];
        if (delegate) {
            if ([delegate respondsToSelector:@selector(parsingUserDetailsFinished:)]) {
                
                [delegate parsingUserDetailsFinished:userDetails];
            }
        }
        userDetails = nil;
    }
    else if ([elementName isEqualToString:@"result"]) {
        userDetails.result = finalString;
    }
    else if ([elementName isEqualToString:@"id"]) {
        userDetails.userId = finalString;
    }
        
    charString = nil;
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    if (delegate) {
        if ([delegate respondsToSelector:@selector(userDetailXMLparsingFailed)]) {
            [delegate userDetailXMLparsingFailed];
        }
    }
}

@end

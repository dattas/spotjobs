//
//  Resume.h
//  MyARSample
//
//  Created by vairat on 08/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Education, Personal, Others, PhotoSign;

@interface Resume : NSManagedObject

@property (nonatomic, retain) NSString * resumeId;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *education;
@property (nonatomic, retain) NSSet *experience;
@property (nonatomic, retain) Others *other;
@property (nonatomic, retain) Personal *personal;
@property (nonatomic, retain) PhotoSign *photosign;
@property (nonatomic, retain) NSSet *projects;
@property (nonatomic, retain) NSSet *reference;
@end

@interface Resume (CoreDataGeneratedAccessors)

- (void)addEducationObject:(Education *)value;
- (void)removeEducationObject:(Education *)value;
- (void)addEducation:(NSSet *)values;
- (void)removeEducation:(NSSet *)values;

- (void)addExperienceObject:(NSManagedObject *)value;
- (void)removeExperienceObject:(NSManagedObject *)value;
- (void)addExperience:(NSSet *)values;
- (void)removeExperience:(NSSet *)values;

- (void)addProjectsObject:(NSManagedObject *)value;
- (void)removeProjectsObject:(NSManagedObject *)value;
- (void)addProjects:(NSSet *)values;
- (void)removeProjects:(NSSet *)values;

- (void)addReferenceObject:(NSManagedObject *)value;
- (void)removeReferenceObject:(NSManagedObject *)value;
- (void)addReference:(NSSet *)values;
- (void)removeReference:(NSSet *)values;

@end

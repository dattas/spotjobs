//
//  SearchCell.m
//  MyARSample
//
//  Created by vairat on 04/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell
@synthesize cellNameLabel;
@synthesize cellImageView;
@synthesize cellButton;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initialization code
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
   
    // Configure the view for the selected state
}

@end

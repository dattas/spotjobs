//
//  CompanyAddress.h
//  MyARSample
//
//  Created by vairat on 23/11/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompanyAddress : NSObject{
    
    
    NSString *addressID;
    NSString *contactName;
    NSString *contactNumber;
    NSString *mobileNumber;
    NSString *address;
    NSString *street;
    NSString *suburb;
    NSString *state;
    NSString *country;
    NSString *postCode;
    NSString *latitude;
    NSString *longitude;
    NSString *altitude;

    
}
@property (nonatomic, strong) NSString *addressID;
@property (nonatomic, strong) NSString *contactName;
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic, strong) NSString *mobileNumber;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *suburb;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *postCode;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *altitude;



@end

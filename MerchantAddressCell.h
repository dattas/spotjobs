//
//  MerchantAddressCell.h
//  GrabItNow
//
//  Created by MyRewards on 3/14/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MerchantAddressCell : UITableViewCell
{
    UILabel *merchantAddressLabel;
    UIButton *mapButton;
}
@property(nonatomic,retain)IBOutlet UILabel *merchantAddressLabel;
@property(nonatomic,retain)IBOutlet UILabel *mapLabel;
@property(nonatomic,strong)IBOutlet UIButton *mapButton;
@property(nonatomic,strong)IBOutlet UIButton *checkButton;
@end
